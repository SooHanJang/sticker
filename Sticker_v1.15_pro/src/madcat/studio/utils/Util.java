package madcat.studio.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

public class Util {
	
	/**
	 * 입력받은 msg를 toast로 표시합니다. duration : Toast.LENGTH_SHORT
	 * @param context
	 * @param msg
	 */
	public static void showToastMessage(Context context, String message) {
		showToastMessage(context, message, Toast.LENGTH_SHORT);
	}
	
	/**
	 * 입력받은 duration 돟안 입력받은 msg를 toast로 표시합니다.
	 * @param context
	 * @param msg
	 * @param duration
	 */
	public static void showToastMessage(Context context, String message, int duration) {
		Toast.makeText(context, message, duration).show();
	}
	
	public static void printLogMessage(String tag, String message) {
		if (Constants.DEV_FLAG)
			Log.d(tag, message);
	}
	
	public static ArrayList<Data> sortDataArrayList(ArrayList<Data> dataList) {
		if (dataList != null) {
			Collections.sort(dataList);
		}
		
		return dataList;
	}
	
	/**
	 * Object 타입을 byte 타입으로 Serialize 합니다.
	 * 
	 * @param object
	 * @return
	 */
	
	public static byte[] serializeObject(Object object) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.close();
			
			byte[] buf = bos.toByteArray();
			
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	/**
	 * byte 타입을 해당 Object 로 deSerialize 합니다. 
	 * 
	 * @param b
	 * @return
	 */
	
	public static Object deSerializeObject(byte[] b) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b));
			Object object = ois.readObject();
			
			ois.close();
			
			return object;
		} catch(IOException ie) {
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
}

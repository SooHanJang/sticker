package madcat.studio.widget;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.database.DataBase;
import madcat.studio.service.WidgetUpdateService;
import madcat.studio.sticker.pro.R;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;

public class DynamicWidgetCalendar extends AppWidgetProvider {
	private static final String TAG = "STICKER_APP_WIDGET_CALENDAR";
	
	private SparseArray<Data> mDataSparseArray = null;
	private UpdateAsyncTask mUpdateAsyncTask = null;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
	
		if (intent != null) {
			String action = intent.getAction();
			
			if (action != null) {
				int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
				
				if (Constants.DEV_FLAG) {
					Log.d(TAG, "ACTION : " + action);
					Log.d(TAG, "WIDGET ID : " + appWidgetId);
				}
				
				if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
					//Widget Update
					if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
						updateWidget(context);
					}
				} else if (action.equals(AppWidgetManager.ACTION_APPWIDGET_DELETED)) {
					//Delete AppWidget Id from DB
					if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
						DataBase.deleteData(context, appWidgetId);
						if (DataBase.countDatasByDynamicWidgetTypes(context) == 0) {
							Intent stopServiceIntent = new Intent(WidgetUpdateService.SERVICE_TERMINATE_ACTION);
							context.startService(stopServiceIntent);
						}
					}
				} else if (action.equals(Constants.WIDGET_UPDATE_DYNAMIC_CALENDAR)) {
					//Widget Update
					updateWidget(context);
				}
			}
		}
	}
	
	private void updateWidget(Context context) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ArrayList<Data> dataList = DataBase.selectDatasByWidgetType(context, Constants.WIDGET_TYPE_DYNAMIC_CALENDAR);
		
		if (!(dataList == null || dataList.isEmpty())) {
			if (mDataSparseArray == null) {
				mDataSparseArray = new SparseArray<Data>();
			} else {
				mDataSparseArray.clear();
			}

			int[] widgetIds = new int[dataList.size()];
			
			for (int i = 0; i < widgetIds.length; i++) {
				Data data = dataList.get(i);
				mDataSparseArray.put(data.getWidgetId(), data);
				widgetIds[i] = data.getWidgetId();
			}
			
			onUpdate(context, appWidgetManager, widgetIds);
		}
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] widgetIds) {
		// TODO Auto-generated method stub
		boolean updateFlag = false;
		
		if (mDataSparseArray == null || mDataSparseArray.size() == 0) {
			ArrayList<Data> dataList = DataBase.selectDatasByWidgetType(context, Constants.WIDGET_TYPE_DYNAMIC_CALENDAR);
			
			if (!(dataList == null || dataList.isEmpty())) {
				if (mDataSparseArray == null) {
					mDataSparseArray = new SparseArray<Data>();
				} else {
					mDataSparseArray.clear();
				}

				widgetIds = new int[dataList.size()];
				
				for (int i = 0; i < widgetIds.length; i++) {
					Data data = dataList.get(i);
					mDataSparseArray.put(data.getWidgetId(), data);
					widgetIds[i] = data.getWidgetId();
				}
				
				updateFlag = true;
			} else {
				updateFlag = false;
			}
		} else {
			updateFlag = true;
		}
		
		if (updateFlag) {
			super.onUpdate(context, appWidgetManager, widgetIds);
			
			if (mUpdateAsyncTask != null) {
				mUpdateAsyncTask.cancel(true);
				mUpdateAsyncTask = null;
			}
			
			mUpdateAsyncTask = new UpdateAsyncTask(context, appWidgetManager, widgetIds);
			mUpdateAsyncTask.execute();
		}
	}
	
	private class UpdateAsyncTask extends AsyncTask<Void, Void, Void> {
		private static final String CALENDAR_CELL_PREFIX	= "widget_layout_dynamic_calendar_cell_";
		
		private Context context = null;
		private AppWidgetManager appWidgetManager = null;
		private int[] widgetIds = null;
		
		private String packageName = null;
		
		public UpdateAsyncTask(Context context, AppWidgetManager appWidgetManager, int[] widgetIds) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.appWidgetManager = appWidgetManager;
			this.widgetIds = widgetIds;
			packageName = context.getPackageName();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < widgetIds.length; i++) {
				appWidgetManager.updateAppWidget(widgetIds[i], makeCalendarUI(context, mDataSparseArray.get(widgetIds[i])));
			}
			
			return null;
		}
		
		private RemoteViews makeCalendarUI(Context context, Data data) {
			int WHITE	= context.getResources().getColor(R.color.black);
			int GRAY	= context.getResources().getColor(R.color.gray);
			int BLUE	= context.getResources().getColor(R.color.blue);
			int RED		= context.getResources().getColor(R.color.red);
			
			int imageId = context.getResources().getIdentifier(data.getWidgetStickerName(), Constants.DRAWABLE, packageName);
			int alphaLv = data.getWidgetAlphaLevel();
			
			Drawable drawable = context.getResources().getDrawable(imageId).mutate();
			drawable.setAlpha(alphaLv);
			
			Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			drawable.setBounds(0,  0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
			drawable.draw(canvas);
			
			RemoteViews remoteViews = new RemoteViews(packageName, R.layout.widget_layout_dynamic_calendar);
			remoteViews.setImageViewBitmap(R.id.widget_layout_dynamic_calendar_image, bitmap);
			
			Calendar thisMonth = new GregorianCalendar();
			String thisYear = null;
			String monthArray[] = context.getResources().getStringArray(R.array.widget_calendar_month_array);
			
			if (context.getResources().getConfiguration().locale.equals(Locale.KOREA)) {
				thisYear = thisMonth.get(Calendar.YEAR) + context.getString(R.string.view_custom_menu_date_criteria_year) + monthArray[thisMonth.get(Calendar.MONTH)];
			} else {
				thisYear = monthArray[thisMonth.get(Calendar.MONTH)] +  ". " + thisMonth.get(Calendar.YEAR);
			}
			
			remoteViews.setTextViewText(R.id.widget_layout_dynamic_calendar_title_text, thisYear);
			
			if (thisMonth.get(Calendar.DATE) != 1) {
				thisMonth.set(Calendar.DATE, thisMonth.getMinimum(Calendar.DATE));
			}
			
			int thisMonthOfDayOfWeek = thisMonth.get(Calendar.DAY_OF_WEEK);
			int thisMonthOfYear = thisMonth.get(Calendar.MONTH);
			
			if (thisMonthOfDayOfWeek != 1) {
				thisMonth.add(Calendar.DATE, (thisMonthOfDayOfWeek - 1) * -1);
			} else {
				thisMonth.add(Calendar.DATE, -7);
			}
			
			for (int i = 0; i < 7 * 6; i++) {
				int id = context.getResources().getIdentifier(CALENDAR_CELL_PREFIX + i, Constants.ID, packageName);
				
				remoteViews.setTextViewText(id, Integer.toString(thisMonth.get(Calendar.DATE)));
				if (thisMonth.get(Calendar.MONTH) != thisMonthOfYear) {
					remoteViews.setTextColor(id, GRAY);
				} else {
					switch (thisMonth.get(Calendar.DAY_OF_WEEK)) {
					case Calendar.SUNDAY:
						remoteViews.setTextColor(id, RED);
						break;
					case Calendar.MONDAY:
					case Calendar.TUESDAY:
					case Calendar.WEDNESDAY:
					case Calendar.THURSDAY:
					case Calendar.FRIDAY:
						remoteViews.setTextColor(id, WHITE);
						break;
					case Calendar.SATURDAY:
						remoteViews.setTextColor(id, BLUE);
						break;
					}
				}
				
				thisMonth.add(Calendar.DATE, 1);
			}
			
			Intent intent = new Intent(context, WidgetQuickActionExecutor.class);
			if (data.getWidgetAction() != "") { intent.setAction(data.getWidgetAction()); }
			remoteViews.setOnClickPendingIntent(R.id.widget_layout_dynamic_calendar_image, PendingIntent.getActivity(context, 0, intent, 0));
			
			return remoteViews;
		}
	}
}

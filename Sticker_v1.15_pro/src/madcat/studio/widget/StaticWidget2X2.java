package madcat.studio.widget;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.database.DataBase;
import madcat.studio.sticker.pro.R;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RemoteViews;

public class StaticWidget2X2 extends AppWidgetProvider {
	private static final String TAG = "STICKER_APP_WIDGET_2X2";
	
	private SparseArray<Data> mDataSparseArray = null;
	private UpdateAsyncTask mUpdateAsyncTask = null;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
	
		if (intent != null) {
			String action = intent.getAction();
			
			if (action != null) {
				int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
				
				if (Constants.DEV_FLAG) {
					Log.d(TAG, "ACTION : " + action);
					Log.d(TAG, "WIDGET ID : " + appWidgetId);
				}
				
				if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
					//Widget Update
					if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
						updateWidget(context);
					}
				} else if (action.equals(AppWidgetManager.ACTION_APPWIDGET_DELETED)) {
					//Delete AppWidget Id from DB
					if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
						DataBase.deleteData(context, appWidgetId);
					}
				} else if (action.equals(Constants.WIDGET_UPDATE_STATIC_2X2)) {
					//Widget Update
					updateWidget(context);
				}
			}
		}
	}
	
	private void updateWidget(Context context) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ArrayList<Data> dataList = DataBase.selectDatasByWidgetSizeAndType(context, Constants.WIDGET_SIZE_2X2, Constants.WIDGET_TYPE_STATIC);
		
		if (!(dataList == null || dataList.isEmpty())) {
			if (mDataSparseArray == null) {
				mDataSparseArray = new SparseArray<Data>();
			} else {
				mDataSparseArray.clear();
			}

			int[] widgetIds = new int[dataList.size()];
			
			for (int i = 0; i < widgetIds.length; i++) {
				Data data = dataList.get(i);
				mDataSparseArray.put(data.getWidgetId(), data);
				widgetIds[i] = data.getWidgetId();
			}
			
			onUpdate(context, appWidgetManager, widgetIds);
		}
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] widgetIds) {
		// TODO Auto-generated method stub
		boolean updateFlag = false;
		
		if (mDataSparseArray == null || mDataSparseArray.size() == 0) {
			ArrayList<Data> dataList = DataBase.selectDatasByWidgetSizeAndType(context, Constants.WIDGET_SIZE_2X2, Constants.WIDGET_TYPE_STATIC);
			
			if (!(dataList == null || dataList.isEmpty())) {
				if (mDataSparseArray == null) {
					mDataSparseArray = new SparseArray<Data>();
				} else {
					mDataSparseArray.clear();
				}

				widgetIds = new int[dataList.size()];
				
				for (int i = 0; i < widgetIds.length; i++) {
					Data data = dataList.get(i);
					mDataSparseArray.put(data.getWidgetId(), data);
					widgetIds[i] = data.getWidgetId();
				}
				
				updateFlag = true;
			} else {
				updateFlag = false;
			}
		} else {
			updateFlag = true;
		}
		
		if (updateFlag) {
			super.onUpdate(context, appWidgetManager, widgetIds);
			
			if (mUpdateAsyncTask != null) {
				mUpdateAsyncTask.cancel(true);
				mUpdateAsyncTask = null;
			}
			
			mUpdateAsyncTask = new UpdateAsyncTask(context, appWidgetManager, widgetIds);
			mUpdateAsyncTask.execute();
		}
	}
	
	private class UpdateAsyncTask extends AsyncTask<Void, Void, Void> {
		private Context context = null;
		private AppWidgetManager appWidgetManager = null;
		private int[] widgetIds = null;
		private String packageName = null;
		
		public UpdateAsyncTask(Context context, AppWidgetManager appWidgetManager, int[] widgetIds) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.appWidgetManager = appWidgetManager;
			this.widgetIds = widgetIds;
			packageName = context.getPackageName();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < widgetIds.length; i++) {
				Data data = mDataSparseArray.get(widgetIds[i]);
				
				int stickerId = context.getResources().getIdentifier(data.getWidgetStickerName(), Constants.DRAWABLE, packageName);
				int backgroundId = context.getResources().getIdentifier(data.getWidgetBackgroundName(), Constants.DRAWABLE, packageName);
				int alphaLv = data.getWidgetAlphaLevel();
				
				Drawable drawable = context.getResources().getDrawable(backgroundId).mutate();
				drawable.setAlpha(alphaLv);
				
				Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				drawable.setBounds(0,  0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
				drawable.draw(canvas);
				
				RemoteViews remoteViews = new RemoteViews(packageName, R.layout.widget_layout_static_2x2);
				remoteViews.setImageViewBitmap(R.id.widget_layout_static_image_2x2_background, bitmap);
				remoteViews.setImageViewResource(R.id.widget_layout_static_image_2x2_sticker, stickerId);
				
				Intent intent = new Intent(context, WidgetQuickActionExecutor.class);
				if (data.getWidgetAction() != "") { intent.setAction(data.getWidgetAction()); }
				remoteViews.setOnClickPendingIntent(R.id.widget_layout_static_image_2x2_sticker, PendingIntent.getActivity(context, 0, intent, 0));
				
				appWidgetManager.updateAppWidget(widgetIds[i], remoteViews);
			}
			
			return null;
		}
	}
}

package madcat.studio.widget;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.database.DataBase;
import madcat.studio.service.WidgetUpdateService;
import madcat.studio.sticker.pro.R;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.RemoteViews;

public class DynamicWidgetCounter extends AppWidgetProvider {
	private static final String TAG = "STICKER_APP_WIDGET_COUNTER";
	
	private SparseArray<Data> mDataSparseArray = null;
	private UpdateAsyncTask mUpdateAsyncTask = null;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
	
		if (intent != null) {
			String action = intent.getAction();
			
			if (action != null) {
				int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
				
				if (Constants.DEV_FLAG) {
					Log.d(TAG, "ACTION : " + action);
					Log.d(TAG, "WIDGET ID : " + appWidgetId);
				}
				
				if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
					//Widget Update
					if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
						updateWidget(context);
					}
				} else if (action.equals(AppWidgetManager.ACTION_APPWIDGET_DELETED)) {
					//Delete AppWidget Id from DB
					if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
						DataBase.deleteData(context, appWidgetId);
						if (DataBase.countDatasByDynamicWidgetTypes(context) == 0) {
							Intent stopServiceIntent = new Intent(WidgetUpdateService.SERVICE_TERMINATE_ACTION);
							stopServiceIntent.setAction(WidgetUpdateService.SERVICE_TERMINATE_ACTION);
							context.startService(stopServiceIntent);
						}
					}
				} else if (action.equals(Constants.WIDGET_UPDATE_DYNAMIC_COUNTER)) {
					//Widget Update
					updateWidget(context);
				}
			}
		}
	}
	
	private void updateWidget(Context context) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		ArrayList<Data> dataList = DataBase.selectDatasByWidgetType(context, Constants.WIDGET_TYPE_DYNAMIC_COUNTER);
		
		if (!(dataList == null || dataList.isEmpty())) {
			if (mDataSparseArray == null) {
				mDataSparseArray = new SparseArray<Data>();
			} else {
				mDataSparseArray.clear();
			}

			int[] widgetIds = new int[dataList.size()];
			
			for (int i = 0; i < widgetIds.length; i++) {
				Data data = dataList.get(i);
				mDataSparseArray.put(data.getWidgetId(), data);
				widgetIds[i] = data.getWidgetId();
			}
			
			onUpdate(context, appWidgetManager, widgetIds);
		}
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] widgetIds) {
		// TODO Auto-generated method stub
		boolean updateFlag = false;
		
		if (mDataSparseArray == null || mDataSparseArray.size() == 0) {
			ArrayList<Data> dataList = DataBase.selectDatasByWidgetType(context, Constants.WIDGET_TYPE_DYNAMIC_COUNTER);
			
			if (!(dataList == null || dataList.isEmpty())) {
				if (mDataSparseArray == null) {
					mDataSparseArray = new SparseArray<Data>();
				} else {
					mDataSparseArray.clear();
				}

				widgetIds = new int[dataList.size()];
				
				for (int i = 0; i < widgetIds.length; i++) {
					Data data = dataList.get(i);
					mDataSparseArray.put(data.getWidgetId(), data);
					widgetIds[i] = data.getWidgetId();
				}
				
				updateFlag = true;
			} else {
				updateFlag = false;
			}
		} else {
			updateFlag = true;
		}
		
		if (updateFlag) {
			super.onUpdate(context, appWidgetManager, widgetIds);
			
			if (mUpdateAsyncTask != null) {
				mUpdateAsyncTask.cancel(true);
				mUpdateAsyncTask = null;
			}
			
			mUpdateAsyncTask = new UpdateAsyncTask(context, appWidgetManager, widgetIds);
			mUpdateAsyncTask.execute();
		}
	}
	
	private class UpdateAsyncTask extends AsyncTask<Void, Void, Void> {
		private static final String PREFIX_LAYOUT			= "widget_layout_dynamic_counter_";
		private static final String PREFIX_DRAWABLE			= "counter_character_";
		private static final String POSTFIX_LAYOUT			= "_number_";
		
		private static final String POSTFIX_DASH			= "_plus_minus";
		
		private Context context = null;
		private AppWidgetManager appWidgetManager = null;
		private int[] widgetIds = null;
		private String packageName = null;
		
		public UpdateAsyncTask(Context context, AppWidgetManager appWidgetManager, int[] widgetIds) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.appWidgetManager = appWidgetManager;
			this.widgetIds = widgetIds;
			packageName = context.getPackageName();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < widgetIds.length; i++) {
				Data data = mDataSparseArray.get(widgetIds[i]);
				
				int stickerId = context.getResources().getIdentifier(data.getWidgetStickerName(), Constants.DRAWABLE, packageName);
				int backgroundId = context.getResources().getIdentifier(data.getWidgetBackgroundName(), Constants.DRAWABLE, packageName);
				int alphaLv = data.getWidgetAlphaLevel();
				
				String keyword = data.getWidgetStickerName().split(Constants.SPLITER)[3];
				
				Drawable drawable = context.getResources().getDrawable(backgroundId).mutate();
				drawable.setAlpha(alphaLv);
				
				Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				drawable.setBounds(0,  0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
				drawable.draw(canvas);
				
				Calendar today = new GregorianCalendar();
				today = new GregorianCalendar(today.get(GregorianCalendar.YEAR), today.get(GregorianCalendar.MONTH), today.get(GregorianCalendar.DATE));
				Calendar criteria = new GregorianCalendar();
				criteria.setTimeInMillis(data.getWidgetCriteria());
				
				int dateCount = (int)((today.getTimeInMillis() - criteria.getTimeInMillis()) / Constants.ONE_DAY);
				String absDateCountStr = Integer.toString(Math.abs(dateCount));
				
				boolean plusMinusFlag = true;
				int possibleNumberLength = 4;
				
				if (keyword.equals(Constants.DDAY_KEYWORD_FINALTEST) || keyword.equals(Constants.DDAY_KEYWORD_NAMELABEL) || keyword.equals(Constants.DDAY_KEYWORD_TAPE)) {
					keyword = Constants.DDAY_KEYWORD_ALIGN_CENTER_LONG;
				} else if (keyword.equals(Constants.DDAY_KEYWORD_FIGHTINGGREEN) || keyword.equals(Constants.DDAY_KEYWORD_PASTELFLOG) || keyword.equals(Constants.DDAY_KEYWORD_PINKRABBIT)) {
					keyword = Constants.DDAY_KEYWORD_MARGINTOP_45;
				} else if (keyword.equals(Constants.DDAY_KEYWORD_MEET)) {
					plusMinusFlag = false;
				} else if (keyword.equals(Constants.DDAY_KEYWORD_PINKHAIR)) {
				} else {
					return null;
				}
				
				String packageName = context.getPackageName();
				RemoteViews remoteViews = new RemoteViews(packageName, context.getResources().getIdentifier(PREFIX_LAYOUT + keyword, Constants.LAYOUT, packageName));
				remoteViews.setImageViewBitmap(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + Constants.BACKGROUND, Constants.ID, packageName), bitmap);
				remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + Constants.IMAGE, Constants.ID, packageName), stickerId);
				
				if (plusMinusFlag) {
					if (dateCount > 0) {
						remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_DASH, Constants.ID, packageName), R.drawable.counter_character_plus);
					} else {
						remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_DASH, Constants.ID, packageName), R.drawable.counter_character_minus);
					}
				}
				
				for (int j = 0; j < possibleNumberLength; j++) {
					remoteViews.setViewVisibility(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), View.VISIBLE);
				}
				
				if (dateCount != 0) {
					if (absDateCountStr.length() > possibleNumberLength) {
						for (int j = 0; j < possibleNumberLength; j++) {
							remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), R.drawable.counter_character_9);
						}
					} else {
						int j = 0;
						
						for ( ; j < absDateCountStr.length(); j++) {
							remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), context.getResources().getIdentifier(PREFIX_DRAWABLE + absDateCountStr.charAt(j), Constants.DRAWABLE, packageName));
						}
						
						for ( ; j < possibleNumberLength; j++) {
							remoteViews.setViewVisibility(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), View.GONE);
						}
					}
				} else {
					if (!plusMinusFlag) {
						if (absDateCountStr.length() > possibleNumberLength) {
							for (int j = 0; j < possibleNumberLength; j++) {
								remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), R.drawable.counter_character_9);
							}
						} else {
							int j = 0;
							
							for ( ; j < absDateCountStr.length(); j++) {
								remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), context.getResources().getIdentifier(PREFIX_DRAWABLE + absDateCountStr.charAt(j), Constants.DRAWABLE, packageName));
							}
							
							for ( ; j < possibleNumberLength; j++) {
								remoteViews.setViewVisibility(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), View.GONE);
							}
						}
					} else {
						remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + 0, Constants.ID, packageName), R.drawable.counter_character_d_2nd);
						remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + 1, Constants.ID, packageName), R.drawable.counter_character_a);
						remoteViews.setImageViewResource(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + 2, Constants.ID, packageName), R.drawable.counter_character_y);
						
						for (int j = 3; j < possibleNumberLength; j++) {
							remoteViews.setViewVisibility(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + j, Constants.ID, packageName), View.GONE);
						}
					}
				}
				
				Intent intent = new Intent(context, WidgetQuickActionExecutor.class);
				if (data.getWidgetAction() != "") { intent.setAction(data.getWidgetAction()); }
				remoteViews.setOnClickPendingIntent(context.getResources().getIdentifier(PREFIX_LAYOUT + keyword + Constants.IMAGE, Constants.ID, packageName), PendingIntent.getActivity(context, 0, intent, 0));
				
				appWidgetManager.updateAppWidget(widgetIds[i], remoteViews);
			}
			
			return null;
		}
	}
}

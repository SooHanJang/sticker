package madcat.studio.widget;

import java.util.List;

import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;

public class WidgetQuickActionExecutor extends Activity {
	private static final String CONTACTS	= "com.android.contacts";
	private static final String DIALTACTS	= ".DialtactsActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_layout_transprent);
		
		Intent intent = getIntent();
		
		if (intent != null) {
			String action = intent.getAction();
			String targetActivity = null;
			boolean isExist = false;
			
			PackageManager pm = getPackageManager();
			
			if (!action.equals("")) {
				Intent loadInstalledAndExecutableApplicationIntent = new Intent(Intent.ACTION_MAIN, null);
				loadInstalledAndExecutableApplicationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
				List<ResolveInfo> installedAndExecutableApplicationList = pm.queryIntentActivities(loadInstalledAndExecutableApplicationIntent, 0);
				
				int executableListSize = installedAndExecutableApplicationList.size();
				ResolveInfo resolveInfo = null;
				
				for (int i = 0; i < executableListSize; i++) {
					resolveInfo = installedAndExecutableApplicationList.get(i);
					
					if (resolveInfo.activityInfo.packageName.equals(action)) {
						targetActivity = resolveInfo.activityInfo.targetActivity;
						isExist = true;
						break;
					}
				}
			}
			
			if (isExist) {
				Intent executorIntent = null;
				
				try {
					executorIntent = pm.getLaunchIntentForPackage(action);
					startActivity(executorIntent);
					overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				} catch (NullPointerException e) {
					Util.showToastMessage(this, getString(R.string.toast_invalid_quick_actoin));
				} catch (RuntimeException e) {
					if (action.equals(CONTACTS)) {
						if (targetActivity == null) {
							executorIntent = new Intent(Intent.ACTION_DIAL);
							startActivity(executorIntent);
							overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
						} else {
							if (targetActivity.equals(CONTACTS + DIALTACTS)) {
								executorIntent = new Intent(Intent.ACTION_VIEW);
								executorIntent.setData(Contacts.CONTENT_URI);
								startActivity(executorIntent);
								overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
							} else {
								Util.showToastMessage(this, getString(R.string.toast_invalid_quick_actoin));
							}
						}
					} else {
						Util.showToastMessage(this, getString(R.string.toast_invalid_quick_actoin));
					}
				}
			} else {
				Util.showToastMessage(this, getString(R.string.toast_not_exist_quick_action));
			}
		} else {
			Util.showToastMessage(this, getString(R.string.toast_invalid_quick_actoin));
		}
		
		finish();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() { }
}

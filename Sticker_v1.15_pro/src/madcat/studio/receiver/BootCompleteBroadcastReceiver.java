package madcat.studio.receiver;

import madcat.studio.constants.Constants;
import madcat.studio.database.DataBase;
import madcat.studio.service.WidgetUpdateService;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class BootCompleteBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if (intent != null) {
			if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
				SharedPreferences preferences = context.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
				
				if (preferences.getBoolean(Constants.FIRST_EXECUTE_FLAG, false)) {
					if (DataBase.countDatasByDynamicWidgetTypes(context) > 0) {
						Intent serviceStartIntent = new Intent(context, WidgetUpdateService.class);
						serviceStartIntent.setAction(WidgetUpdateService.SERVICE_EXECUTE_ACTION);
						context.startService(serviceStartIntent);
					}
				}
			}
		}
	}
}

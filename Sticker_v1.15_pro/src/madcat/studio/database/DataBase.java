package madcat.studio.database;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class DataBase {
	private static final String	DATABASE_NAME							= "sticker.db";
//	private static final int	DATABASE_VERSION						= 1;
	private static final String	DATABASE_WIDGET_TABLE_NAME				= "widget";
	
	private static final String	DATABASE_ATTR_ID						= "_id";
	private static final String	DATABASE_ATTR_WIDGET_ID					= "_widget_id";
	private static final String DATABASE_ATTR_WIDGET_SIZE				= "_widget_size";
	private static final String	DATABASE_ATTR_WIDGET_STICKER_NAME		= "_widget_sticker_name";
	private static final String	DATABASE_ATTR_WIDGET_BACKGROUND_NAME	= "_widget_background_name";
	private static final String DATABASE_ATTR_WIDGET_ALPHA_LEVEL		= "_widget_alpha_level";
	private static final String	DATABASE_ATTR_WIDGET_TYPE				= "_widget_type";
	private static final String	DATABASE_ATTR_WIDGET_CRITERIA			= "_widget_criteria";
	private static final String DATABASE_ATTR_WIDGET_ACTION				= "_widget_action";
	
	private static final String	CREATE_TABLE							= "CREATE TABLE " + DATABASE_WIDGET_TABLE_NAME + " (" + DATABASE_ATTR_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, " + 
																		  DATABASE_ATTR_WIDGET_ID + " INTEGER NOT NULL UNIQUE, " +
																		  DATABASE_ATTR_WIDGET_SIZE + " INTEGER NOT NULL, " +
																		  DATABASE_ATTR_WIDGET_STICKER_NAME + " TEXT NOT NULL, " +
																		  DATABASE_ATTR_WIDGET_BACKGROUND_NAME + " TEXT NOT NULL, " +
																		  DATABASE_ATTR_WIDGET_ALPHA_LEVEL + " INTEGER NOT NULL, " +
																		  DATABASE_ATTR_WIDGET_TYPE + " INTEGER NOT NULL, " +
																		  DATABASE_ATTR_WIDGET_CRITERIA + " INTEGER NOT NULL, " +
																		  DATABASE_ATTR_WIDGET_ACTION + " TEXT NOT NULL);";
	
	private static final String DROP_TABLE								= "DROP TABLE IF EXISTS " + DATABASE_WIDGET_TABLE_NAME + ";";
	
	public static boolean createDatabase(Context context) {
		SQLiteDatabase db = null;
		boolean dbFlag = false;

		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				db.execSQL(CREATE_TABLE);
				dbFlag = true;
			} catch (SQLiteException e) {
				dbFlag = false;
			} finally {
				db.close();
				db = null;
			}
		}
		
		return dbFlag;
	}
	
	public static boolean initializeDatabase(Context context) {
		SQLiteDatabase db = null;
		boolean dbFlag = false;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				db.execSQL(DROP_TABLE);
				db.execSQL(CREATE_TABLE);
				dbFlag = true;
			} catch (SQLiteException e) {
				dbFlag =false;
			} finally {
				db.close();
				db = null;
			}
		}
		
		return dbFlag;
	}

	public static boolean destroyDatabase(Context context) {
		SQLiteDatabase db = null;
		boolean dbFlag = false;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				db.execSQL(DROP_TABLE);
				dbFlag = true;
			} catch (SQLiteException e) {
				dbFlag = false;
			} finally {
				db.close();
				db = null;
			}
		}
		
		return dbFlag;
	}
	
	public static boolean insertData(Context context, Data data) {
		SQLiteDatabase db = null;
		boolean insertFlag = false;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				db.insert(DATABASE_WIDGET_TABLE_NAME, null, makeContentValue(data));
				insertFlag = true;
				printAllDatas(context);
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
				insertFlag = false;
			} finally {
				db.close();
				db = null;
			}
		}
		
		return insertFlag;
	}
	
	private static final String UPDATE_DATA = DATABASE_ATTR_WIDGET_ID + " = ";
	
	public static boolean updateData(Context context, Data data) {
		SQLiteDatabase db = null;
		boolean updateFlag = false;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				db.update(DATABASE_WIDGET_TABLE_NAME, makeContentValue(data), UPDATE_DATA + data.getWidgetId(), null);
				updateFlag = true;
				printAllDatas(context);
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
				updateFlag = false;
			} finally {
				db.close();
				db = null;
			}
		}
		
		return updateFlag;
	}
	
	private static final String DELETE_DATA = DATABASE_ATTR_WIDGET_ID + " = ";
	
	public static boolean deleteData(Context context, int widgetId) {
		SQLiteDatabase db = null;
		boolean deleteFlag = false;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				db.delete(DATABASE_WIDGET_TABLE_NAME, DELETE_DATA + widgetId, null);
				deleteFlag = true;
				printAllDatas(context);
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
				deleteFlag = false;
			} finally {
				db.close();
				db = null;
			}
		}
		
		return deleteFlag;
	}
	
//	private static final String SELECT_DATA_BY_ID = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_DRAWABLE_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
//											 	    " FROM " + DATABASE_WIDGET_TABLE_NAME + 
//											 	    " WHERE " + DATABASE_ATTR_WIDGET_ID + " = ";
	
	private static final String SELECT_DATA_BY_ID = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
													" FROM " + DATABASE_WIDGET_TABLE_NAME + 
													" WHERE " + DATABASE_ATTR_WIDGET_ID + " = ";
	
	public static Data selectData(Context context, int widgetId) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		Data selectData = null;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATA_BY_ID + widgetId, null);
				
				if (cursor.moveToNext()) {
					selectData = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
				}
				
				printData(selectData);
				return selectData;
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
				selectData = null;
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return selectData;
	}
	
	public static final String SELECT_DATAS = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
			  								  " FROM " + DATABASE_WIDGET_TABLE_NAME;
	
	public static ArrayList<Data> selectAllDatas(Context context) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		ArrayList<Data> dataList = null;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS, null);
				
				if (cursor.getCount() > 0) {
					dataList = new ArrayList<Data>();
					
					while (cursor.moveToNext()) {
						Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
						dataList.add(data);
					}
				}
				
				printDataList(dataList);
				return dataList;
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return null;
	}
	
//	private static final String SELECT_DATAS_BY_WIDGET_ID = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
//													  " FROM " + DATABASE_WIDGET_TABLE_NAME + 
//													  " WHERE " + DATABASE_ATTR_WIDGET_ID + " = ";
//	
//	public static Data selectDatasByWidgetId(Context context, int widgetId) {
//		SQLiteDatabase db = null;
//		Cursor cursor = null;
//		Data data = null;
//		
//		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
//		
//		if (db != null) {
//			try {
//				cursor = db.rawQuery(SELECT_DATAS_BY_WIDGET_ID + widgetId, null);
//				
//				if (cursor.getCount() > 0) {
//					data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
//				}
//				
//				printData(data);
//				return data;
//			} catch (SQLiteException e) {
//				Log.e("ERR", e.toString());
//			} finally {
//				cursor.close();
//				db.close();
//				cursor = null;
//				db = null;
//			}
//		}
//		
//		return null;
//	}
//	
//	public static ArrayList<Data> selectDatasByWidgetIds(Context context, int[] widgetIds) {
//		SQLiteDatabase db = null;
//		Cursor cursor = null;
//		ArrayList<Data> dataList = null;
//		
//		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
//		
//		if (db != null) {
//			try {
//				StringBuilder whereStringBulider = new StringBuilder();
//				whereStringBulider.append(widgetIds[0]);
//				for (int i = 1; i < widgetIds.length; i++)
//					whereStringBulider.append(" OR " + widgetIds[i]);
//				
//				cursor = db.rawQuery(SELECT_DATAS_BY_WIDGET_ID + whereStringBulider, null);
//				
//				if (cursor.getCount() > 0) {
//					dataList = new ArrayList<Data>();
//					
//					while (cursor.moveToNext()) {
//						Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
//						dataList.add(data);
//					}
//				}
//				
//				printDataList(dataList);
//				return dataList;
//			} catch (SQLiteException e) {
//				Log.e("ERR", e.toString());
//			} finally {
//				cursor.close();
//				db.close();
//				cursor = null;
//				db = null;
//			}
//		}
//		
//		return null;
//	}
	
	private static final String SELECT_DATAS_BY_SIZE = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
			   " FROM " + DATABASE_WIDGET_TABLE_NAME + 
			   " WHERE " + DATABASE_ATTR_WIDGET_SIZE + " = ";

	public static ArrayList<Data> selectDatasByWidgetSize(Context context, int widgetWidth, int widgetHeight) {
		return selectDatasByWidgetSize(context, widgetWidth * 10 + widgetHeight);
	}
	
	public static ArrayList<Data> selectDatasByWidgetSize(Context context, int widgetSize) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		ArrayList<Data> dataList = null;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS_BY_SIZE + widgetSize, null);
				
				if (cursor.getCount() > 0) {
					dataList = new ArrayList<Data>();
				
					while (cursor.moveToNext()) {
						Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
						dataList.add(data);
					}
				}
			
				printDataList(dataList);
				return dataList;
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return null;
	}
	
	private static final String SELECT_DATAS_BY = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
												  " FROM " + DATABASE_WIDGET_TABLE_NAME + 
												  " WHERE ";
	
	public static ArrayList<Data> selectDatasByWidgetSizeAndType(Context context, int widgetSize, int widgetType) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		ArrayList<Data> dataList = null;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS_BY + DATABASE_ATTR_WIDGET_SIZE + " = " + widgetSize + " AND " + DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType, null);
				
				if (cursor.getCount() > 0) {
					dataList = new ArrayList<Data>();
				
					while (cursor.moveToNext()) {
						Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
						dataList.add(data);
					}
				}
			
				printDataList(dataList);
				return dataList;
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return null;
	}
	
	public static ArrayList<Data> selectDatasByWidgetType(Context context, int widgetType) {
		return selectDatasByWidgetType(context, DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType + "");
	}
	
	public static ArrayList<Data> selectDatasByWidgetTypes(Context context, int widgetType1, int widgetType2) {
		return selectDatasByWidgetType(context, DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType1 + " OR " + DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType2);
	}
	
	private static ArrayList<Data> selectDatasByWidgetType(Context context, String widgetType) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		ArrayList<Data> dataList = null;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS_BY + widgetType, null);
				
				if (cursor.getCount() > 0) {
					dataList = new ArrayList<Data>();
					
					while (cursor.moveToNext()) {
						Data data = new Data(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7));
						dataList.add(data);
					}
				}
				
				printDataList(dataList);
				return dataList;
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return null;
	}
	
//	private static final String COUNT_ALL_DATAS = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
//												 " FROM " + DATABASE_WIDGET_TABLE_NAME;

	public static int countAllDatas(Context context) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		int count = 0;	
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS, null);
				count = cursor.getCount();
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return count;
	}
	
	public static int countDataByWidgetId(Context context, int widgetId) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		int count = 0;

		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS_BY + DATABASE_ATTR_WIDGET_ID + " = " + widgetId, null);
				count = cursor.getCount();
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return count;
	}
	
	public static int countDatasByWidgetType(Context context, int widgetType) {
		return countDatasByWidgetType(context, DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType + "");
	}
	
	public static int countDatasByWidgetTypes(Context context, int widgetType1, int widgetType2) {
		return countDatasByWidgetType(context, DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType1 + " OR " + DATABASE_ATTR_WIDGET_TYPE + " = " + widgetType2);
	}
	
	public static int countDatasByDynamicWidgetTypes(Context context) {
		return countDatasByWidgetType(context, DATABASE_ATTR_WIDGET_TYPE + " = " + Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12 + " OR " + DATABASE_ATTR_WIDGET_TYPE + " = " + Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24 + " OR " + DATABASE_ATTR_WIDGET_TYPE + " = " + Constants.WIDGET_TYPE_DYNAMIC_COUNTER + " OR " + DATABASE_ATTR_WIDGET_TYPE + " = " + Constants.WIDGET_TYPE_DYNAMIC_CALENDAR);
	}
	
	private static int countDatasByWidgetType(Context context, String widgetType) {
		SQLiteDatabase db = null;
		Cursor cursor = null;
		int count = 0;
		
		db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
		
		if (db != null) {
			try {
				cursor = db.rawQuery(SELECT_DATAS_BY + widgetType, null);
				count = cursor.getCount();
			} catch (SQLiteException e) {
				Log.e("ERR", e.toString());
			} finally {
				cursor.close();
				db.close();
				cursor = null;
				db = null;
			}
		}
		
		return count;
	}
	
	private static ContentValues makeContentValue(Data data) {
		ContentValues values = null;
		
		if (data != null) {
			values = new ContentValues();
			values.put(DATABASE_ATTR_WIDGET_ID, data.getWidgetId());
			values.put(DATABASE_ATTR_WIDGET_SIZE, data.getWidgetSize());
			values.put(DATABASE_ATTR_WIDGET_STICKER_NAME, data.getWidgetStickerName());
			values.put(DATABASE_ATTR_WIDGET_BACKGROUND_NAME, data.getWidgetBackgroundName());
			values.put(DATABASE_ATTR_WIDGET_ALPHA_LEVEL, data.getWidgetAlphaLevel());
			values.put(DATABASE_ATTR_WIDGET_TYPE, data.getWidgetType());
			values.put(DATABASE_ATTR_WIDGET_CRITERIA, data.getWidgetCriteria());
			values.put(DATABASE_ATTR_WIDGET_ACTION, data.getWidgetAction());
		}
		
		return values;
	}
	
	
	//PRINT METHODS
	private static final String PRINT_DATA = "SELECT " + DATABASE_ATTR_WIDGET_ID + ", " + DATABASE_ATTR_WIDGET_SIZE + ", " + DATABASE_ATTR_WIDGET_STICKER_NAME + ", " + DATABASE_ATTR_WIDGET_BACKGROUND_NAME + ", " + DATABASE_ATTR_WIDGET_ALPHA_LEVEL + ", " + DATABASE_ATTR_WIDGET_TYPE + ", " + DATABASE_ATTR_WIDGET_CRITERIA + ", " + DATABASE_ATTR_WIDGET_ACTION +
											 " FROM " + DATABASE_WIDGET_TABLE_NAME;
	
	public static final void printAllDatas(Context context) {
		if (Constants.DEV_FLAG) {
			SQLiteDatabase db = null;
			Cursor cursor = null;
			
			db = context.openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
			
			if (db != null) {
				try {
					cursor = db.rawQuery(PRINT_DATA, null);
					
					if (cursor.getCount() > 0) {
						while (cursor.moveToNext()) {
//							Util.printLogMessage("DATABASE_PRINT", "WIDGET ID : " + cursor.getInt(0) + " / WIDGET SIZE : " + cursor.getInt(1) + " / WIDGET STICKER ID : " + cursor.getString(2) + " / WIDGET BACKGROUND ID : " + cursor.getString(3) + " / WIDGET ALPHA LEVEL : " + cursor.getInt(4) + " / WIDGET TYPE : " + cursor.getInt(5) + " / WIDGET CRITERIA : " + cursor.getLong(6) + " / WIDGET ACTION : " + cursor.getString(7));
						}
					} else {
//						Util.printLogMessage("DATABASE_PRINT", "NO DATA.");
					}
				} catch (SQLiteException e) {
					Log.e("ERR", e.toString());
				} finally {
					cursor.close();
					db.close();
					cursor = null;
					db = null;
				}
			}
		}
	}
	
	public static final void printData(Data data) {
		if (Constants.DEV_FLAG) {
			if (data != null) {
//				Util.printLogMessage("DATA_PRINT", "WIDGET ID : " + data.getWidgetId() + " / WIDGET SIZE : " + data.getWidgetSize() + " / WIDGET STICKER ID : " + data.getWidgetStickerName() + " / WIDGET BACKGROUND ID : " + data.getWidgetBackgroundName() + " / WIDGET ALPHA LEVEL : " + data.getWidgetAlphaLevel() + " / WIDGET TYPE : " + data.getWidgetType() + " / WIDGET CRITERIA : " + data.getWidgetCriteria() + " / WIDGET ACTION : " + data.getWidgetAction());
			}
		}
	}
	
	public static final void printDataList(ArrayList<Data> dataList) {
		if (Constants.DEV_FLAG) {
			if (dataList != null) {
//				for (Data data : dataList) {
//					Util.printLogMessage("DATALIST_PRINT", "WIDGET ID : " + data.getWidgetId() + " / WIDGET SIZE : " + data.getWidgetSize() + " / WIDGET STICKER ID : " + data.getWidgetStickerName() + " / WIDGET BACKGROUND ID : " + data.getWidgetBackgroundName() + " / WIDGET ALPHA LEVEL : " + data.getWidgetAlphaLevel() + " / WIDGET TYPE : " + data.getWidgetType() + " / WIDGET CRITERIA : " + data.getWidgetCriteria() + " / WIDGET ACTION : " + data.getWidgetAction());
//				}
			}
		}
	}
}

package madcat.studio.database;

import madcat.studio.constants.Constants;

public class Data implements Comparable<Data> {
	private int mWidgetId = 0;
	private int mWidgetSize = 0;
	private String mWidgetStickerName = "";
	private String mWidgetBackgroundName = "";
	private int mWidgetAlphaLevel = 0;
	private int mWidgetType = 0;
	private long mWidgetCriteria = 0;
	private String mWidgetAction = "";
	
	public Data() {
		// TODO Auto-generated constructor stub
		this(0, 0, "", "", 255, 0, 0, "");
	}
	
	public Data(int widgetSize, String widgetStickerName, String widgetBackgroundName) {
		this(0, widgetSize, widgetStickerName, widgetBackgroundName, 255, 0, 0, "");
	}
	
	public Data(int widgetId, int widgetSize, String widgetStickerName, String widgetBackgroundName, int widgetAlphaLevel, int widgetType) {
		this(widgetId, widgetSize, widgetStickerName, widgetBackgroundName, widgetAlphaLevel, widgetType, 0, "");
	}

	public Data(int widgetId, int widgetWidth, int widgetHeight, String widgetStickerName, String widgetBackgroundName, int widgetAlphaLevel, int widgetType) {
		this(widgetId, widgetWidth, widgetHeight, widgetStickerName, widgetBackgroundName, widgetAlphaLevel, widgetType, 0, "");
	}
	
	public Data(int widgetId, int widgetWidth, int widgetHeight, String widgetStickerName, String widgetBackgroundName, int widgetAlphaLevel, int widgetType, long widgetCriteria, String widgetAction) {
		// TODO Auto-generated constructor stub
		this(widgetId, widgetWidth * 10 + widgetHeight, widgetStickerName, widgetBackgroundName, widgetAlphaLevel, widgetType, widgetCriteria, widgetAction);
	}
	
	public Data(int widgetId, int widgetSize, String widgetStickerName, String widgetBackgroundName, int widgetAlphaLevel, int widgetType, long widgetCriteria, String widgetAction) {
		mWidgetId = widgetId;
		mWidgetSize = widgetSize;
		mWidgetStickerName = widgetStickerName;
		mWidgetBackgroundName = widgetBackgroundName;
		mWidgetAlphaLevel = widgetAlphaLevel;
		mWidgetType = widgetType;
		mWidgetCriteria = widgetCriteria;
		mWidgetAction = widgetAction;
	}
	
	public int compareTo(Data another) {
		// TODO Auto-generated method stub
		if (mWidgetId > another.getWidgetId()) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public void		setWidgetId(int widgetId) { mWidgetId = widgetId; }
	public void		setWidgetSize(int widgetWidth, int widgetHeight) { mWidgetSize = widgetWidth * 10 + widgetHeight; }
	public void		setWidgetStickerName(String widgetStickerName) { mWidgetStickerName = widgetStickerName; }
	public void		setWidgetBackgroundName(String widgetBackgroudName) { mWidgetBackgroundName = widgetBackgroudName; }
	public void		setWidgetAlphaLevel(int widgetAlphaLevel) { mWidgetAlphaLevel = widgetAlphaLevel; }
	public void		setWidgetType(int widgetType) { mWidgetType = widgetType; }
	public void		setWidgetCriteria(long widgetCriteria) { mWidgetCriteria = widgetCriteria; }
	public void		setWidgetAction(String widgetAction) { mWidgetAction = widgetAction; }
	
	public int		getWidgetId() { return mWidgetId; }
	public int		getWidgetSize() { return mWidgetSize; }
	public String	getWidgetStickerName() { return mWidgetStickerName; }
	public String	getWidgetBackgroundName() { return mWidgetBackgroundName; }
	public int		getWidgetAlphaLevel() { return mWidgetAlphaLevel; }
	public int		getWidgetType() { return mWidgetType; }
	public long		getWidgetCriteria() { return mWidgetCriteria; }
	public String	getWidgetAction() { return mWidgetAction; }
	
	public int getWidgetWidth() {
		switch (mWidgetSize) {
		case Constants.WIDGET_SIZE_1X1:
		case Constants.WIDGET_SIZE_1X2:
		case Constants.WIDGET_SIZE_1X3:
		case Constants.WIDGET_SIZE_1X4:
			return 1;
		case Constants.WIDGET_SIZE_2X1:
		case Constants.WIDGET_SIZE_2X2:
		case Constants.WIDGET_SIZE_2X3:
		case Constants.WIDGET_SIZE_2X4:
			return 2;
		case Constants.WIDGET_SIZE_3X1:
		case Constants.WIDGET_SIZE_3X2:
		case Constants.WIDGET_SIZE_3X3:
		case Constants.WIDGET_SIZE_3X4:
			return 3;
		case Constants.WIDGET_SIZE_4X1:
		case Constants.WIDGET_SIZE_4X2:
		case Constants.WIDGET_SIZE_4X3:
		case Constants.WIDGET_SIZE_4X4:
			return 4;
		default:
			return 1;
		}
	}
	
	public int getWidgetHeight() {
		switch (mWidgetSize) {
		case Constants.WIDGET_SIZE_1X1:
		case Constants.WIDGET_SIZE_2X1:
		case Constants.WIDGET_SIZE_3X1:
		case Constants.WIDGET_SIZE_4X1:
			return 1;
		case Constants.WIDGET_SIZE_1X2:
		case Constants.WIDGET_SIZE_2X2:
		case Constants.WIDGET_SIZE_3X2:
		case Constants.WIDGET_SIZE_4X2:
			return 2;
		case Constants.WIDGET_SIZE_1X3:
		case Constants.WIDGET_SIZE_2X3:
		case Constants.WIDGET_SIZE_3X3:
		case Constants.WIDGET_SIZE_4X3:
			return 3;
		case Constants.WIDGET_SIZE_1X4:
		case Constants.WIDGET_SIZE_2X4:
		case Constants.WIDGET_SIZE_3X4:
		case Constants.WIDGET_SIZE_4X4:
			return 4;
		default:
			return 1;
		}
	}
}

package madcat.studio.dialogs;

import madcat.studio.utils.Util;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

public class DateCriteriaSetDialog extends DatePickerDialog {
	private String mTitle = null;
	
	public DateCriteriaSetDialog(Context context, String title, OnDateSetListener listener, int year, int month, int date) {
		// TODO Auto-generated constructor stub
		super(context, listener, year, month, date);
		mTitle = title;
		setTitle(mTitle);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onDateChanged(DatePicker view, int year, int month, int day) {
		// TODO Auto-generated method stub
		super.onDateChanged(view, year, month, day);
		setTitle(mTitle);
	}
	
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
	}
}

package madcat.studio.activity;

import madcat.studio.constants.Constants;
import madcat.studio.database.DataBase;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import madcat.studio.view.WidgetCustomView;
import madcat.studio.view.WidgetSideBar;
import madcat.studio.view.WidgetSelectView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public abstract class WidgetSettingActivity extends Activity {
	protected static final String TAG = "WIDGET_SETTING_ACTIVITY";

	protected Context mContext = null;
	
	protected RelativeLayout mMainLayout = null;
	protected ImageView mTitleView = null;
	
	protected WidgetSelectView mWidgetSelectView = null;
	protected WidgetCustomView mWidgetCustomView = null;
	
	/**
	 * mSideScrollBarView에 Open/Close Animation 적용해야됨. -> 적용함. 2012. 08. 13. by daniel
	 */
	protected WidgetSideBar mSideBar = null;
	
	protected boolean mSideBarFlag = true;
	
	protected static final boolean STATE_SELECT = true;
	protected static final boolean STATE_CUSTOM = false;
	protected boolean mViewState = STATE_SELECT;
	
	protected int mWidgetWidth = Constants.WIDGET_SIZE_1;
	protected int mWidgetHeight = Constants.WIDGET_SIZE_1;
	protected int mWidgetId = 0;
	protected int mWidgetType = 0;
	
	protected String mSelectedCategory = Constants.CATEGORY_ALL;
	
	protected abstract void setWidgetType();
	protected abstract void loadWidgets();
	public abstract boolean saveWidget();
	protected abstract void updateWidget();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_layout_widget_setting);
		mContext = this;
		
		Intent intent = getIntent();
		
		SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		if (pref.getBoolean(Constants.FIRST_EXECUTE_FLAG, true)) {
			ProgressDialog progressDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.progress_create_database));
			
			if (DataBase.createDatabase(mContext)) {
				Util.showToastMessage(mContext, mContext.getString(R.string.toast_database_create_success));
				SharedPreferences.Editor prefEditor = pref.edit();
				prefEditor.putBoolean(Constants.FIRST_EXECUTE_FLAG, false);
				prefEditor.commit();
			} else {
				Util.showToastMessage(mContext, mContext.getString(R.string.toast_database_create_failure));
			}
			
			if (progressDialog.isShowing())
				progressDialog.dismiss();
		}
		
		
		if (intent != null) {
			mWidgetId = getIntent().getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			
			if (mWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
				Util.showToastMessage(mContext, mContext.getString(R.string.toast_invalid_widget_msg));
				setResult(RESULT_CANCELED);
				finish();
			} else {
				mMainLayout = (RelativeLayout)findViewById(R.id.activity_layout_widget_setting_main_layout);
				mTitleView = (ImageView)findViewById(R.id.activity_layout_widget_setting_title);
				
				mWidgetSelectView = new WidgetSelectView(mContext);
				mWidgetSelectView.setParentActivity(this);
				mMainLayout.addView(mWidgetSelectView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
				
				setWidgetType();
				loadWidgets();
				
				if (mSideBarFlag) { makeSideBar(); }
			}
		} else {
			Util.showToastMessage(mContext, mContext.getString(R.string.toast_invalid_widget_msg));
			setResult(RESULT_CANCELED);
			finish();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mSideBarFlag) {
			if (mSideBar == null || !mSideBar.getAnimationFlag()) {
				if (mViewState) {		//STATE_SELECT
					setResult(RESULT_CANCELED);
					finish();
				} else {				//STATE_CUSTOM
					changeView();
				}
			}
		} else {
			if (mViewState) {		//STATE_SELECT
				setResult(RESULT_CANCELED);
				finish();
			} else {				//STATE_CUSTOM
				changeView();
			}
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	
		setResult(RESULT_CANCELED);
		finish();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (mWidgetSelectView != null) { Util.recursiveRecycle(mWidgetSelectView); }
		if (mWidgetCustomView != null) { Util.recursiveRecycle(mWidgetCustomView); }
		if (mSideBarFlag) { removeSideBar(); }
		
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	public void changeView() {
		if (mMainLayout.getChildCount() > 0) {
			mViewState = !mViewState;
			
			//View changing Animation Start.
			//Hanging Animation Flag.
			
			if (mViewState) {		//STATE_SELECT
				//set select view properties.
				if (mWidgetCustomView != null) {
					mMainLayout.removeViewInLayout(mWidgetCustomView);
					Util.recursiveRecycle(mWidgetCustomView);
					System.gc();
					mWidgetCustomView = null;
				}
				
				mTitleView.setImageResource(R.drawable.menu_mgt_image_title_list);
				
				mWidgetSelectView = new WidgetSelectView(mContext);
				mWidgetSelectView.setParentActivity(this);
				mWidgetSelectView.setSelectedCategory(mSelectedCategory);
				
				mMainLayout.addView(mWidgetSelectView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
				if (mSideBarFlag) { makeSideBar(); }
				
				setWidgetType();
				loadWidgets();
			} else {				//STATE_CUSTOM
				//set custom view properties.
				mWidgetCustomView = new WidgetCustomView(mContext);
				mWidgetCustomView.initialize();

				mWidgetCustomView.setParentActivity(this);
				mWidgetCustomView.setWidgetStickerData(mWidgetSelectView.getSelectedWidgetStickerName(), mWidgetSelectView.getSelectedWidgetBackgroundName(), mWidgetType);
				mWidgetCustomView.setWidgetSize(mWidgetWidth, mWidgetHeight);
				mWidgetCustomView.setWidgetAction("");
				mWidgetCustomView.setWidgetCriteria(0);
				
				mTitleView.setImageResource(R.drawable.menu_mgt_image_title_setting);
				
				if (mWidgetSelectView != null) {
					mMainLayout.removeViewInLayout(mWidgetSelectView);
					Util.recursiveRecycle(mWidgetSelectView);
					System.gc();
					mWidgetSelectView = null;
				}
				
				if (mSideBarFlag) { removeSideBar(); }
				mMainLayout.addView(mWidgetCustomView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
				
				if (mWidgetType == Constants.WIDGET_TYPE_DYNAMIC_COUNTER) {
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_length_4));
					mWidgetCustomView.showDateCriteriaDialog();
					mWidgetCustomView.showCountDate();
				}
			}
		}
	}
	
	public void makeSideBar() {
		mSideBar = new WidgetSideBar(mContext, mSelectedCategory);
		mSideBar.setParentActivity(this);
		
		float density = mContext.getResources().getDisplayMetrics().density;
		
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int)(120 * density), (int)(466 * density));
		layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		
		mMainLayout.addView(mSideBar, layoutParams);
	}
	
	public void removeSideBar() {
		mMainLayout.removeViewInLayout(mSideBar);
		
		Util.recursiveRecycle(mSideBar);
		System.gc();
		
		mSideBar = null;
	}
	
	public void setSelectedCategory(String category) {
		mSelectedCategory = category;
		
		mWidgetSelectView.initializeLayout();
		mWidgetSelectView.setSelectedCategory(mSelectedCategory);
		loadWidgets();
	}
	
	public void setTitleImage(int resId) {
		mTitleView.setImageResource(resId);
	}
	
	public void setSideBarVisibility(boolean visibility) {
		if (mSideBarFlag) {
			mSideBar.setSideBarVisibility(visibility);
		}		
	}
	
	public boolean getSideBarVisibility() {
		if (mSideBarFlag) {
			return mSideBar.getSideBarVisibility();
		} else {
			return false;
		}
	}
	
	public boolean getAnimationFlag() {
		if (mSideBarFlag) {
			return mSideBar.getAnimationFlag();
		} else {
			return false;
		}
	}
}

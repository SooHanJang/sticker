package madcat.studio.activity;

import madcat.studio.sticker.pro.MainActivity;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import madcat.studio.view.HelpContentView;
import madcat.studio.view.HelpIndexView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;

public class WidgetHelpActivity extends Activity {
	private Context mContext = null;
	
	private LinearLayout mHelpMainLayout = null;
	private HelpIndexView mHelpIndexView = null;
	private HelpContentView mHelpContentView = null;
	
	private LinearLayout.LayoutParams mHelpLayoutParams = null;
	
	public static final int HELP_PAGE_INDEX			= 0;
	public static final int HELP_PAGE_ENROLL		= 1;
	public static final int HELP_PAGE_MANAGEMENT	= 2;
	
	private int mPage = HELP_PAGE_INDEX;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_layout_menu_help);
		
		mContext = this;
		
		mHelpMainLayout = (LinearLayout)findViewById(R.id.activity_layout_menu_help_layout);
		mHelpIndexView = new HelpIndexView(mContext);
		mHelpContentView = new HelpContentView(mContext);
		mHelpLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
		
		mHelpIndexView.setParentActivity(this);
		mHelpContentView.setParentActivity(this);
		
		mHelpMainLayout.addView(mHelpIndexView, mHelpLayoutParams);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mPage == HELP_PAGE_INDEX) {
			startActivity(new Intent(WidgetHelpActivity.this, MainActivity.class));
			overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			finish();
		} else {
			goHelpPage(HELP_PAGE_INDEX);
		}
	}
	
	public void goHelpPage(int page) {
		mPage = page;
		
		if (mHelpMainLayout.getChildCount() > 0) {
			mHelpMainLayout.removeAllViews();
		}
		
		switch (mPage) {
		case HELP_PAGE_INDEX:
			mHelpMainLayout.addView(mHelpIndexView, mHelpLayoutParams);
			break;
		case HELP_PAGE_ENROLL:
			mHelpContentView.setHelpPage(HelpContentView.PAGE_ENROLL);
			mHelpMainLayout.addView(mHelpContentView, mHelpLayoutParams);
			break;
		case HELP_PAGE_MANAGEMENT:
			mHelpContentView.setHelpPage(HelpContentView.PAGE_MANAGEMENT);
			mHelpMainLayout.addView(mHelpContentView, mHelpLayoutParams);
			break;
		}
	}
}

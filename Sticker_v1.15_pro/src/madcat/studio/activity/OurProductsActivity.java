package madcat.studio.activity;

import java.util.ArrayList;

import madcat.studio.adapter.OurProductsAdapter;
import madcat.studio.adapter.data.OurProductData;
import madcat.studio.sticker.pro.MainActivity;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

public class OurProductsActivity extends Activity {
	private Context mContext = null;

	private ListView mOurProductsListView = null;
	private ArrayList<OurProductData> mOurProductDataList = null;
	
	private static final String PREFIX_MARKET_LINK = "market://details?id=";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_layout_menu_our_products);
		mContext = this;
		
		mOurProductsListView = (ListView)findViewById(R.id.activity_layout_our_products_listview);
		
		makeOurProductsList();
		
		OurProductsAdapter ourProductsAdapter = new OurProductsAdapter(mContext, R.layout.view_layout_our_products_list_row, mOurProductDataList);
		mOurProductsListView.setAdapter(ourProductsAdapter);
		mOurProductsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(PREFIX_MARKET_LINK + mOurProductDataList.get(position).getAddressString()));
				startActivity(intent);
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		startActivity(new Intent(OurProductsActivity.this, MainActivity.class));
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
		finish();
	}
	
	private void makeOurProductsList() {
		if (mOurProductDataList == null) {
			mOurProductDataList = new ArrayList<OurProductData>();
		} else {
			mOurProductDataList.clear();
		}
		
		//QuotesTag
		OurProductData qutoesTag = new OurProductData();
		qutoesTag.setIconDrawableId(R.drawable.our_product_icon_tag);
		qutoesTag.setLabelString(mContext.getResources().getString(R.string.our_product_quotes_label));
		qutoesTag.setExplainString(mContext.getResources().getString(R.string.our_product_quotes_explain));
		qutoesTag.setAddressString(mContext.getResources().getString(R.string.our_product_quotes_address));
		
		//Smart Timer
		OurProductData smartTimer = new OurProductData();
		smartTimer.setIconDrawableId(R.drawable.our_product_icon_timer);
		smartTimer.setLabelString(mContext.getResources().getString(R.string.our_product_timer_label));
		smartTimer.setExplainString(mContext.getResources().getString(R.string.our_product_timer_explain));
		smartTimer.setAddressString(mContext.getResources().getString(R.string.our_product_timer_address));
		
		//Etoile
		OurProductData etoile = new OurProductData();
		etoile.setIconDrawableId(R.drawable.our_product_icon_etoile);
		etoile.setLabelString(mContext.getResources().getString(R.string.our_product_etoile_label));
		etoile.setExplainString(mContext.getResources().getString(R.string.our_product_etoile_explain));
		etoile.setAddressString(mContext.getResources().getString(R.string.our_product_etoile_address));
		
		//Safe Return
		OurProductData safeReturn = new OurProductData();
		safeReturn.setIconDrawableId(R.drawable.our_product_icon_safe);
		safeReturn.setLabelString(mContext.getResources().getString(R.string.our_product_safe_label));
		safeReturn.setExplainString(mContext.getResources().getString(R.string.our_product_safe_explain));
		safeReturn.setAddressString(mContext.getResources().getString(R.string.our_product_safe_address));
		
		//All That Idea
		OurProductData allThatIdea = new OurProductData();
		allThatIdea.setIconDrawableId(R.drawable.our_product_icon_idea);
		allThatIdea.setLabelString(mContext.getResources().getString(R.string.our_product_idea_label));
		allThatIdea.setExplainString(mContext.getResources().getString(R.string.our_product_idea_explain));
		allThatIdea.setAddressString(mContext.getResources().getString(R.string.our_product_idea_address));
		
		//Pretty Battery
		OurProductData prettyBattery = new OurProductData();
		prettyBattery.setIconDrawableId(R.drawable.our_product_icon_battery);
		prettyBattery.setLabelString(mContext.getResources().getString(R.string.our_product_battery_label));
		prettyBattery.setExplainString(mContext.getResources().getString(R.string.our_product_battery_explain));
		prettyBattery.setAddressString(mContext.getResources().getString(R.string.our_product_battery_address));
		
		mOurProductDataList.add(qutoesTag);
		mOurProductDataList.add(smartTimer);
		mOurProductDataList.add(etoile);
		mOurProductDataList.add(safeReturn);
		mOurProductDataList.add(allThatIdea);
		mOurProductDataList.add(prettyBattery);
	}
}

package madcat.studio.activity;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.database.DataBase;
import madcat.studio.sticker.pro.MainActivity;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import madcat.studio.view.WidgetCustomView;
import madcat.studio.view.WidgetGalleryView;
import madcat.studio.view.WidgetListView;
import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class WidgetManagementActivity extends Activity {
	private Context mContext = null;
	
	private RelativeLayout mMainLayout = null;
	
	private ImageView mTitleView = null;
	private CheckBox mViewChangeCheckBox = null;
	
	private WidgetListView mWidgetListView = null;
	private WidgetGalleryView mWidgetGalleryView = null;
	private WidgetCustomView mWidgetCustomView = null;
	
	private ArrayList<Data> mRegisteredWidgetList = null;
	private Data mSelectedWidget = null;
	
	private static final boolean STATE_SELECT	= true;
	private static final boolean STATE_CUSTOM	= false;
	
	private boolean mViewState					= STATE_SELECT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_layout_menu_management);
		
		mMainLayout = (RelativeLayout)findViewById(R.id.activity_layout_menu_management_main_layout);
		
		mTitleView = (ImageView)findViewById(R.id.activity_layout_menu_management_title);
		mViewChangeCheckBox = (CheckBox)findViewById(R.id.activity_layout_menu_management_view_change_checkbox);
		
		mViewChangeCheckBox.setOnCheckedChangeListener(new CheckBoxEventHandler());
		
		mWidgetGalleryView = new WidgetGalleryView(mContext);
		mWidgetGalleryView.setParentActivity(this);
		mMainLayout.addView(mWidgetGalleryView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
		loadRegisteredWidgets();
	}
	
	private boolean mPauseFlag = false;
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (mPauseFlag) {
			loadRegisteredWidgets();
			mPauseFlag = false;
		}
	}
	
	protected void onPause() {
		super.onPause();
		
		mPauseFlag = true;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mViewState) {
			startActivity(new Intent(WidgetManagementActivity.this, MainActivity.class));
			overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			finish();
		} else {
			if (mViewChangeCheckBox.isChecked()) {	//LIST
				changeView(MODE_LIST);
			} else {				//GALLERY
				changeView(MODE_GALLERY);
			}
		}
	}
	
	private void loadRegisteredWidgets() {
		if (mRegisteredWidgetList == null) {
			mRegisteredWidgetList = new ArrayList<Data>();
		} else {
			mRegisteredWidgetList.clear();
		}
		
		mRegisteredWidgetList.addAll(Util.sortDataArrayList(DataBase.selectAllDatas(mContext)));
		
		if (mRegisteredWidgetList == null || mRegisteredWidgetList.isEmpty()) {
			Util.showToastMessage(mContext, mContext.getString(R.string.toast_no_data_msg));
			startActivity(new Intent(WidgetManagementActivity.this, MainActivity.class));
			overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			finish();
		} else {
			if (mViewState) {
				if (mViewChangeCheckBox.isChecked()) {
					mWidgetListView.setRegisteredWidgetList(mRegisteredWidgetList);
				} else {
					mWidgetGalleryView.loadWidgetsFromDatabase(mRegisteredWidgetList);
				}
			}
		}
	}
	
	public static final int MODE_LIST		= 0;
	public static final int MODE_GALLERY	= 1;
	public static final int MODE_CUSTOM		= 2;
	
	public void changeView(int mode) {
		boolean continueFlag = true;
		
		if (mMainLayout.getChildCount() > 0) {
			switch (mode) {
			case MODE_LIST:
				if (!mViewState) { //if previous view state is custom ...
					if (mWidgetCustomView != null) {
						mMainLayout.removeViewInLayout(mWidgetCustomView);
						Util.recursiveRecycle(mWidgetCustomView);
						System.gc();
						mWidgetCustomView = null;
					}
				} else {
					if (mViewChangeCheckBox.isChecked()) {	//if previous select view state is gallery ...
						if (mWidgetGalleryView != null) {
							mMainLayout.removeViewInLayout(mWidgetGalleryView);
							Util.recursiveRecycle(mWidgetGalleryView);
							System.gc();
							mWidgetGalleryView = null;
						}
					} else {
						continueFlag = false;
						loadRegisteredWidgets();
					}
				}
				
				if (continueFlag) {
					mTitleView.setImageResource(R.drawable.menu_mgt_image_title_list);
					mViewState = STATE_SELECT;
					mViewChangeCheckBox.setVisibility(View.VISIBLE);
					mViewChangeCheckBox.setBackgroundResource(R.drawable.com_menu_mgt_gallery_toggle);
					
					if (mWidgetListView == null) {
						mWidgetListView = new WidgetListView(mContext);
						mWidgetListView.setParentActivity(this);
					}
					
					mMainLayout.addView(mWidgetListView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
					
					loadRegisteredWidgets();
				}
				break;
			case MODE_GALLERY:
				if (!mViewState) { //if previous view state is custom ...
					if (mWidgetCustomView != null) {
						mMainLayout.removeViewInLayout(mWidgetCustomView);
						Util.recursiveRecycle(mWidgetCustomView);
						System.gc();
						mWidgetCustomView = null;
					}
				} else {
					if (!mViewChangeCheckBox.isChecked()) {	//if previous select view state is list ...
						if (mWidgetListView != null) {
							mMainLayout.removeViewInLayout(mWidgetListView);
							Util.recursiveRecycle(mWidgetListView);
							System.gc();
							mWidgetListView = null;
						}
					} else {
						continueFlag = false;
						loadRegisteredWidgets();
					}
				}
				
				if (continueFlag) {
					mTitleView.setImageResource(R.drawable.menu_mgt_image_title_list);
					mViewState = STATE_SELECT;
					mViewChangeCheckBox.setVisibility(View.VISIBLE);
					mViewChangeCheckBox.setBackgroundResource(R.drawable.com_menu_mgt_list_toggle);
					
					if (mWidgetGalleryView == null) {
						mWidgetGalleryView = new WidgetGalleryView(mContext);
						mWidgetGalleryView.setParentActivity(this);
					}
					
					mMainLayout.addView(mWidgetGalleryView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
					
					loadRegisteredWidgets();
				}
				break;
			case MODE_CUSTOM:
				if (mViewState) { //if previous view state is select ...
					if (mViewChangeCheckBox.isChecked()) {	//if previous select view state is list ...
						if (mWidgetListView != null) {
							mSelectedWidget = mWidgetListView.getSelectedWidgetData();
							mMainLayout.removeViewInLayout(mWidgetListView);
							Util.recursiveRecycle(mWidgetListView);
							System.gc();
							mWidgetListView = null;
						}
					} else {				//if previous select view state is gallery ...
						if (mWidgetGalleryView != null) {
							mSelectedWidget = mWidgetGalleryView.getSelectedWidgetData();
							mMainLayout.removeViewInLayout(mWidgetGalleryView);
							Util.recursiveRecycle(mWidgetGalleryView);
							System.gc();
							mWidgetGalleryView = null;
						}
					}
				} else {
					continueFlag = false;
				}
				
				if (continueFlag) {
					mTitleView.setImageResource(R.drawable.menu_mgt_image_title_setting);
					mViewState = STATE_CUSTOM;
					mViewChangeCheckBox.setVisibility(View.GONE);
					
					if (mWidgetCustomView == null) {
						mWidgetCustomView = new WidgetCustomView(mContext);
					}
					
					if (mSelectedWidget == null)
						mSelectedWidget = mRegisteredWidgetList.get(0);
					
					mWidgetCustomView.initialize();
					
					mWidgetCustomView.setParentActivity(this);
					mWidgetCustomView.setWidgetStickerData(mSelectedWidget.getWidgetStickerName(), mSelectedWidget.getWidgetBackgroundName(), mSelectedWidget.getWidgetType());
					mWidgetCustomView.setWidgetSize(mSelectedWidget.getWidgetWidth(), mSelectedWidget.getWidgetHeight());
					mWidgetCustomView.setWidgetAlphaLevel(mSelectedWidget.getWidgetAlphaLevel());
					mWidgetCustomView.setWidgetAction(mSelectedWidget.getWidgetAction());
					mWidgetCustomView.setWidgetCriteria(mSelectedWidget.getWidgetCriteria());
					
					mMainLayout.addView(mWidgetCustomView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
					
					if (mSelectedWidget.getWidgetType() == Constants.WIDGET_TYPE_DYNAMIC_COUNTER) {
						mWidgetCustomView.showCountDate();
					}
				}
				break;
			}
		}
	}
	
	public void saveWidget() {
		boolean saveFlag = false;
		
		switch (mWidgetCustomView.getWidgetType()) {
		case Constants.WIDGET_TYPE_STATIC:
		case Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12:
		case Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24:
		case Constants.WIDGET_TYPE_DYNAMIC_CALENDAR:
			saveFlag = true;
			break;
		case Constants.WIDGET_TYPE_DYNAMIC_COUNTER:
			String keyword = mWidgetCustomView.getSelectedWidgetStickerName().split(Constants.SPLITER)[3];
			int countDate = mWidgetCustomView.countDate();
			int countDateLength = Integer.toString(Math.abs(countDate)).length();
			
			if (keyword.equals(Constants.DDAY_KEYWORD_MEET)) {
				if (countDate < 0) {
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_not_support_minus));
				} else {
					if (countDateLength < 5) {
						saveFlag = true;
					} else {
						Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_over_counting));
					}
				}
			} else if (keyword.equals(Constants.DDAY_KEYWORD_FINALTEST) || keyword.equals(Constants.DDAY_KEYWORD_FIGHTINGGREEN) || keyword.equals(Constants.DDAY_KEYWORD_PINKRABBIT) ||
					   keyword.equals(Constants.DDAY_KEYWORD_NAMELABEL) || keyword.equals(Constants.DDAY_KEYWORD_PASTELFLOG) ||
					   keyword.equals(Constants.DDAY_KEYWORD_PINKHAIR) || keyword.equals(Constants.DDAY_KEYWORD_TAPE)) {
				if (countDateLength < 5) {
					saveFlag = true;
				} else {
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_over_counting));
				}
			}
			break;
		}
		
		if (saveFlag) {
			saveFlag = false;
			
			if (DataBase.countDataByWidgetId(mContext, mSelectedWidget.getWidgetId()) > 0) {
				mSelectedWidget.setWidgetStickerName(mWidgetCustomView.getSelectedWidgetStickerName());
				mSelectedWidget.setWidgetBackgroundName(mWidgetCustomView.getSelectedWidgetBackgroundName());
				mSelectedWidget.setWidgetAlphaLevel(mWidgetCustomView.getWidgetAlphaLevel());
				mSelectedWidget.setWidgetAction(mWidgetCustomView.getWidgetAction());
				
				if (mSelectedWidget.getWidgetType() == Constants.WIDGET_TYPE_DYNAMIC_COUNTER) {
					mSelectedWidget.setWidgetCriteria(mWidgetCustomView.getWidgetCriteria());
				}
				
				saveFlag = DataBase.updateData(mContext, mSelectedWidget);
			}
			
			if (saveFlag) {
				//DATA UPDATE SUCCESS.
				updateWidget();
			} else {
				//DATA UPDATE FAILURE.
				Util.showToastMessage(mContext, mContext.getString(R.string.toast_target_widget_not_exist_msg));
			}
			
			loadRegisteredWidgets();
			
			if (mViewChangeCheckBox.isChecked()) {
				changeView(MODE_LIST);
			} else {
				changeView(MODE_GALLERY);
			}
		}
	}
	
	private void updateWidget() {
		Intent updateWidgetIntent = null;
		
		switch (mSelectedWidget.getWidgetType()) {
		case Constants.WIDGET_TYPE_STATIC:
			switch (mSelectedWidget.getWidgetSize()) {
			case Constants.WIDGET_SIZE_1X1:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_1X1);
				break;
			case Constants.WIDGET_SIZE_1X2:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_1X2);
				break;
			case Constants.WIDGET_SIZE_1X3:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_1X3);
				break;
			case Constants.WIDGET_SIZE_1X4:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_1X4);
				break;
			case Constants.WIDGET_SIZE_2X1:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_2X1);
				break;
			case Constants.WIDGET_SIZE_2X2:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_2X2);
				break;
			case Constants.WIDGET_SIZE_2X3:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_2X3);
				break;
			case Constants.WIDGET_SIZE_2X4:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_2X4);
				break;
			case Constants.WIDGET_SIZE_3X1:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_3X1);
				break;
			case Constants.WIDGET_SIZE_3X2:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_3X2);
				break;
			case Constants.WIDGET_SIZE_3X3:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_3X3);
				break;
			case Constants.WIDGET_SIZE_3X4:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_3X4);
				break;
			case Constants.WIDGET_SIZE_4X1:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_4X1);
				break;
			case Constants.WIDGET_SIZE_4X2:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_4X2);
				break;
			case Constants.WIDGET_SIZE_4X3:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_4X3);
				break;
			case Constants.WIDGET_SIZE_4X4:
				updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_4X4);
				break;
			}
			break;
		case Constants.WIDGET_TYPE_DYNAMIC_COUNTER:
			updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_DYNAMIC_COUNTER);
			break;
		case Constants.WIDGET_TYPE_DYNAMIC_CALENDAR:
			updateWidgetIntent = new Intent(Constants.WIDGET_UPDATE_DYNAMIC_CALENDAR);
			break;
		}
		
		if (updateWidgetIntent != null) {
			updateWidgetIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mSelectedWidget.getWidgetId());
			sendBroadcast(updateWidgetIntent);
		}
	}
	
	private class CheckBoxEventHandler implements CompoundButton.OnCheckedChangeListener {
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			if (buttonView.getId() == R.id.activity_layout_menu_management_view_change_checkbox) {
				if (mViewState) {
					if (isChecked) {	//Gallery -> List
						changeView(MODE_LIST);
					} else {			//List -> Gallery
						changeView(MODE_GALLERY);
					}	
				}
			}
		}
	}
}

package madcat.studio.activity;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.database.DataBase;
import madcat.studio.service.WidgetUpdateService;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;

public class DynamicWidgetSettingActivityCounter extends WidgetSettingActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mTitleView.setImageResource(R.drawable.category_calendar);
	}
	
	@Override
	protected void setWidgetType() {
		// TODO Auto-generated method stub
		mWidgetType = Constants.WIDGET_TYPE_DYNAMIC_COUNTER;
		mWidgetSelectView.setWidgetType(mWidgetType);
		
		mSideBarFlag = false;
	}
	
	protected void loadWidgets() {
		//Widget Size, Category가 일치하는 Widget Drawable 불러와서
		//TableRow에 addView 한 뒤, TableLayout.addView(TableRow)
		//동적으로 TableRow를 TableLayout에 추가하는 작업이다.
		//Out of Memory에 유의할 것.
		mWidgetWidth = Constants.WIDGET_SIZE_2;
		mWidgetHeight = Constants.WIDGET_SIZE_1;

		mWidgetSelectView.setWidgetWidth(mWidgetWidth);
		mWidgetSelectView.setWidgetHeight(mWidgetHeight);
		mWidgetSelectView.loadWidgets();
	}
	
	@Override
	public boolean saveWidget() {
		// TODO Auto-generated method stub
		String keyword = mWidgetCustomView.getSelectedWidgetStickerName().split(Constants.SPLITER)[3];
		int countDate = mWidgetCustomView.countDate();
		int countDateLength = Integer.toString(Math.abs(countDate)).length();
		
		boolean saveFlag = false;
		
		if (keyword.equals(Constants.DDAY_KEYWORD_MEET)) {
			if (countDate < 0) {
				Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_not_support_minus));
			} else {
				if (countDateLength < 5) {
					saveFlag = true;
				} else {
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_over_counting));
				}
			}
		} else if (keyword.equals(Constants.DDAY_KEYWORD_FINALTEST) || keyword.equals(Constants.DDAY_KEYWORD_FIGHTINGGREEN) || keyword.equals(Constants.DDAY_KEYWORD_PINKRABBIT) ||
				   keyword.equals(Constants.DDAY_KEYWORD_NAMELABEL) || keyword.equals(Constants.DDAY_KEYWORD_PASTELFLOG) ||
				   keyword.equals(Constants.DDAY_KEYWORD_PINKHAIR) || keyword.equals(Constants.DDAY_KEYWORD_TAPE)) {
			if (countDateLength < 5) {
				saveFlag = true;
			} else {
				Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_over_counting));
			}
		}
		
		if (saveFlag) {
			if (DataBase.insertData(mContext, new Data(mWidgetId, mWidgetWidth, mWidgetHeight, mWidgetCustomView.getSelectedWidgetStickerName(), mWidgetCustomView.getSelectedWidgetBackgroundName(), mWidgetCustomView.getWidgetAlphaLevel(), mWidgetCustomView.getWidgetType(), mWidgetCustomView.getWidgetCriteria(), mWidgetCustomView.getWidgetAction()))) {
				//Insert Success.
				Intent intent = new Intent(Constants.WIDGET_UPDATE_DYNAMIC_COUNTER);
				intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mWidgetId);
				setResult(RESULT_OK, intent);
				
				//UPDATE WIDGES.
				updateWidget();
				DataBase.printAllDatas(mContext);
				
				Intent startServiceIntent = new Intent(WidgetUpdateService.SERVICE_EXECUTE_ACTION);
				startService(startServiceIntent);
				
				finish();
				
				return true;
			} else {
				//Insert Fail.
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	protected void updateWidget() {
		// TODO Auto-generated method stub
		Intent widgetUpdateIntent = new Intent(Constants.WIDGET_UPDATE_DYNAMIC_COUNTER);
		widgetUpdateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mWidgetId);
		sendBroadcast(widgetUpdateIntent);
	}
}

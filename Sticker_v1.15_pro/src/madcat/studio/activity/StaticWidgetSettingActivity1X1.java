package madcat.studio.activity;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.database.DataBase;
import android.appwidget.AppWidgetManager;
import android.content.Intent;

public class StaticWidgetSettingActivity1X1 extends WidgetSettingActivity {
	@Override
	protected void setWidgetType() {
		// TODO Auto-generated method stub
		mWidgetType = Constants.WIDGET_TYPE_STATIC;
		mWidgetSelectView.setWidgetType(mWidgetType);
		
		mSideBarFlag = true;
	}
	
	protected void loadWidgets() {
		//Widget Size, Category가 일치하는 Widget Drawable 불러와서
		//TableRow에 addView 한 뒤, TableLayout.addView(TableRow)
		//동적으로 TableRow를 TableLayout에 추가하는 작업이다.
		//Out of Memory에 유의할 것.
		mWidgetWidth = Constants.WIDGET_SIZE_1;
		mWidgetHeight = Constants.WIDGET_SIZE_1;

		mWidgetSelectView.setWidgetWidth(mWidgetWidth);
		mWidgetSelectView.setWidgetHeight(mWidgetHeight);
		mWidgetSelectView.loadWidgets();
	}
	
	@Override
	public boolean saveWidget() {
		// TODO Auto-generated method stub
		if (DataBase.insertData(mContext, new Data(mWidgetId, mWidgetWidth, mWidgetHeight, mWidgetCustomView.getSelectedWidgetStickerName(), mWidgetCustomView.getSelectedWidgetBackgroundName(), mWidgetCustomView.getWidgetAlphaLevel(), mWidgetCustomView.getWidgetType(), mWidgetCustomView.getWidgetCriteria(), mWidgetCustomView.getWidgetAction()))) {
			//Insert Success.
			Intent intent = new Intent(Constants.WIDGET_UPDATE_STATIC_1X1);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mWidgetId);
			setResult(RESULT_OK, intent);
			
			//UPDATE WIDGES.
			updateWidget();
			DataBase.printAllDatas(mContext);
			
			finish();
			
			return true;
		} else {
			//Insert Fail.
			return false;
		}
	}
	
	@Override
	protected void updateWidget() {
		// TODO Auto-generated method stub
		Intent widgetUpdateIntent = new Intent(Constants.WIDGET_UPDATE_STATIC_1X1);
		widgetUpdateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mWidgetId);
		sendBroadcast(widgetUpdateIntent);
	}
}

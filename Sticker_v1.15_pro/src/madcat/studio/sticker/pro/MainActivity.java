package madcat.studio.sticker.pro;

import java.util.Locale;

import madcat.studio.activity.OurProductsActivity;
import madcat.studio.activity.WidgetHelpActivity;
import madcat.studio.activity.WidgetManagementActivity;
import madcat.studio.activity.WidgetViewActivity;
import madcat.studio.constants.Constants;
import madcat.studio.database.DataBase;
import madcat.studio.service.WidgetUpdateService;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

public class MainActivity extends Activity {
	private Context mContext = null;
	private SharedPreferences mPref = null;
	
	private ImageButton mMenuWidgetLogoBtn = null, mMenuWidgetViewBtn = null, mMenuWidgetManagementBtn = null, mMenuWidgetHelpBtn = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mContext = this;
		Locale locale = mContext.getResources().getConfiguration().locale;
		
		if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
			setContentView(R.layout.activity_layout_main_ko);
			
			mMenuWidgetLogoBtn = (ImageButton)findViewById(R.id.activity_layout_main_ko_logo_btn);
			mMenuWidgetViewBtn = (ImageButton)findViewById(R.id.activity_layout_main_ko_view_widget_btn);
			mMenuWidgetManagementBtn = (ImageButton)findViewById(R.id.activity_layout_main_ko_management_widget_btn);
			mMenuWidgetHelpBtn = (ImageButton)findViewById(R.id.activity_layout_main_ko_help_widget_btn);
		} else {
			setContentView(R.layout.activity_layout_main_en);
			
			mMenuWidgetLogoBtn = (ImageButton)findViewById(R.id.activity_layout_main_en_logo_btn);
			mMenuWidgetViewBtn = (ImageButton)findViewById(R.id.activity_layout_main_en_view_widget_btn);
			mMenuWidgetManagementBtn = (ImageButton)findViewById(R.id.activity_layout_main_en_management_widget_btn);
			mMenuWidgetHelpBtn = (ImageButton)findViewById(R.id.activity_layout_main_en_help_widget_btn);
		}
		
		mPref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);

		if (mPref.getBoolean(Constants.FIRST_EXECUTE_FLAG, true)) {
	    	new MakeDataBaseAsyncTask().execute();
	    } else {
	    	if (DataBase.countDatasByDynamicWidgetTypes(mContext) > 0) {
				Intent intent = new Intent(WidgetUpdateService.SERVICE_EXECUTE_ACTION);
				startService(intent);
			}
	    }
		
		ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
		mMenuWidgetLogoBtn.setOnClickListener(buttonEventHandler);
		mMenuWidgetViewBtn.setOnClickListener(buttonEventHandler);
		mMenuWidgetManagementBtn.setOnClickListener(buttonEventHandler);
		mMenuWidgetHelpBtn.setOnClickListener(buttonEventHandler);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = null;
			
			switch (v.getId()) {
			case R.id.activity_layout_main_ko_logo_btn:
			case R.id.activity_layout_main_en_logo_btn:
				intent = new Intent(MainActivity.this, OurProductsActivity.class);
				break;
			case R.id.activity_layout_main_ko_view_widget_btn:
			case R.id.activity_layout_main_en_view_widget_btn:
				intent = new Intent(MainActivity.this, WidgetViewActivity.class);
				break;
			case R.id.activity_layout_main_ko_management_widget_btn:
			case R.id.activity_layout_main_en_management_widget_btn:
				if (DataBase.countAllDatas(mContext) > 0) {
					intent = new Intent(MainActivity.this, WidgetManagementActivity.class);
				} else {
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_no_data_msg));
				}
				break;
			case R.id.activity_layout_main_ko_help_widget_btn:
			case R.id.activity_layout_main_en_help_widget_btn:
				intent = new Intent(MainActivity.this, WidgetHelpActivity.class);
				break;
			}
			
			if (intent != null) {
				startActivity(intent);
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				finish();
			}
		}
	}
	
	private class MakeDataBaseAsyncTask extends AsyncTask<Void, Void, Void> {
    	private boolean dbFlag = false;
    	private ProgressDialog progressDialog = null;
    	
    	@Override
    	protected void onPreExecute() {
    		// TODO Auto-generated method stub
    		progressDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.progress_create_database));
    	}
    	
    	@Override
    	protected Void doInBackground(Void... params) {
    		// TODO Auto-generated method stub
    		
    		//Create DB.
    		dbFlag = DataBase.createDatabase(mContext);
    		publishProgress();
    		
    		return null;
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		// TODO Auto-generated method stub
    		if (progressDialog != null && progressDialog.isShowing()) {
    			progressDialog.dismiss();
    		}
    	}
    	
    	@Override
    	protected void onProgressUpdate(Void... values) {
    		// TODO Auto-generated method stub
    		if (dbFlag) {
    			Util.showToastMessage(mContext, mContext.getString(R.string.toast_database_create_success));
      			SharedPreferences.Editor prefEditor = mPref.edit();
      			prefEditor.putBoolean(Constants.FIRST_EXECUTE_FLAG, false);
      			prefEditor.commit();
    		} else {
    			Util.showToastMessage(mContext, mContext.getString(R.string.toast_database_create_failure));
    		}
    	}
    }
}
package madcat.studio.service;

import madcat.studio.constants.Constants;
import madcat.studio.database.DataBase;
import madcat.studio.utils.Util;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class WidgetUpdateService extends Service {
	private static final String TAG = "STICKER_APP_WIDGET_SERVICE";
	
	public static final String	SERVICE_EXECUTE_ACTION		= "madcat.studio.sticker.pro.service.execute";
	public static final String	SERVICE_UPDATE_ACTION		= "madcat.studio.sticker.pro.service.update";
	public static final String	SERVICE_TERMINATE_ACTION	= "madcat.studio.sticker.pro.service.terminate";
	
	private Context mContext = null;
	
	private WidgetUpdateBroadcastReceiver mBroadcastReceiver = null;
	
	private AlarmManager mAlarmManager = null;
	private PendingIntent mAlarmOperation = null;
	
	private boolean mImmortal = true;
	private boolean mScreenFlag = false;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		mContext = this;
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		
		if (intent == null) {
			serviceStart(SERVICE_EXECUTE_ACTION);
		} else {
			serviceStart(intent.getAction());
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		super.onStartCommand(intent, flags, startId);
		
		if (intent == null) {
			serviceStart(SERVICE_EXECUTE_ACTION);
		} else {
			serviceStart(intent.getAction());
		}
		
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		unregistryBroadcastReceiver();
		unregistryServiceRestartAlarm();
		
		if (mImmortal)
			registryServiceResurrectionAlarm();
		
		super.onDestroy();
	}

	
	private static final String WAKE_LOCK_TAG = "madcat.studio.service.pro.sticker.wakelock";
	
	private void serviceStart(String action) {
		PowerManager powerManager = (PowerManager)mContext.getSystemService(Context.POWER_SERVICE);
		WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_TAG);
		
		wakeLock.acquire();
		
		Util.printLogMessage(TAG, action);
		
		mScreenFlag = powerManager.isScreenOn();
		
		if (action.equals(SERVICE_EXECUTE_ACTION) || action.equals(SERVICE_UPDATE_ACTION)) {
			mImmortal = true;
			registryBroadcastReceiver();
			registryServiceRestartAlarm();
			updateAllDynamicWidgets();
		} else if (action.equals(SERVICE_TERMINATE_ACTION)) {
			unregistryBroadcastReceiver();
			unregistryServiceRestartAlarm();
			mImmortal = false;
			stopSelf();
		}
		
		if (wakeLock != null) {
			wakeLock.release();
			wakeLock = null;
		}
		
		if (powerManager != null) {
			powerManager = null;
		}
	}
	
	private void registryBroadcastReceiver() {
		unregistryBroadcastReceiver();
		
		if (mBroadcastReceiver == null) {
			mBroadcastReceiver = new WidgetUpdateBroadcastReceiver();
			IntentFilter intentFilter = new IntentFilter();
			
			intentFilter.addAction(Intent.ACTION_SCREEN_ON);
			intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
//			intentFilter.addAction(Intent.ACTION_TIME_TICK);
			intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
			intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
			
			intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
			
			registerReceiver(mBroadcastReceiver, intentFilter);
		}
	}
	
	private void unregistryBroadcastReceiver() {
		if (mBroadcastReceiver != null) {
			unregisterReceiver(mBroadcastReceiver);
		}
		
		mBroadcastReceiver = null;
	}
	
	private static final long RESTART_ALARM_INTERVAL = 1000 * 60 * 5;	// 5 Minute
	
	private void registryServiceRestartAlarm() {
		unregistryServiceRestartAlarm();
		
		if (mAlarmManager == null) {
			mAlarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		}
		
		if (mAlarmOperation == null) {
			mAlarmOperation = PendingIntent.getService(mContext, 0, new Intent(SERVICE_UPDATE_ACTION), PendingIntent.FLAG_UPDATE_CURRENT);
		}
		
		mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + RESTART_ALARM_INTERVAL, RESTART_ALARM_INTERVAL, mAlarmOperation);
	}
	
	private void unregistryServiceRestartAlarm() {
		if (mAlarmOperation != null) {
			mAlarmOperation.cancel();
		}
		
		if (mAlarmManager != null) {
			mAlarmManager.cancel(mAlarmOperation);
		}
		
		mAlarmOperation = null;
		mAlarmManager = null;
	}
	
	private static final long RESURRECTION_ALARM_INTERVAL = 1000 * 5;	// 5 Seconds
	
	private void registryServiceResurrectionAlarm() {
		if (mAlarmManager == null) {
			mAlarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		}
		
		PendingIntent serviceResurrectionIntent = PendingIntent.getService(mContext, 0, new Intent(SERVICE_EXECUTE_ACTION), 0);
		mAlarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + RESURRECTION_ALARM_INTERVAL, serviceResurrectionIntent);
	}
	
	//All Widget Update
	private void updateAllDynamicWidgets() {
//		updateClockWidgets();
		updateCounterWidgets();
		updateCalendarWidgets();
	}
	
//	private void updateClockWidgets() {
//		if (mScreenFlag) {
//			if (DataBase.countDatasByWidgetTypes(mContext, Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12, Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24) > 0) {
//				sendBroadcast(new Intent(Constants.WIDGET_UPDATE_DYNAMIC_CLOCK));
//			}
//		}
//	}
	
	private void updateCounterWidgets() {
		if (mScreenFlag) {
			if (DataBase.countDatasByWidgetType(mContext, Constants.WIDGET_TYPE_DYNAMIC_COUNTER) > 0) {
				sendBroadcast(new Intent(Constants.WIDGET_UPDATE_DYNAMIC_COUNTER));
			}
		}
	}
	
	private void updateCalendarWidgets() {
		if (mScreenFlag) {
			if (DataBase.countDatasByWidgetType(mContext, Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) > 0) {
				sendBroadcast(new Intent(Constants.WIDGET_UPDATE_DYNAMIC_CALENDAR));
			}
		}
	}
	
	private class WidgetUpdateBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			if (intent != null) {
				String action = intent.getAction();
				
				Util.printLogMessage(TAG, "BR : " + action);
				
				if (action.equals(Intent.ACTION_SCREEN_ON)) {
					mScreenFlag = true;
					updateAllDynamicWidgets();
				} else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
					mScreenFlag = false;
				} else {
					if (mScreenFlag) {
						if (action.equals(Intent.ACTION_TIME_TICK)) {
//							updateClockWidgets();
						} else if (action.equals(Intent.ACTION_DATE_CHANGED) || action.equals(Intent.ACTION_TIME_CHANGED) || action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
							updateAllDynamicWidgets();
							updateAllDynamicWidgets();
						}
					}
				}
			}
		}
	}
}

package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.adapter.data.OurProductData;
import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class OurProductsAdapter extends ArrayAdapter<OurProductData> {
	private Context mContext = null;
	private int mLayoutId = 0;
	private ArrayList<OurProductData> mDataList = null;
	
	private ImageView mIconView = null;
	private TextView mLabelView = null, mExplainView = null;
	
	public OurProductsAdapter(Context context, int layoutId, ArrayList<OurProductData> dataList) {
		// TODO Auto-generated constructor stub
		super(context, layoutId, dataList);
		
		mContext = context;
		mLayoutId = layoutId;
		mDataList = new ArrayList<OurProductData>(dataList);
	}
	
	public void setDataList(ArrayList<OurProductData> dataList) {
		if (mDataList == null) {
			mDataList = new ArrayList<OurProductData>(dataList);
		} else {
			mDataList.clear();
			mDataList.addAll(dataList);
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		final int selected = position;
		
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(mLayoutId, null);
		}
		
		OurProductData data = mDataList.get(selected);
		
		try {
			if (data != null) {
				mIconView = (ImageView)view.findViewById(R.id.view_layout_our_products_list_row_icon_view);
				mLabelView = (TextView)view.findViewById(R.id.view_layout_our_products_list_row_title_view);
				mExplainView = (TextView)view.findViewById(R.id.view_layout_our_products_list_row_explain_view);
				
				mIconView.setImageResource(data.getIconDrawableId());
				mLabelView.setText(data.getLabelString());
				mExplainView.setText(data.getExplainString());
				
				return view;
			}
		} catch (NullPointerException e) {
			return view;
		}
		
		return null;
	}
}

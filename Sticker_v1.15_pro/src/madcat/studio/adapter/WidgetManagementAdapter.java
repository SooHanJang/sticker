package madcat.studio.adapter;

import java.util.ArrayList;
import java.util.List;

import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WidgetManagementAdapter extends ArrayAdapter<Data> {
	private Context mContext = null;
	private int mLayoutId = 0;
	private ArrayList<Data> mDataList = null;
	
	private ImageView mWidgetPreview = null, mWidgetBackground = null;
	private TextView mWidgetSubjectText = null, mWidgetQuickActionText = null;
	
	private PackageManager mPackageManager = null;
	private List<ResolveInfo> mInstalledApplicationList = null;
	
	public WidgetManagementAdapter(Context context, int layoutId, ArrayList<Data> dataList) {
		// TODO Auto-generated constructor stub
		super(context, layoutId, dataList);
		
		mContext = context;
		mLayoutId = layoutId;
		mDataList = new ArrayList<Data>(dataList);
		
		Intent loadInstalledAndExecutableApplicationIntent = new Intent(Intent.ACTION_MAIN, null);
		loadInstalledAndExecutableApplicationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		
		mPackageManager = context.getPackageManager();
		mInstalledApplicationList = mPackageManager.queryIntentActivities(loadInstalledAndExecutableApplicationIntent, 0);
	}
	
	public void setDataList(ArrayList<Data> dataList) {
		if (mDataList == null) {
			mDataList = new ArrayList<Data>(dataList);
		} else {
			mDataList.clear();
			mDataList.addAll(dataList);
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		final int selected = position;
		
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(mLayoutId, null);
		}
		
		Data data = mDataList.get(selected);
		
		try {
			if (data != null) {
				mWidgetPreview = (ImageView)view.findViewById(R.id.view_layout_widget_management_list_row_widgetPreview);
				mWidgetBackground = (ImageView)view.findViewById(R.id.view_layout_widget_management_list_row_widgetBackground);
				
				mWidgetSubjectText = (TextView)view.findViewById(R.id.view_layout_widget_management_list_row_widgetExplain);
				mWidgetQuickActionText = (TextView)view.findViewById(R.id.view_layout_widget_management_list_row_widgetQuickAction);
				
				mWidgetPreview.setImageResource(mContext.getResources().getIdentifier(data.getWidgetStickerName(), Constants.DRAWABLE, mContext.getPackageName()));
				mWidgetPreview.setAlpha(Constants.MAX_ALPHA_VALUE);
				mWidgetBackground.setAlpha(Constants.MAX_ALPHA_VALUE);
				mWidgetBackground.setVisibility(View.VISIBLE);
				
				switch (data.getWidgetType()) {
				case Constants.WIDGET_TYPE_STATIC:
					switch (data.getWidgetSize()) {
					case Constants.WIDGET_SIZE_1X1:
						mWidgetSubjectText.setText(mContext.getString(R.string.widget_label_1x1));
						break;
					case Constants.WIDGET_SIZE_2X1:
						mWidgetSubjectText.setText(mContext.getString(R.string.widget_label_2x1));
						break;
					case Constants.WIDGET_SIZE_2X2:
						mWidgetSubjectText.setText(mContext.getString(R.string.widget_label_2x2));
						break;
					}
					break;
				case Constants.WIDGET_TYPE_DYNAMIC_CALENDAR:
					mWidgetSubjectText.setText(mContext.getString(R.string.widget_label_calendar));
					break;
				case Constants.WIDGET_TYPE_DYNAMIC_COUNTER:
					mWidgetSubjectText.setText(mContext.getString(R.string.widget_label_counter));
					break;
				}
				
				String action = data.getWidgetAction();
				
				if (action.equals("")) {
					mWidgetQuickActionText.setText(mContext.getString(R.string.view_custom_menu_quick_action_no_data));
				} else {
					ResolveInfo resolveInfo = null;
					boolean findFlag = false;
					
					for (int i = 0; i < mInstalledApplicationList.size(); i++) {
						resolveInfo = mInstalledApplicationList.get(i);
						if (resolveInfo.activityInfo.packageName.equals(data.getWidgetAction())) {
							mWidgetQuickActionText.setText(resolveInfo.loadLabel(mPackageManager).toString());
							findFlag = true;
						}
					}
					
					if (!findFlag) {
						mWidgetQuickActionText.setText(mContext.getString(R.string.view_custom_menu_quick_action_no_data));
					}
				}
				
				if (data.getWidgetType() != Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
					mWidgetBackground.setImageResource(mContext.getResources().getIdentifier(data.getWidgetBackgroundName(), Constants.DRAWABLE, mContext.getPackageName()));
					mWidgetBackground.setAlpha(data.getWidgetAlphaLevel());
				} else {
					mWidgetBackground.setVisibility(View.GONE);
					mWidgetPreview.setAlpha(data.getWidgetAlphaLevel());
				}
				
				return view;
			}
		} catch (NullPointerException e) {
			return view;
		}
		
		return null;
	}
}

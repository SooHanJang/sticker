package madcat.studio.adapter.data;


public class OurProductData {
	private int mIconDrawableId = 0;
	private String mLabelString = null;
	private String mExplainString = null;
	private String mAddressString = null;
	
	public OurProductData() {
		// TODO Auto-generated constructor stub
	}
	
	public OurProductData(int iconId, String label, String explain, String address) {
		mIconDrawableId = iconId;
		mLabelString = label;
		mExplainString = explain;
		mAddressString = address;
	}
	
	public void setIconDrawableId(int iconId) {
		mIconDrawableId = iconId;
	}
	
	public void setLabelString(String label) {
		mLabelString = label;
	}
	
	public void setExplainString(String explain) {
		mExplainString = explain;
	}
	
	public void setAddressString(String address) {
		mAddressString = address;
	}
	
	public int getIconDrawableId() {
		return mIconDrawableId;
	}
	
	public String getLabelString() {
		return mLabelString;
	}
	
	public String getExplainString() {
		return mExplainString;
	}
	
	public String getAddressString() {
		return mAddressString;
	}
}

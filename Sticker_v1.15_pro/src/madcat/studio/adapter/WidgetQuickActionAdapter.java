package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.sticker.pro.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WidgetQuickActionAdapter extends ArrayAdapter<String> {
	private Context mContext = null;
	private int mLayoutId = 0;
	
	private ArrayList<String> mActionList = null;
	private ArrayList<Drawable> mIconList = null;
	
	private ImageView mApplicationIconView = null;
	private TextView mApplicationLabelView = null;
	
	public WidgetQuickActionAdapter(Context context, int layoutId, int textViewId, ArrayList<String> actionList, ArrayList<Drawable> iconList) {
		// TODO Auto-generated constructor stub
		super(context, layoutId, actionList);
		
		mContext = context;
		mLayoutId = layoutId;
		mActionList = new ArrayList<String>();
		mIconList = new ArrayList<Drawable>();
		
		mActionList.addAll(actionList);
		mIconList.addAll(iconList);
	}
	
	public void setActionList(ArrayList<String> actionList) {
		if (mActionList == null) {
			mActionList = new ArrayList<String>();
		} else {
			mActionList.clear();
		}
		
		mActionList.addAll(actionList);
	}
	
	public void setIconList(ArrayList<Drawable> iconList) {
		if (mIconList == null) {
			mIconList = new ArrayList<Drawable>();
		} else {
			mIconList.clear();
		}
		
		mIconList.addAll(iconList);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		final int selected = position;
		
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(mLayoutId, null);
		}
		
		String applicationlabel = mActionList.get(selected);
		Drawable applicationIcon = mIconList.get(selected);
		
		if (applicationlabel != null && applicationIcon != null) {
			mApplicationIconView = (ImageView)view.findViewById(R.id.view_layout_widget_custom_quick_action_list_row_icon);
			mApplicationLabelView = (TextView)view.findViewById(R.id.view_layout_widget_custom_quick_action_list_row_label);
			
			mApplicationIconView.setImageDrawable(applicationIcon);
			mApplicationLabelView.setText(applicationlabel);
			
			return view;
		}
		
		return null;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		final int selected = position;
		
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(mLayoutId, null);
		}
		
		String applicationlabel = mActionList.get(selected);
		Drawable applicationIcon = mIconList.get(selected);
		
		if (applicationlabel != null && applicationIcon != null) {
			mApplicationIconView = (ImageView)view.findViewById(R.id.view_layout_widget_custom_quick_action_list_row_icon);
			mApplicationLabelView = (TextView)view.findViewById(R.id.view_layout_widget_custom_quick_action_list_row_label);
			
			mApplicationIconView.setImageDrawable(applicationIcon);
			mApplicationLabelView.setText(applicationlabel);
			
			return view;
		}
		
		return view;
	}
}

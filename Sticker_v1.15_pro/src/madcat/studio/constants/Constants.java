package madcat.studio.constants;

public class Constants {
	//Development's Properties.
	public static final boolean	DEV_FLAG			= false;
		
	//SHARED PREFERENCE.
	public static final String	PREFERENCE_NAME		= "sticker_perference";
	public static final String	FIRST_EXECUTE_FLAG	= "first_execute_flag";
		
	//WIDGET CONSTANTS.
	public static final int		WIDGET_SIZE_1		= 1;
	public static final int		WIDGET_SIZE_2		= 2;
	public static final int		WIDGET_SIZE_3		= 3;
	public static final int		WIDGET_SIZE_4		= 4;
	
	public static final int		WIDGET_SIZE_1X1		= WIDGET_SIZE_1 * 10 + WIDGET_SIZE_1;
	public static final int		WIDGET_SIZE_1X2		= WIDGET_SIZE_1 * 10 + WIDGET_SIZE_2;
	public static final int		WIDGET_SIZE_1X3		= WIDGET_SIZE_1 * 10 + WIDGET_SIZE_3;
	public static final int		WIDGET_SIZE_1X4		= WIDGET_SIZE_1 * 10 + WIDGET_SIZE_4;
	
	public static final int		WIDGET_SIZE_2X1		= WIDGET_SIZE_2 * 10 + WIDGET_SIZE_1;
	public static final int		WIDGET_SIZE_2X2		= WIDGET_SIZE_2 * 10 + WIDGET_SIZE_2;
	public static final int		WIDGET_SIZE_2X3		= WIDGET_SIZE_2 * 10 + WIDGET_SIZE_3;
	public static final int		WIDGET_SIZE_2X4		= WIDGET_SIZE_2 * 10 + WIDGET_SIZE_4;
	
	public static final int		WIDGET_SIZE_3X1		= WIDGET_SIZE_3 * 10 + WIDGET_SIZE_1;
	public static final int		WIDGET_SIZE_3X2		= WIDGET_SIZE_3 * 10 + WIDGET_SIZE_2;
	public static final int		WIDGET_SIZE_3X3		= WIDGET_SIZE_3 * 10 + WIDGET_SIZE_3;
	public static final int		WIDGET_SIZE_3X4		= WIDGET_SIZE_3 * 10 + WIDGET_SIZE_4;
	
	public static final int		WIDGET_SIZE_4X1		= WIDGET_SIZE_4 * 10 + WIDGET_SIZE_1;
	public static final int		WIDGET_SIZE_4X2		= WIDGET_SIZE_4 * 10 + WIDGET_SIZE_2;
	public static final int		WIDGET_SIZE_4X3		= WIDGET_SIZE_4 * 10 + WIDGET_SIZE_3;
	public static final int		WIDGET_SIZE_4X4		= WIDGET_SIZE_4 * 10 + WIDGET_SIZE_4;
	
	public static final String	WIDGET_SIZE_1X1_STR = "1x1";
	public static final String	WIDGET_SIZE_1X2_STR = "1x2";
	public static final String	WIDGET_SIZE_1X3_STR = "1x3";
	public static final String	WIDGET_SIZE_1X4_STR = "1x4";
	
	public static final String	WIDGET_SIZE_2X1_STR = "2x1";
	public static final String	WIDGET_SIZE_2X2_STR = "2x2";
	public static final String	WIDGET_SIZE_2X3_STR = "2x3";
	public static final String	WIDGET_SIZE_2X4_STR = "2x4";
	
	public static final String	WIDGET_SIZE_3X1_STR = "3x1";
	public static final String	WIDGET_SIZE_3X2_STR = "3x2";
	public static final String	WIDGET_SIZE_3X3_STR = "3x3";
	public static final String	WIDGET_SIZE_3X4_STR = "3x4";
	
	public static final String	WIDGET_SIZE_4X1_STR = "4x1";
	public static final String	WIDGET_SIZE_4X2_STR = "4x2";
	public static final String	WIDGET_SIZE_4X3_STR = "4x3";
	public static final String	WIDGET_SIZE_4X4_STR = "4x4";
	
	public static final int		WIDGET_TYPE_STATIC				= 0;
	public static final int		WIDGET_TYPE_DYNAMIC_CLOCK_12	= 1;
	public static final int		WIDGET_TYPE_DYNAMIC_CLOCK_24	= 2;
	public static final int		WIDGET_TYPE_DYNAMIC_COUNTER		= 3;
	public static final int		WIDGET_TYPE_DYNAMIC_CALENDAR	= 4;
	
	public static final String[] WIDGET_TYPE_ARRAY				= {"static", "clock12", "clock24", "counter", "calendar"};
	
	//GENERAL DRAWABLE CONSTANTS
	public static final String	SPLITER					= "_";
	
	public static final String	STICKER_IDENTIFIER		= "sticker";
	public static final String	ID						= "id";
	public static final String	DRAWABLE				= "drawable";
	public static final String	LAYOUT					= "layout";
	public static final String	BACKGROUND				= SPLITER + "background";
	public static final String	IMAGE					= SPLITER + "image";
	public static final String	PADDING					= "padding";
	
	public static final int		MAX_ALPHA_VALUE			= 255;
	
	//COLOR NAME
	public static final String	STICKER_COLOR_RED			= "";
	public static final String	STICKER_COLOR_PINK			= "";
	public static final String	STICKER_COLOR_YELLOW			= "";
	
	public static final String	BACKGROUND_COLOR_WHITE		= SPLITER + "ffffff";
	public static final String	BACKGROUND_COLOR_BLACK		= SPLITER + "000000";
	public static final String	BACKGROUND_COLOR_DARK_GREEN	= SPLITER + "1f3a39";
	
	public static final	String	DEFAULT_COLOR_GRAY			= "#888888";
	
	//CATEGORY NAME
	public static final String	CATEGORY_ALL			= "all";
	public static final String	CATEGORY_DAILY			= "daily";
	public static final String	CATEGORY_CHEER			= "cheer";
	public static final String	CATEGORY_DIET			= "diet";
	public static final String	CATEGORY_LOVE			= "love";
	public static final String	CATEGORY_STUDY			= "study";
	public static final String	CATEGORY_DYNAMIC		= "dynamic";
	public static final String	CATEGORY_DDAY			= "counter";
	public static final String	CATEGORY_CALENDAR		= "calendar";
	
	
	//D DAY COUNTER CONSTANTS.
	public static final long	ONE_DAY		= 24 * 60 * 60 * 1000;
	
	//layout : margin top 30dip , number length : 3 , support minus : true
	public static final String	DDAY_KEYWORD_MARGINTOP_45		= "margintop_45";
	public static final String	DDAY_KEYWORD_FIGHTINGGREEN		= "fightinggreen";
	public static final String	DDAY_KEYWORD_PASTELFLOG			= "pastelfrog";
	public static final String	DDAY_KEYWORD_PINKRABBIT			= "pinkrabbit";
	
	//layout : align center , number length : 4 , support minus : true
	public static final String	DDAY_KEYWORD_ALIGN_CENTER_LONG	= "align_center_long";
	public static final String	DDAY_KEYWORD_FINALTEST			= "finaltest";
	public static final String	DDAY_KEYWORD_NAMELABEL			= "namelabel";
	public static final String	DDAY_KEYWORD_TAPE				= "tape";
	
	//layout : individual , number length : 4 , support minus : false
	public static final String	DDAY_KEYWORD_MEET				= "meet";
		
	//layout : individual , number length : 4 , support minus : true
	public static final String	DDAY_KEYWORD_PINKHAIR			= "pinkhair";
	
	//WIDGET UPDATE ACTION
	public static final	String	WIDGET_UPDATE_STATIC_1X1			= "madcat.studio.sticker.pro.update.static.1x1";
	public static final	String	WIDGET_UPDATE_STATIC_1X2			= "madcat.studio.sticker.pro.update.static.1x2";
	public static final	String	WIDGET_UPDATE_STATIC_1X3			= "madcat.studio.sticker.pro.update.static.1x3";
	public static final	String	WIDGET_UPDATE_STATIC_1X4			= "madcat.studio.sticker.pro.update.static.1x4";
	
	public static final	String	WIDGET_UPDATE_STATIC_2X1			= "madcat.studio.sticker.pro.update.static.2x1";
	public static final	String	WIDGET_UPDATE_STATIC_2X2			= "madcat.studio.sticker.pro.update.static.2x2";
	public static final	String	WIDGET_UPDATE_STATIC_2X3			= "madcat.studio.sticker.pro.update.static.2x3";
	public static final	String	WIDGET_UPDATE_STATIC_2X4			= "madcat.studio.sticker.pro.update.static.2x4";
	
	public static final	String	WIDGET_UPDATE_STATIC_3X1			= "madcat.studio.sticker.pro.update.static.3x1";
	public static final	String	WIDGET_UPDATE_STATIC_3X2			= "madcat.studio.sticker.pro.update.static.3x2";
	public static final	String	WIDGET_UPDATE_STATIC_3X3			= "madcat.studio.sticker.pro.update.static.3x3";
	public static final	String	WIDGET_UPDATE_STATIC_3X4			= "madcat.studio.sticker.pro.update.static.3x4";
	
	public static final	String	WIDGET_UPDATE_STATIC_4X1			= "madcat.studio.sticker.pro.update.static.4x1";
	public static final	String	WIDGET_UPDATE_STATIC_4X2			= "madcat.studio.sticker.pro.update.static.4x2";
	public static final	String	WIDGET_UPDATE_STATIC_4X3			= "madcat.studio.sticker.pro.update.static.4x3";
	public static final	String	WIDGET_UPDATE_STATIC_4X4			= "madcat.studio.sticker.pro.update.static.4x4";

	public static final String	WIDGET_UPDATE_DYNAMIC_CLOCK			= "madcat.studio.sticker.pro.update.dynamic.clock";
	public static final String	WIDGET_UPDATE_DYNAMIC_COUNTER		= "madcat.studio.sticker.pro.update.dynamic.counter";
	public static final String	WIDGET_UPDATE_DYNAMIC_CALENDAR		= "madcat.studio.sticker.pro.update.dynamic.calendar";
}

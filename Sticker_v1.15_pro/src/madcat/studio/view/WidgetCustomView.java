package madcat.studio.view;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import madcat.studio.activity.WidgetManagementActivity;
import madcat.studio.activity.WidgetSettingActivity;
import madcat.studio.adapter.WidgetQuickActionAdapter;
import madcat.studio.constants.Constants;
import madcat.studio.dialogs.DateCriteriaSetDialog;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import madcat.studio.view.data.StickerData;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class WidgetCustomView extends LinearLayout {
	private Context mContext = null;
	
	private RelativeLayout mPreviewLayout = null;
	private ImageView mPreviewImage = null, mBackgroundImage = null;
	private ColorDrawableImageButton[] mStickerColorBtns = null, mBackgroundColorBtns = null;
	private SeekBar mAlphaLevelBar = null;
	private Spinner mQuickActionSpinner = null;
	private LinearLayout mDateCriteriaLayout = null;
	private TextView mDateCriteriaText = null;
	private ImageButton mDateCriteriaEditBtn = null, mAttachBtn = null;
	
	private int mWidgetType = Constants.WIDGET_TYPE_STATIC;

	private String mSelectedWidgetStickerName = null;
	private String mSelectedWidgetBackgroundName = null;

	//Calendar Properties
	private TextView mCalendarTitleView = null;
	private TextView mCalendarCellViews[] = null;
	
	//D Day Counter Properties
	private ImageView mDayCounterDashView = null;
	private ImageView[] mDayCounterNumberViews = null;
	
	//A MenuBar Properties
	private StickerData mStickerData = null;
	
	//B MenuBar Properties
	private String mWidgetAction = null;
	
	private int mCriteriaYear = 0;
	private int mCriteriaMonth = 0;
	private int mCriteriaDate = 0;
	
	private boolean mPlusMinusFlag = true;
	private int mPossibleNumberLength = 4;

	private int CELL_WIDTH		= 80;
	private int CELL_HEIGHT		= 100;
	
	public WidgetCustomView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.view_layout_widget_custom, this, true);
		
		mWidgetAction = mContext.getString(R.string.view_custom_menu_quick_action_no_data);
		
		GregorianCalendar today = new GregorianCalendar();
		mCriteriaYear = today.get(GregorianCalendar.YEAR);
		mCriteriaMonth = today.get(GregorianCalendar.MONTH);
		mCriteriaDate = today.get(GregorianCalendar.DATE);
		
		mPreviewLayout = (RelativeLayout)findViewById(R.id.view_layout_widget_custom_preview_layout);
		mAlphaLevelBar = (SeekBar)findViewById(R.id.view_layout_widget_custom_alpha_level);
		mQuickActionSpinner = (Spinner)findViewById(R.id.view_layout_widget_custom_quick_action);
		mDateCriteriaLayout = (LinearLayout)findViewById(R.id.view_layout_widget_custom_date_criteria_layout);
		mDateCriteriaText = (TextView)findViewById(R.id.view_layout_widget_custom_date_criteria_text);
		mDateCriteriaEditBtn = (ImageButton)findViewById(R.id.view_layout_widget_custom_date_criteria_edit_btn);
		mAttachBtn = (ImageButton)findViewById(R.id.view_layout_widget_custom_attach_btn);
		
		mStickerColorBtns = new ColorDrawableImageButton[3];
		mBackgroundColorBtns = new ColorDrawableImageButton[3];
		
		String prefix_sticker = "view_layout_widget_custom_sticker_color_";
		String prefix_background = "view_layout_widget_custom_background_color_";
		String packageName = mContext.getPackageName();
		
		ImageButtonHandler imageButtonHandler = new ImageButtonHandler();
		
		for (int i = 0; i < 3; i++) {
			mStickerColorBtns[i] = (ColorDrawableImageButton)findViewById(mContext.getResources().getIdentifier(prefix_sticker + i, Constants.ID, packageName));
			mBackgroundColorBtns[i] = (ColorDrawableImageButton)findViewById(mContext.getResources().getIdentifier(prefix_background + i, Constants.ID, packageName));
			
			mStickerColorBtns[i].setOnClickListener(imageButtonHandler);
			mBackgroundColorBtns[i].setOnClickListener(imageButtonHandler);
		}
		
		mAlphaLevelBar.setOnSeekBarChangeListener(new SeekBarEventHandler());
		
		ButtonClickEventHandler buttonClickEventHandler = new ButtonClickEventHandler();
		mDateCriteriaEditBtn.setOnClickListener(buttonClickEventHandler);
		mAttachBtn.setOnClickListener(buttonClickEventHandler);
		
		setCriteriaOnTextView();
		
		float scale = mContext.getResources().getDisplayMetrics().density;
		CELL_WIDTH = (int)(CELL_WIDTH * scale);
		CELL_HEIGHT = (int)(CELL_HEIGHT * scale);
	}
	
	public void initialize() {
		if (mPreviewImage != null)
			mPreviewImage.setImageDrawable(null);
		
		if (mBackgroundImage != null)
			mBackgroundImage.setImageDrawable(null);
		
		mAlphaLevelBar.setProgress(Constants.MAX_ALPHA_VALUE);
		
//		mPreviewImage.setAlpha(Constants.MAX_ALPHA_VALUE);
//		mBackgroundImage.setAlpha(Constants.MAX_ALPHA_VALUE);
		
		for (int i = 0; i < mStickerColorBtns.length; i++) {
			mStickerColorBtns[i].setClicked(false);
			mStickerColorBtns[i].setEnabled(true);
		}
		
		mStickerColorBtns[0].setFillColor(Constants.DEFAULT_COLOR_GRAY);
		mStickerColorBtns[1].setFillColor(Constants.DEFAULT_COLOR_GRAY);
		mStickerColorBtns[2].setFillColor(Constants.DEFAULT_COLOR_GRAY);
		
		for (int i = 0; i < mBackgroundColorBtns.length; i++) {
			mBackgroundColorBtns[i].setClicked(false);
			mBackgroundColorBtns[i].setEnabled(true);
		}
		
		mBackgroundColorBtns[0].setFillColor(Constants.DEFAULT_COLOR_GRAY);
		mBackgroundColorBtns[1].setFillColor(Constants.DEFAULT_COLOR_GRAY);
		mBackgroundColorBtns[2].setFillColor(Constants.DEFAULT_COLOR_GRAY);
		
		mCriteriaYear = 0;
		mCriteriaMonth = 0;
		mCriteriaDate = 0;
		
		mPlusMinusFlag = true;
		mPossibleNumberLength = 4;
	}
	
	private WidgetSettingActivity mParentSettingActivity = null;
	private WidgetManagementActivity mParentManagementActivity = null;
	
	public void setParentActivity(WidgetSettingActivity parentActivity) {
		mParentSettingActivity = parentActivity;
		mParentManagementActivity = null;
		
		mAttachBtn.setImageResource(R.drawable.custom_attach_button);
	}
	
	public void setParentActivity(WidgetManagementActivity parentActivity) {
		mParentManagementActivity = parentActivity;
		mParentSettingActivity = null;
		
		mAttachBtn.setImageResource(R.drawable.custom_save_button);
	}
	
	private static final String PREFIX_LAYOUT			= "widget_layout_dynamic_counter_";
	private static final String PREFIX_DRAWABLE			= "counter_character_";
	private static final String POSTFIX_LAYOUT			= "_number_";
	
	private static final String POSTFIX_DASH			= "_plus_minus";
	
	public void setWidgetStickerData(String stickerName, String backgroundName, int widgetType) {
		String[] parsedStickerName = stickerName.split(Constants.SPLITER);
		String size = parsedStickerName[1];
		String category = parsedStickerName[2];
		String keyword = parsedStickerName[3];
		
		if (mStickerData == null) {
			mStickerData = new StickerData();
		} else {
			mStickerData.clear();
		}
		
		Field[] drawables = R.drawable.class.getFields();
		String drawableName = null;
		String[] parsedDrawableName = null;
		
		if (drawables != null && drawables.length > 0) {
			for (int i = 0; i < drawables.length; i++) {
				drawableName = drawables[i].getName();
				parsedDrawableName =  drawables[i].getName().split(Constants.SPLITER);
				if (parsedDrawableName[0].equals(Constants.STICKER_IDENTIFIER) && parsedDrawableName[1].equals(size) && parsedDrawableName[2].equals(category) && parsedDrawableName[3].equals(keyword)) {
					if (drawableName.contains(Constants.BACKGROUND)) {
						mStickerData.addBackgroundName(drawableName);
					} else {
						mStickerData.addStickerName(drawableName);
					}
				}
			}
		}
		
		mSelectedWidgetStickerName = stickerName;
		mSelectedWidgetBackgroundName = backgroundName;
		
		this.setWidgetStickerData(null, widgetType);
	}
	
	private static final String CALENDAR_CELL_PREFIX	= "widget_layout_dynamic_calendar_for_preview_cell_";
	
	public void setWidgetStickerData(StickerData stickerData, int widgetType) {
		setWidgetType(widgetType);
		
		if (stickerData != null) {
			if (mStickerData != null && !mStickerData.isEmpty()) {
				mStickerData.clear();
			} else {
				mStickerData = new StickerData();
			}
			
			mStickerData.addAll(stickerData);

			mSelectedWidgetStickerName = mStickerData.getStickerName(0);
			mSelectedWidgetBackgroundName = mStickerData.getBackgroundName(0);
		}
		
		if (mWidgetType == Constants.WIDGET_TYPE_DYNAMIC_COUNTER) {
			String keyword = mSelectedWidgetStickerName.split(Constants.SPLITER)[3];
			
			if (keyword.equals(Constants.DDAY_KEYWORD_FINALTEST) || keyword.equals(Constants.DDAY_KEYWORD_NAMELABEL) || keyword.equals(Constants.DDAY_KEYWORD_TAPE)) {
				keyword = Constants.DDAY_KEYWORD_ALIGN_CENTER_LONG;
				mPlusMinusFlag = true;
				mPossibleNumberLength = 4;
			} else if (keyword.equals(Constants.DDAY_KEYWORD_FIGHTINGGREEN) || keyword.equals(Constants.DDAY_KEYWORD_PASTELFLOG) || keyword.equals(Constants.DDAY_KEYWORD_PINKRABBIT)) {
				keyword = Constants.DDAY_KEYWORD_MARGINTOP_45;
				mPlusMinusFlag = true;
				mPossibleNumberLength = 4;
			} else if (keyword.equals(Constants.DDAY_KEYWORD_MEET)) {
				mPlusMinusFlag = false;
				mPossibleNumberLength = 4;
			} else if (keyword.equals(Constants.DDAY_KEYWORD_PINKHAIR)) {
				mPlusMinusFlag = true;
				mPossibleNumberLength = 4;
			} else {
				keyword = null;
			}
			
			if (keyword != null) {
				String packageName = mContext.getPackageName();
				
				if (mPreviewLayout != null) {
					Util.recursiveRecycle(mPreviewLayout);
				}
				
				LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				inflater.inflate(mContext.getResources().getIdentifier(PREFIX_LAYOUT + keyword, Constants.LAYOUT, packageName), mPreviewLayout);
				
				mBackgroundImage = (ImageView)findViewById(mContext.getResources().getIdentifier(PREFIX_LAYOUT + keyword + Constants.BACKGROUND, Constants.ID, packageName));
				mPreviewImage = (ImageView)findViewById(mContext.getResources().getIdentifier(PREFIX_LAYOUT + keyword + Constants.IMAGE, Constants.ID, packageName));
				
				mBackgroundImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetBackgroundName, Constants.DRAWABLE, packageName));
				mPreviewImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetStickerName, Constants.DRAWABLE, packageName));
				
				if (mPlusMinusFlag) {
					mDayCounterDashView = (ImageView)findViewById(mContext.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_DASH, Constants.ID, packageName));
				}
				
				mDayCounterNumberViews = new ImageView[mPossibleNumberLength];
				
				for (int i = 0; i < mDayCounterNumberViews.length; i++) {
					mDayCounterNumberViews[i] = (ImageView)findViewById(mContext.getResources().getIdentifier(PREFIX_LAYOUT + keyword + POSTFIX_LAYOUT + i, Constants.ID, packageName));
				}
			}
		} else if (mWidgetType == Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
			String packageName = mContext.getPackageName();
			
			if (mPreviewLayout != null) {
				Util.recursiveRecycle(mPreviewLayout);
			}
			
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			inflater.inflate(R.layout.widget_layout_dynamic_calendar_for_preview, mPreviewLayout);
			
			mBackgroundImage = null;
			mPreviewImage = (ImageView)findViewById(R.id.widget_layout_dynamic_calendar_for_preview_image);
			
			mCalendarTitleView = (TextView)findViewById(R.id.widget_layout_dynamic_calendar_for_preview_title_text);
			mCalendarCellViews = new TextView[42];
			
			mPreviewImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetStickerName, Constants.DRAWABLE, mContext.getPackageName()));
			
			Calendar thisMonth = new GregorianCalendar();
			String thisYear = null;
			String[] monthArray = mContext.getResources().getStringArray(R.array.widget_calendar_month_array);
			
			Locale locale = mContext.getResources().getConfiguration().locale;
			
			if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
				thisYear = thisMonth.get(Calendar.YEAR) + mContext.getString(R.string.view_custom_menu_date_criteria_year) + monthArray[thisMonth.get(Calendar.MONTH)];
			} else {
				thisYear = monthArray[thisMonth.get(Calendar.MONTH)] + ". " + thisMonth.get(Calendar.YEAR);
			}
			
			mCalendarTitleView.setText(thisYear);
			
			if (thisMonth.get(Calendar.DATE) != 1) {
				thisMonth.set(Calendar.DATE, thisMonth.getMinimum(Calendar.DATE));
			}
			
			int thisMonthOfDayOfWeek = thisMonth.get(Calendar.DAY_OF_WEEK);
			int thisMonthOfYear = thisMonth.get(Calendar.MONTH);
			
			if (thisMonthOfDayOfWeek != 1) {
				thisMonth.add(Calendar.DATE, (thisMonthOfDayOfWeek - 1) * -1);
			} else {
				thisMonth.add(Calendar.DATE, -7);
			}
			
			for (int i = 0; i < 42; i++) {
				mCalendarCellViews[i] = (TextView)findViewById(mContext.getResources().getIdentifier(CALENDAR_CELL_PREFIX + i, Constants.ID, packageName));
				
				mCalendarCellViews[i].setText(Integer.toString(thisMonth.get(Calendar.DATE)));
				if (thisMonth.get(Calendar.MONTH) != thisMonthOfYear) {
					mCalendarCellViews[i].setTextColor(mContext.getResources().getColor(R.color.gray));
				} else {
					if (thisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						mCalendarCellViews[i].setTextColor(mContext.getResources().getColor(R.color.red));
					} else if (thisMonth.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)  {
						mCalendarCellViews[i].setTextColor(mContext.getResources().getColor(R.color.blue));
					} else {
						mCalendarCellViews[i].setTextColor(mContext.getResources().getColor(R.color.black));
					}
				}
				
				thisMonth.add(Calendar.DATE, 1);
			}
		} else {
			mPreviewImage = (ImageView)findViewById(R.id.view_layout_widget_custom_preview);
			mBackgroundImage = (ImageView)findViewById(R.id.view_layout_widget_custom_background);
			
			mPreviewImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetStickerName, Constants.DRAWABLE, mContext.getPackageName()));
		}
		
		int stickerDataSize = mStickerData.getStickerNameListSize();
		
		if (stickerDataSize == 1) {
			mStickerColorBtns[0].setFillColor(mStickerData.getStickerColor(mContext, 0));
			mStickerColorBtns[0].setClicked(true);
			mStickerColorBtns[0].setEnabled(true);
			
			for (int i = 1; i < mStickerColorBtns.length; i++) {
				mStickerColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
				mStickerColorBtns[i].setClicked(false);
				mStickerColorBtns[i].setEnabled(false);
			}
		} else {
			mStickerColorBtns[0].setFillColor(mStickerData.getStickerColor(mContext, 0));
			mStickerColorBtns[0].setClicked(true);
			mStickerColorBtns[0].setEnabled(true);
			
			for (int i = 1; i < stickerDataSize; i++) {
				try {
					mStickerColorBtns[i].setFillColor(mStickerData.getStickerColor(mContext, i));
				} catch (NotFoundException e) {
					mStickerColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
				}
				
				mStickerColorBtns[i].setClicked(false);
				mStickerColorBtns[i].setEnabled(true);
			}
			
			for (int i = stickerDataSize; i < mStickerColorBtns.length; i++) {
				mStickerColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
				mStickerColorBtns[i].setClicked(false);
				mStickerColorBtns[i].setEnabled(false);
			}
		}
		
		String fixedBackgroundColor = "";
		
		try {
			fixedBackgroundColor = mSelectedWidgetStickerName.split(Constants.SPLITER)[5];
		} catch (ArrayIndexOutOfBoundsException e) {
			fixedBackgroundColor = "";
		}
		
		if (mWidgetType != Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
			mBackgroundImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetBackgroundName, Constants.DRAWABLE, mContext.getPackageName()));
			int backgroundDataSize = mStickerData.getBackgroundNameListSize();
			
			if (backgroundDataSize == 1) {
				mBackgroundColorBtns[0].setFillColor(mStickerData.getBackgroundColor(mContext, 0));
				mBackgroundColorBtns[0].setClicked(true);
				mBackgroundColorBtns[0].setEnabled(true);
				
				for (int i = 1; i < mBackgroundColorBtns.length; i++) {
					mBackgroundColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
					mBackgroundColorBtns[i].setClicked(false);
					mBackgroundColorBtns[i].setEnabled(false);
				}
			} else {
				mBackgroundColorBtns[0].setFillColor(mStickerData.getBackgroundColor(mContext, 0));
				mBackgroundColorBtns[0].setClicked(true);
				mBackgroundColorBtns[0].setEnabled(true);
				
				for (int i = 1; i < backgroundDataSize; i++) {
					try {
						mBackgroundColorBtns[i].setFillColor(mStickerData.getBackgroundColor(mContext, i));
					} catch (NotFoundException e) {
						mBackgroundColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
					}

					mBackgroundColorBtns[i].setClicked(false);
					mBackgroundColorBtns[i].setEnabled(true);
				}
				
				for (int i = backgroundDataSize; i < mBackgroundColorBtns.length; i++) {
					mBackgroundColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
					mBackgroundColorBtns[i].setClicked(false);
					mBackgroundColorBtns[i].setEnabled(false);
				}
			}	
			
			if (fixedBackgroundColor != "") {
				int fixedBackgroundColorIndex = 0;
				for (int i = 0; i < mStickerData.getBackgroundNameListSize(); i++) {
					if (mStickerData.getBackgroundColor(mContext, i).replace("#", "").equals(fixedBackgroundColor)) {
						fixedBackgroundColorIndex = i;
						i = mStickerData.getBackgroundNameListSize();
					}
				}
				
				for (int i = 0; i < mBackgroundColorBtns.length; i++) {
					if (fixedBackgroundColorIndex == i) {
						mBackgroundColorBtns[i].setClicked(true);
						mBackgroundColorBtns[i].setEnabled(true);
					} else {
						mBackgroundColorBtns[i].setClicked(false);
						mBackgroundColorBtns[i].setEnabled(false);
					}
				}
			} else {
				for (int i = 0; i < mStickerData.getBackgroundNameListSize(); i++) {
					if (mStickerData.getBackgroundName(i).equals(mSelectedWidgetBackgroundName)) {
						mBackgroundColorBtns[i].setClicked(true);
						mBackgroundColorBtns[i].setEnabled(true);
					} else {
						mBackgroundColorBtns[i].setClicked(false);
						mBackgroundColorBtns[i].setEnabled(true);
					}
				}
			}
		} else {
			for (int i = 0; i < mBackgroundColorBtns.length; i++) {
				mBackgroundColorBtns[i].setFillColor(Constants.DEFAULT_COLOR_GRAY);
				mBackgroundColorBtns[i].setClicked(false);
				mBackgroundColorBtns[i].setEnabled(false);
			}
		}
	}
	
	public void setWidgetSize(int widgetWidth, int widgetHeight) {
		LinearLayout.LayoutParams layoutParams = null;
		
		if (mWidgetType == Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
			layoutParams = new LinearLayout.LayoutParams(CELL_WIDTH * widgetWidth, (int)(CELL_HEIGHT * 1.5f));
		} else {
			if (widgetWidth > 2) { widgetWidth = 2; }
			widgetHeight = 1;
			
			layoutParams = new LinearLayout.LayoutParams(CELL_WIDTH * widgetWidth, CELL_HEIGHT * widgetHeight);
		}
		
		layoutParams.setMargins(0, 0, 0, 10);
		mPreviewLayout.setLayoutParams(layoutParams);
		mPreviewLayout.setGravity(Gravity.CENTER);
	}
	
	public void setWidgetType(int widgetType) {
		mWidgetType = widgetType;
		
		switch (widgetType) {
		case Constants.WIDGET_TYPE_STATIC:
		case Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12:
		case Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24:
		case Constants.WIDGET_TYPE_DYNAMIC_CALENDAR:
			mDateCriteriaLayout.setVisibility(View.GONE);
			break;
		case Constants.WIDGET_TYPE_DYNAMIC_COUNTER:
			mDateCriteriaLayout.setVisibility(View.VISIBLE);
			break;
		}
	}
	
	public void setWidgetAlphaLevel(int alphaLevel) {
		mAlphaLevelBar.setProgress(alphaLevel);
	}
	
	public void setWidgetAction(String widgetAction) {
		mWidgetAction = widgetAction;
		new LoadInstalledAndExecutableAsyncTask().execute();
	}
	
	public void setWidgetCriteria(long widgetCriteria) {
		Calendar criteria = new GregorianCalendar();
		
		if (widgetCriteria != 0) {
			criteria.setTimeInMillis(widgetCriteria);
		}
		
		mCriteriaYear = criteria.get(GregorianCalendar.YEAR);
		mCriteriaMonth = criteria.get(GregorianCalendar.MONTH);
		mCriteriaDate = criteria.get(GregorianCalendar.DATE);
		
		setCriteriaOnTextView();
	}
	
	public String getSelectedWidgetStickerName() {
		return mSelectedWidgetStickerName;
	}
	
	public String getSelectedWidgetBackgroundName() {
		return mSelectedWidgetBackgroundName;
	}
	
	public int getWidgetType() {
		return mWidgetType;
	}
	
	public int getWidgetAlphaLevel() {
		return mAlphaLevelBar.getProgress();
	}
	
	public String getWidgetAction() {
		return mWidgetAction;
	}
	
	public long getWidgetCriteria() {
		Calendar criteria = new GregorianCalendar(mCriteriaYear, mCriteriaMonth, mCriteriaDate);
		return criteria.getTimeInMillis();
	}
	
	public void showDateCriteriaDialog() {
		DateCriteriaSetDialog dateCriteriaPickDialog = new DateCriteriaSetDialog(mContext, mContext.getString(R.string.view_custom_menu_date_criteria_dialog_title), new CriteriaSetEventHandler(), mCriteriaYear, mCriteriaMonth, mCriteriaDate);
		dateCriteriaPickDialog.show();
	}
	
	public int countDate() {
		Calendar today = new GregorianCalendar();
		today = new GregorianCalendar(today.get(GregorianCalendar.YEAR), today.get(GregorianCalendar.MONTH), today.get(GregorianCalendar.DATE));
		Calendar criteria = new GregorianCalendar(mCriteriaYear, mCriteriaMonth, mCriteriaDate);
		
		return (int)((today.getTimeInMillis() - criteria.getTimeInMillis()) / Constants.ONE_DAY);
	}
	
	public void showCountDate() {
		String packageName = mContext.getPackageName();
		
		int dateCount = countDate();
		String absDateCountStr = Integer.toString(Math.abs(dateCount));
		
		if (mPlusMinusFlag) {
			mDayCounterDashView.setVisibility(View.VISIBLE);
			if (dateCount > 0) {
				mDayCounterDashView.setImageResource(R.drawable.counter_character_plus);
			} else {
				mDayCounterDashView.setImageResource(R.drawable.counter_character_minus);
			}
		}

		for (int i = 0; i < mDayCounterNumberViews.length; i++) {
			mDayCounterNumberViews[i].setVisibility(View.VISIBLE);
		}
		
		if (dateCount != 0) {
			if (absDateCountStr.length() > mPossibleNumberLength) {
				for (int i = 0; i < mPossibleNumberLength; i++) {
					mDayCounterNumberViews[i].setImageResource(R.drawable.counter_character_9);
				}
			} else {
				int i = 0;
				for ( ; i < absDateCountStr.length(); i++) {
					mDayCounterNumberViews[i].setImageResource(mContext.getResources().getIdentifier(PREFIX_DRAWABLE + absDateCountStr.charAt(i), Constants.DRAWABLE, packageName));
				}
				
				for ( ; i < mPossibleNumberLength; i++) {
					mDayCounterNumberViews[i].setVisibility(View.GONE);
				}
			}
		} else {
			if (!mPlusMinusFlag) {
				if (absDateCountStr.length() > mPossibleNumberLength) {
					for (int i = 0; i < mPossibleNumberLength; i++) {
						mDayCounterNumberViews[i].setImageResource(R.drawable.counter_character_9);
					}
				} else {
					int i = 0;
					for ( ; i < absDateCountStr.length(); i++) {
						mDayCounterNumberViews[i].setImageResource(mContext.getResources().getIdentifier(PREFIX_DRAWABLE + absDateCountStr.charAt(i), Constants.DRAWABLE, packageName));
					}
					
					for ( ; i < mPossibleNumberLength; i++) {
						mDayCounterNumberViews[i].setVisibility(View.GONE);
					}
				}
			} else {
				mDayCounterNumberViews[0].setImageResource(R.drawable.counter_character_d_2nd);
				mDayCounterNumberViews[1].setImageResource(R.drawable.counter_character_a);
				mDayCounterNumberViews[2].setImageResource(R.drawable.counter_character_y);
				
				for (int i = 3 ; i < mPossibleNumberLength; i++) {
					mDayCounterNumberViews[i].setVisibility(View.GONE);
				}
			}
		}
	}
	
	private void saveWidget() {
		if (mParentSettingActivity != null) {
			mParentSettingActivity.saveWidget();
		} else {
			mParentManagementActivity.saveWidget();
		}
	}
	
	private class ImageButtonHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			boolean flag = true;
			
			switch (v.getId()) {
			case R.id.view_layout_widget_custom_sticker_color_0:
				if (!mStickerColorBtns[0].isClicked()) {
					mStickerColorBtns[0].setClicked(true);
					mStickerColorBtns[1].setClicked(false);
					mStickerColorBtns[2].setClicked(false);
					mSelectedWidgetStickerName = mStickerData.getStickerName(0);
					flag = true;
				}
				break;
			case R.id.view_layout_widget_custom_sticker_color_1:
				if (!mStickerColorBtns[1].isClicked()) {
					mStickerColorBtns[0].setClicked(false);
					mStickerColorBtns[1].setClicked(true);
					mStickerColorBtns[2].setClicked(false);
					mSelectedWidgetStickerName = mStickerData.getStickerName(1);
					flag = true;
				}
				break;
			case R.id.view_layout_widget_custom_sticker_color_2:
				if (!mStickerColorBtns[2].isClicked()) {
					mStickerColorBtns[0].setClicked(false);
					mStickerColorBtns[1].setClicked(false);
					mStickerColorBtns[2].setClicked(true);
					mSelectedWidgetStickerName = mStickerData.getStickerName(2);
					flag = true;
				}
				break;
			case R.id.view_layout_widget_custom_background_color_0:
				if (!mBackgroundColorBtns[0].isClicked()) {
					mBackgroundColorBtns[0].setClicked(true);
					mBackgroundColorBtns[1].setClicked(false);
					mBackgroundColorBtns[2].setClicked(false);
					mSelectedWidgetBackgroundName = mStickerData.getBackgroundName(0);
					flag = false;
				}
				break;
			case R.id.view_layout_widget_custom_background_color_1:
				if (!mBackgroundColorBtns[1].isClicked()) {
					mBackgroundColorBtns[0].setClicked(false);
					mBackgroundColorBtns[1].setClicked(true);
					mBackgroundColorBtns[2].setClicked(false);
					mSelectedWidgetBackgroundName = mStickerData.getBackgroundName(1);
					flag = false;
				}
				break;
			case R.id.view_layout_widget_custom_background_color_2:
				if (!mBackgroundColorBtns[2].isClicked()) {
					mBackgroundColorBtns[0].setClicked(false);
					mBackgroundColorBtns[1].setClicked(false);
					mBackgroundColorBtns[2].setClicked(true);
					mSelectedWidgetBackgroundName = mStickerData.getBackgroundName(2);
					flag = false;
				}
				break;
			}
			
			if (flag) {
				mPreviewImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetStickerName, Constants.DRAWABLE, mContext.getPackageName()));
				
				if (mStickerData.getBackgroundNameListSize() == 1) {
					mBackgroundColorBtns[0].setClicked(true);
					mBackgroundColorBtns[0].setEnabled(true);
					
					for (int i = 1; i < mBackgroundColorBtns.length; i++) {
						mBackgroundColorBtns[i].setClicked(false);
						mBackgroundColorBtns[i].setEnabled(false);
					}
				} else {
					try {
						String fixedBackgroundColor = mSelectedWidgetStickerName.split(Constants.SPLITER)[5];
						int fixedBackgroundColorIndex = 0;
						for (int i = 0; i < mStickerData.getBackgroundNameListSize(); i++) {
							if (mStickerData.getBackgroundColor(mContext, i).replace("#", "").equals(fixedBackgroundColor)) {
								i = mStickerData.getBackgroundNameListSize();
							}
						}
						
						mSelectedWidgetBackgroundName = mStickerData.getBackgroundColor(mContext, fixedBackgroundColorIndex);
						
						for (int i = 0; i < mBackgroundColorBtns.length; i++) {
							if (fixedBackgroundColorIndex == i) {
								mBackgroundColorBtns[i].setClicked(true);
								mBackgroundColorBtns[i].setEnabled(true);
							} else {
								mBackgroundColorBtns[i].setEnabled(false);
								mBackgroundColorBtns[i].setEnabled(false);
							}
						}
						
						for (int i = 0; i < mStickerData.getBackgroundNameListSize(); i++) {
							String backgroundName = mStickerData.getBackgroundName(i);
							
							if (backgroundName.contains(fixedBackgroundColor)) {
								mSelectedWidgetBackgroundName = backgroundName;
								
								break;
							}
						}
					} catch (ArrayIndexOutOfBoundsException e) {
						for (int i = 0; i < mStickerData.getBackgroundNameListSize(); i++) {
							if (mStickerData.getBackgroundName(i).equals(mSelectedWidgetBackgroundName)) {
								mBackgroundColorBtns[i].setClicked(true);
								mBackgroundColorBtns[i].setEnabled(true);
							} else {
								mBackgroundColorBtns[i].setClicked(false);
								mBackgroundColorBtns[i].setEnabled(true);
							}
						}
					}
				}
			}
			
			mBackgroundImage.setImageResource(mContext.getResources().getIdentifier(mSelectedWidgetBackgroundName, Constants.DRAWABLE, mContext.getPackageName()));
		}
	}
	
	private class SeekBarEventHandler implements SeekBar.OnSeekBarChangeListener {
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			// TODO Auto-generated method stub
			if (mWidgetType == Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
				mPreviewImage.setAlpha(progress);
			} else {
				mBackgroundImage.setAlpha(progress);
			}
		}
		
		public void onStartTrackingTouch(SeekBar seekBar) { }
		public void onStopTrackingTouch(SeekBar seekBar) { }
	}

	private class ButtonClickEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.view_layout_widget_custom_date_criteria_edit_btn:
				showDateCriteriaDialog();
				break;
			case R.id.view_layout_widget_custom_attach_btn:
				saveWidget();
				break;
			}
		}
	}
	
	private class CriteriaSetEventHandler implements DatePickerDialog.OnDateSetListener {
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			// TODO Auto-generated method stub
			int previousYear = mCriteriaYear;
			int previousMonth = mCriteriaMonth;
			int previousDate = mCriteriaDate;
			
			mCriteriaYear = year;
			mCriteriaMonth = monthOfYear;
			mCriteriaDate = dayOfMonth;
			
			int countDate = countDate();
			boolean isContinue = true;
			
			if (!mPlusMinusFlag) {
				if (countDate < 0) {
					mCriteriaYear = previousYear;
					mCriteriaMonth = previousMonth;
					mCriteriaDate = previousDate;
					isContinue = false;
					
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_not_support_minus));
				}
			}
			
			if (isContinue) {
				if (Integer.toString(Math.abs(countDate)).length() > mPossibleNumberLength) {
					mCriteriaYear = previousYear;
					mCriteriaMonth = previousMonth;
					mCriteriaDate = previousDate;
					
					Util.showToastMessage(mContext, mContext.getString(R.string.toast_dday_counter_over_counting));
				} else {
					setCriteriaOnTextView();
					showCountDate();
				}
			}
		}
	}
	
	private void setCriteriaOnTextView() {
		mDateCriteriaText.setText(mCriteriaYear + mContext.getString(R.string.view_custom_menu_date_criteria_year) +
								 (mCriteriaMonth + 1) + mContext.getString(R.string.view_custom_menu_date_criteria_month) +
								  mCriteriaDate + mContext.getString(R.string.view_custom_menu_date_criteria_date));
	}
	
	private ArrayList<String> mInstalledAndExcutableApplicationPackageNameList = null;
	
	private class LoadInstalledAndExecutableAsyncTask extends AsyncTask<Void, Void, Void> {
		private ArrayList<String> installedAndExecutableApplicationLabelList = null;
		private ArrayList<Drawable> installedAndExecutableApplicationIconList = null;
		private WidgetQuickActionAdapter quickActionSpinnerAdapter = null;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			if (mInstalledAndExcutableApplicationPackageNameList == null) {
				mInstalledAndExcutableApplicationPackageNameList = new ArrayList<String>();
			} else {
				mInstalledAndExcutableApplicationPackageNameList.clear();
			}
			
			installedAndExecutableApplicationLabelList = new ArrayList<String>();
			installedAndExecutableApplicationIconList = new ArrayList<Drawable>();
			
			publishProgress();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			PackageManager pm = mContext.getPackageManager();

			Intent loadInstalledLauncherApplicationIntent = new Intent(Intent.ACTION_MAIN, null);
			loadInstalledLauncherApplicationIntent.addCategory(Intent.CATEGORY_HOME);
			List<ResolveInfo> installedLauncherApplicationList = pm.queryIntentActivities(loadInstalledLauncherApplicationIntent, 0);
			HashSet<String> installedLauncherApplicationHashSet = new HashSet<String>();
			
			int launcherListSize = installedLauncherApplicationList.size();
			for (int i = 0; i < launcherListSize; i++) {
				installedLauncherApplicationHashSet.add(installedLauncherApplicationList.get(i).loadLabel(pm).toString());
			}
			
			Intent loadInstalledAndExecutableApplicationIntent = new Intent(Intent.ACTION_MAIN, null);
			loadInstalledAndExecutableApplicationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			List<ResolveInfo> installedAndExecutableApplicationList = pm.queryIntentActivities(loadInstalledAndExecutableApplicationIntent, 0);
			
			installedAndExecutableApplicationLabelList.clear();
			installedAndExecutableApplicationIconList.clear();
			
			installedAndExecutableApplicationLabelList.add(mContext.getString(R.string.view_custom_menu_quick_action_no_data));
			installedAndExecutableApplicationIconList.add(mContext.getResources().getDrawable(R.drawable.custom_spinner_no_quick_action));
			mInstalledAndExcutableApplicationPackageNameList.add("");
			
			int executableListSize = installedAndExecutableApplicationList.size();
			String label = null;
			ResolveInfo resolveInfo = null;
			for (int i = 0; i < executableListSize; i++) {
				resolveInfo = installedAndExecutableApplicationList.get(i);
				label = resolveInfo.loadLabel(pm).toString();
				
				if (!installedLauncherApplicationHashSet.contains(label)) {
					installedAndExecutableApplicationLabelList.add(resolveInfo.loadLabel(pm).toString());
					installedAndExecutableApplicationIconList.add(resolveInfo.loadIcon(pm));
					mInstalledAndExcutableApplicationPackageNameList.add(resolveInfo.activityInfo.packageName);
				}
			}
			
			publishProgress();
			
			return null;
		}
		
//		@Override
//		protected void onPostExecute(Void result) {
//			// TODO Auto-generated method stub
//			installedAndExecutableApplicationLabelList.clear();
//			installedAndExecutableApplicationIconList.clear();
//			
//			installedAndExecutableApplicationLabelList = null;
//			installedAndExecutableApplicationIconList = null;
//		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			if (installedAndExecutableApplicationLabelList.isEmpty() && installedAndExecutableApplicationIconList.isEmpty()) {
				mQuickActionSpinner.setEnabled(false);
				
				installedAndExecutableApplicationLabelList.add(mContext.getString(R.string.view_custom_menu_quick_action_loadding));
				installedAndExecutableApplicationIconList.add(mContext.getResources().getDrawable(R.drawable.custom_spinner_quick_loadding));
				quickActionSpinnerAdapter = new WidgetQuickActionAdapter(mContext, R.layout.view_layout_widget_custom_quick_action_list_row, R.id.view_layout_widget_custom_quick_action_list_row_label, installedAndExecutableApplicationLabelList, installedAndExecutableApplicationIconList);
				mQuickActionSpinner.setAdapter(quickActionSpinnerAdapter);
			} else {
				mQuickActionSpinner.setEnabled(true);
				
				if (quickActionSpinnerAdapter == null) {
					quickActionSpinnerAdapter = new WidgetQuickActionAdapter(mContext, R.layout.view_layout_widget_custom_quick_action_list_row, R.id.view_layout_widget_custom_quick_action_list_row_label, installedAndExecutableApplicationLabelList, installedAndExecutableApplicationIconList);
				} else {
					quickActionSpinnerAdapter.setActionList(installedAndExecutableApplicationLabelList);
					quickActionSpinnerAdapter.setIconList(installedAndExecutableApplicationIconList);
				}
				
				mQuickActionSpinner.setAdapter(quickActionSpinnerAdapter);
				mQuickActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
						// TODO Auto-generated method stub
						if (position == 0) {
							mWidgetAction = "";
						} else {
							mWidgetAction = mInstalledAndExcutableApplicationPackageNameList.get(position);
						}
					}
					
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
					}
				});
				
				if (mWidgetAction.equals("")) {
					mQuickActionSpinner.setSelection(0);
				} else {
					for (int i = 0; i < mInstalledAndExcutableApplicationPackageNameList.size(); i ++) {
						if (mWidgetAction.equals(mInstalledAndExcutableApplicationPackageNameList.get(i))) {
							mQuickActionSpinner.setSelection(i);
							break;
						}
					}
				}
			}
		}
	}
}
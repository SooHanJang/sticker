package madcat.studio.view;

import java.lang.reflect.Field;
import java.util.ArrayList;

import madcat.studio.activity.WidgetSettingActivity;
import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import madcat.studio.view.data.ImageViewData;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

public class WidgetSelectView extends LinearLayout {
//	private static final String TAG = "WIDGET_SELECT_VIEW";
	
	private Context mContext = null;
	
	/**
	 * mStickerContainer��  ViewPager �����ؾߵ�.
	 */
	private ScrollView mWidgetContainerScroll = null;
	private TableLayout mWidgetContainer = null;
	
	private int mWidgetType = Constants.WIDGET_TYPE_STATIC;
	private int mWidgetWidth = Constants.WIDGET_SIZE_1;
	private int mWidgetHeight = Constants.WIDGET_SIZE_1;
	
	private ArrayList<ImageViewData> mWidgetList = null;
	private Data mSelectedWidgetData = null;
	
	private String mSelectedWidgetCategory = null;
	
	private boolean mAnimationFlag = false;
	
	private int CELL_WIDTH		= 77;
	private int CELL_HEIGHT		= 100;
	
	public WidgetSelectView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);

		mContext = context;
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.view_layout_widget_select, this, true);
		
		mWidgetContainerScroll = (ScrollView)findViewById(R.id.view_layout_widget_select_sticker_container_scroll);
		mWidgetContainer = (TableLayout)findViewById(R.id.view_layout_widget_select_sticker_container);
		
		mWidgetList = new ArrayList<ImageViewData>();
		
		mSelectedWidgetCategory = Constants.CATEGORY_ALL;
		
		TouchEventHandler touchEventHandler = new TouchEventHandler();
		mWidgetContainerScroll.setOnTouchListener(touchEventHandler);
		mWidgetContainer.setOnTouchListener(touchEventHandler);
		
		float scale = mContext.getResources().getDisplayMetrics().density;
		CELL_WIDTH = (int)(CELL_WIDTH * scale);
		CELL_HEIGHT = (int)(CELL_HEIGHT * scale);
	}
	
	public void setSelectedCategory(String category) {
		mSelectedWidgetCategory = category;
	}
	
	public void loadWidgets() {
		String widgetSize = mWidgetWidth + "x" + mWidgetHeight;
		String packageName = mContext.getPackageName();
		
		int weightSumForViewGroup = 1;
		
		switch (mWidgetWidth) {
		case Constants.WIDGET_SIZE_1:
			weightSumForViewGroup = 4;
			break;
		case Constants.WIDGET_SIZE_2:
			weightSumForViewGroup = 2;
			break;
		case Constants.WIDGET_SIZE_3:
		case Constants.WIDGET_SIZE_4:
			weightSumForViewGroup = 1;
			break;
		}
		
		Field[] drawables = R.drawable.class.getFields();
		
		if (mWidgetList == null) {
			mWidgetList = new ArrayList<ImageViewData>();
		} else {
			mWidgetList.clear();
		}
		
 		if (mWidgetContainer.getChildCount() > 0) {
			mWidgetContainer.removeAllViews();
		}
 		
 		ImageViewEventHandler imageViewEventHandler = new ImageViewEventHandler();
 		
 		TableRow tableRow = null;
 		int columnIndex = 0;
 		boolean isFirstIamgeView = true;
 		
		if (drawables != null && drawables.length > 0) {
			ArrayList<String> foundStickerList = new ArrayList<String>();
			boolean findFlag = true;
			
			for (int i = 0; i < drawables.length; i++, findFlag = true) {
				String drawableName = drawables[i].getName();
				String keyword = null;
				
				if (drawableName.contains(Constants.STICKER_IDENTIFIER) && drawableName.contains(widgetSize) && !drawableName.contains(Constants.BACKGROUND)) {
					if (mWidgetType == Constants.WIDGET_TYPE_STATIC) {
						if (mSelectedWidgetCategory != Constants.CATEGORY_ALL) {
							findFlag = drawableName.contains(mSelectedWidgetCategory)
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CALENDAR])
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12])
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24])
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_COUNTER]);
						} else {
							findFlag = !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CALENDAR])
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12])
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24])
									&& !drawableName.contains(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_COUNTER]);
						}
					} else {
						findFlag = drawableName.contains(Constants.WIDGET_TYPE_ARRAY[mWidgetType]);
					}
					
					if (findFlag) {
						String[] parsedDrawableName = drawableName.split(Constants.SPLITER);
						keyword = parsedDrawableName[3];
						
						if (!foundStickerList.contains(keyword)) {
							foundStickerList.add(keyword);
							
							Data data = new Data();
							data.setWidgetStickerName(drawableName);
							ImageViewData imageViewData = new ImageViewData();
							imageViewData.setData(data);
							
							mWidgetList.add(imageViewData);
						} 
					}
				}
			}
			
			foundStickerList.clear();
			foundStickerList = null;
			
			System.gc();
		}
		
		ImageViewData imageViewData = null;
		for (int i = 0; i < mWidgetList.size(); i++) {
			imageViewData = mWidgetList.get(i);
			
			RelativeLayout layout = new RelativeLayout(mContext);
			layout.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
			layout.setPadding(5, 5, 5, 5);
			
			int stickerId = getResources().getIdentifier(imageViewData.getData().getWidgetStickerName(), Constants.DRAWABLE, packageName);
			int backgroundId = 0;
			
			String[] parsedStickerName = imageViewData.getData().getWidgetStickerName().split(Constants.SPLITER);
			
			if (mWidgetType != Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
				String fixedBackgroundColor = null;
				
				try {
					fixedBackgroundColor = Constants.SPLITER + parsedStickerName[5];
				} catch (ArrayIndexOutOfBoundsException e) {
					fixedBackgroundColor = null;
				}
				
				if (fixedBackgroundColor == null) {
					backgroundId = getResources().getIdentifier((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_WHITE), Constants.DRAWABLE, packageName);
					if (backgroundId != 0) {
						imageViewData.getData().setWidgetBackgroundName((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_WHITE));
					} else {
						backgroundId = getResources().getIdentifier((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_BLACK), Constants.DRAWABLE, packageName);
						if (backgroundId != 0) {
							imageViewData.getData().setWidgetBackgroundName((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_BLACK));
						} else {
							backgroundId = getResources().getIdentifier((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_DARK_GREEN), Constants.DRAWABLE, packageName);
							imageViewData.getData().setWidgetBackgroundName((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_DARK_GREEN));
						}
					}
				} else {
					backgroundId = getResources().getIdentifier((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + fixedBackgroundColor), Constants.DRAWABLE, packageName);
					imageViewData.getData().setWidgetBackgroundName((parsedStickerName[0] + Constants.SPLITER + parsedStickerName[1] + Constants.SPLITER + parsedStickerName[2] + Constants.SPLITER + parsedStickerName[3] + Constants.BACKGROUND + fixedBackgroundColor));
				}
				
				ImageView background = new ImageView(mContext);
				background.setImageResource(backgroundId);
				background.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
				background.setClickable(false);
				
				layout.addView(background, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
			}
			
			ImageView sticker = new ImageView(mContext);
			sticker.setImageResource(stickerId);
			sticker.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
			sticker.setClickable(true);
			sticker.setOnClickListener(imageViewEventHandler);
			
			layout.addView(sticker, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT));
			
			imageViewData.setImageView(sticker);
			
			if (columnIndex++ == 0) {
				tableRow = new TableRow(mContext);
				tableRow.setGravity(Gravity.CENTER);
				tableRow.setOrientation(TableRow.HORIZONTAL);
			}
			
			if (isFirstIamgeView) {
				mSelectedWidgetData = imageViewData.getData();
				isFirstIamgeView = false;
			}
			
			tableRow.addView(layout, new TableRow.LayoutParams(CELL_WIDTH * mWidgetWidth, CELL_HEIGHT * mWidgetHeight));
			
			if (columnIndex == weightSumForViewGroup) {
				mWidgetContainer.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, CELL_HEIGHT * mWidgetHeight));
				columnIndex = 0;
			}
		}
		
		if (columnIndex > 0) {
			if (mWidgetWidth == 1) {
				for (int j = tableRow.getChildCount(); j < 4; j++) {
					RelativeLayout paddingLayout = new RelativeLayout(mContext);
					paddingLayout.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
					paddingLayout.setPadding(5, 5, 5, 5);
					
					tableRow.addView(paddingLayout, new TableRow.LayoutParams(CELL_WIDTH * mWidgetWidth, CELL_HEIGHT * mWidgetHeight));
				}
			} else if (mWidgetWidth == 2) {
				for (int j = tableRow.getChildCount(); j < 2; j++) {
					RelativeLayout paddingLayout = new RelativeLayout(mContext);
					paddingLayout.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
					paddingLayout.setPadding(5, 5, 5, 5);
					
					tableRow.addView(paddingLayout, new TableRow.LayoutParams(CELL_WIDTH * mWidgetWidth, CELL_HEIGHT * mWidgetHeight));
				}
			}
			
			mWidgetContainer.addView(tableRow);
		}
	}

	private WidgetSettingActivity mParentActivity = null;
	
	public void setParentActivity(WidgetSettingActivity parentActivity) {
		mParentActivity = parentActivity;
	}
	
	public void setWidgetWidth(int widgetWidth) {
		mWidgetWidth = widgetWidth;
	}
	
	public void setWidgetHeight(int widgetHeight) {
		mWidgetHeight = widgetHeight;
	}
	
	public void setWidgetType(int widgetType) {
		mWidgetType = widgetType;
	}
	
	public int getWidgetWidth() {
		return mWidgetWidth;
	}
	
	public int getWidgetHeight() {
		return mWidgetHeight;
	}
	
	public String getWidgetSize() {
		return mWidgetWidth + "x" + mWidgetHeight;
	}
	
	public String getSelectedWidgetStickerName() {
		return mSelectedWidgetData.getWidgetStickerName();
	}
	
	public String getSelectedWidgetBackgroundName() {
		return mSelectedWidgetData.getWidgetBackgroundName();
	}
	
	public String getSelectedWidgetCategory() {
		return mSelectedWidgetCategory;
	}
	
	public boolean isAnimationRunning() {
		return mAnimationFlag;
	}
	
	public void initializeLayout() {
		Util.recursiveRecycle(mWidgetContainer);
		mWidgetContainer = (TableLayout)findViewById(R.id.view_layout_widget_select_sticker_container);
	}
	
	private class TouchEventHandler implements View.OnTouchListener {
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			if (!mParentActivity.getAnimationFlag()) {
				if (mParentActivity.getSideBarVisibility()) {
					mParentActivity.setSideBarVisibility(false);
				}
			}
			
			return false;
		}
	}
	
	private class ImageViewEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (!mAnimationFlag) {
				if (mParentActivity.getSideBarVisibility()) {
					mParentActivity.setSideBarVisibility(false);
				} else {
					ImageViewData imageViewData = null;
					for (int i = 0; i < mWidgetList.size(); i++) {
						imageViewData = mWidgetList.get(i);
						if (imageViewData.getImageView().equals(v)) {
							mSelectedWidgetData = imageViewData.getData();
							mParentActivity.changeView();
							break;
						}
					}
				}
			}
		}
	}
}
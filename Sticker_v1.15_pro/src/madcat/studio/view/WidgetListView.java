package madcat.studio.view;

import java.util.ArrayList;

import madcat.studio.activity.WidgetManagementActivity;
import madcat.studio.adapter.WidgetManagementAdapter;
import madcat.studio.database.Data;
import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class WidgetListView extends LinearLayout {
	private Context mContext = null;
	
	private ListView mListView = null;
	private WidgetManagementAdapter mWidgetListAdapter = null;
	
	private ArrayList<Data> mRegisteredWidgetList = null;
	private Data mSelectedWidget = null;
	
	public WidgetListView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.view_layout_widget_list, this);
		
		mListView = (ListView)findViewById(R.id.view_layout_widget_list_listview);
		
		mRegisteredWidgetList = new ArrayList<Data>();
	}
	
	private WidgetManagementActivity mParentActivity = null;
	
	public void setParentActivity(WidgetManagementActivity parentActivity) {
		mParentActivity = parentActivity; 
	}
	
	public void setRegisteredWidgetList(ArrayList<Data> registeredWidgetList) {
		if (mRegisteredWidgetList == null) {
			mRegisteredWidgetList = new ArrayList<Data>(registeredWidgetList);
		} else {
			mRegisteredWidgetList.clear();
			mRegisteredWidgetList.addAll(registeredWidgetList);
		}
		
		mWidgetListAdapter = new WidgetManagementAdapter(mContext, R.layout.view_layout_widget_management_list_row, mRegisteredWidgetList);
		mListView.setAdapter(mWidgetListAdapter);
		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				// TODO Auto-generated method stub
				mSelectedWidget = mRegisteredWidgetList.get(position);
				mParentActivity.changeView(WidgetManagementActivity.MODE_CUSTOM);
			}
		});
	}
	
	public Data getSelectedWidgetData() {
		return mSelectedWidget;
	}
}

package madcat.studio.view;

import madcat.studio.activity.WidgetSettingActivity;
import madcat.studio.constants.Constants;
import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

public class WidgetSideBar extends RelativeLayout {
	private Context mContext = null;
	
	/**
	 * mSideScrollBarView에 Open/Close Animation 적용해야됨. -> 적용함. 2012. 08. 13. by daniel
	 */
	private RelativeLayout mSideBarLayout = null;
	private ImageButton mSideBarControlBtn = null, mSideBarControlHiddenBtn = null;
	private RadioGroup mSideBarRadioGroup = null;
	private RadioButton[] mSideBarRadioButtons = null;
	
	private boolean mIsSideBarOnOff = true;
	private boolean mIsRunningAnimation = false;
	
	private float mScale = 0.0f;
	
	private static final String RADIO_BUTTON_NAME	= "view_layout_widget_select_sidebar_radiobtn_";
	
	public WidgetSideBar(Context context) {
		// TODO Auto-generated constructor stub
		this(context, Constants.CATEGORY_ALL);
	}
	
	public WidgetSideBar(Context context, String selectedCategory) {
		super(context);
		
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.view_layout_widget_select_sidebar, this);
		
		mSideBarLayout = (RelativeLayout)findViewById(R.id.view_layout_widget_select_sidebar_layout);
		mSideBarControlBtn = (ImageButton)findViewById(R.id.view_layout_widget_select_sidebar_btn);
		mSideBarControlHiddenBtn = (ImageButton)findViewById(R.id.view_layout_widget_select_sidebar_hidden_btn);
		mSideBarRadioGroup = (RadioGroup)findViewById(R.id.view_layout_widget_select_sidebar_radiogroup);
		
		mSideBarRadioButtons = new RadioButton[6];
		for (int i = 0; i < 6; i++) {
			mSideBarRadioButtons[i] = (RadioButton)findViewById(context.getResources().getIdentifier(RADIO_BUTTON_NAME + i, Constants.ID, mContext.getPackageName()));
		}
		
		if (selectedCategory.equals(Constants.CATEGORY_ALL)) {
			mSideBarRadioButtons[0].setChecked(true);
		} else if (selectedCategory.equals(Constants.CATEGORY_DAILY)) {
			mSideBarRadioButtons[1].setChecked(true);
		} else if (selectedCategory.equals(Constants.CATEGORY_CHEER)) {
			mSideBarRadioButtons[2].setChecked(true);
		} else if (selectedCategory.equals(Constants.CATEGORY_DIET)) {
			mSideBarRadioButtons[3].setChecked(true);
		} else if (selectedCategory.equals(Constants.CATEGORY_LOVE)) {
			mSideBarRadioButtons[4].setChecked(true);
		} else if (selectedCategory.equals(Constants.CATEGORY_STUDY)) {
			mSideBarRadioButtons[5].setChecked(true);
		}
		
		ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
		mSideBarControlBtn.setOnClickListener(buttonEventHandler);
		mSideBarControlHiddenBtn.setOnClickListener(buttonEventHandler);
		mSideBarRadioGroup.setOnCheckedChangeListener(new RadioButtonEventHandler());
	
		mScale = mContext.getResources().getDisplayMetrics().density;
	}
	
	private WidgetSettingActivity mParentActivity = null;
	
	public void setParentActivity(WidgetSettingActivity parentActivity) {
		mParentActivity = parentActivity;
	}
	
	private static final int LAYOUT_ANIMATION_INTERVAL = 300;
	private static final int BUTTON_ANIMATION_INTERVAL = 200;
	
	public void setSideBarVisibility(boolean visibility) {
		if (!mIsRunningAnimation) {
			TranslateAnimation sidebarLayoutAnimation = null;
			TranslateAnimation sidebarButtonAnimation = null;
			
			if (visibility) {		//Execute Sidebar On Animation
				sidebarLayoutAnimation = new TranslateAnimation(120 * mScale * -1, 0.0f, 0.0f, 0.0f);
				sidebarButtonAnimation = new TranslateAnimation(80 * mScale * -1, 0.0f, 0.0f, 0.0f);
				sidebarButtonAnimation.setStartOffset(LAYOUT_ANIMATION_INTERVAL - BUTTON_ANIMATION_INTERVAL);
			} else {				//Execute Sidebar Off Animation
				sidebarLayoutAnimation = new TranslateAnimation(0.0f, 120 * mScale * -1, 0.0f, 0.0f);
				sidebarButtonAnimation = new TranslateAnimation(0.0f, 80 * mScale * -1, 0.0f, 0.0f);
			}
			
			sidebarLayoutAnimation.setDuration(LAYOUT_ANIMATION_INTERVAL);
			sidebarLayoutAnimation.setInterpolator(new AccelerateInterpolator());
			sidebarLayoutAnimation.setFillAfter(true);
			
			sidebarButtonAnimation.setDuration(BUTTON_ANIMATION_INTERVAL);
			sidebarButtonAnimation.setInterpolator(new AccelerateInterpolator());
			sidebarButtonAnimation.setFillAfter(true);
			
			mSideBarLayout.setAnimation(null);
			mSideBarControlBtn.setAnimation(null);
			
			mSideBarLayout.startAnimation(sidebarLayoutAnimation);
			mSideBarControlBtn.startAnimation(sidebarButtonAnimation);
			new AnimationLockAsyncTask(visibility).execute();
		}
	}
	
	public boolean getSideBarVisibility() {
		return mIsSideBarOnOff;
	}
	
	public boolean getAnimationFlag() {
		return mIsRunningAnimation;
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (!mIsRunningAnimation) {
				switch (v.getId()) {
				case R.id.view_layout_widget_select_sidebar_btn:
					setSideBarVisibility(false);
					break;
				case R.id.view_layout_widget_select_sidebar_hidden_btn:
					setSideBarVisibility(true);
					break;
				}
			}
		}
	}
	
	private class RadioButtonEventHandler implements RadioGroup.OnCheckedChangeListener {
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if (!mIsRunningAnimation) {
				switch (checkedId) {
				case R.id.view_layout_widget_select_sidebar_radiobtn_0:
					mParentActivity.setSelectedCategory(Constants.CATEGORY_ALL);
					mParentActivity.setTitleImage(R.drawable.menu_mgt_image_title_list);
					break;
				case R.id.view_layout_widget_select_sidebar_radiobtn_1:
					mParentActivity.setSelectedCategory(Constants.CATEGORY_DAILY);
					mParentActivity.setTitleImage(R.drawable.category_daily);
					break;
				case R.id.view_layout_widget_select_sidebar_radiobtn_2:
					mParentActivity.setSelectedCategory(Constants.CATEGORY_CHEER);
					mParentActivity.setTitleImage(R.drawable.category_cheer);
					break;
				case R.id.view_layout_widget_select_sidebar_radiobtn_3:
					mParentActivity.setSelectedCategory(Constants.CATEGORY_DIET);
					mParentActivity.setTitleImage(R.drawable.category_diet);
					break;
				case R.id.view_layout_widget_select_sidebar_radiobtn_4:
					mParentActivity.setSelectedCategory(Constants.CATEGORY_LOVE);
					mParentActivity.setTitleImage(R.drawable.category_love);
					break;
				case R.id.view_layout_widget_select_sidebar_radiobtn_5:
					mParentActivity.setSelectedCategory(Constants.CATEGORY_STUDY);
					mParentActivity.setTitleImage(R.drawable.category_study);
					break;
				}
			}
		}
	}
	
	private class AnimationLockAsyncTask extends AsyncTask<Void, Void, Void> {
		private boolean visibility = false;
		
		public AnimationLockAsyncTask(boolean visibility) {
			// TODO Auto-generated constructor stub
			this.visibility = visibility;
			
			if (visibility) {
				mSideBarControlBtn.setClickable(true);
				mSideBarLayout.setVisibility(View.VISIBLE);
				mSideBarRadioGroup.setVisibility(View.VISIBLE);
				
				for (int i = 0; i < mSideBarRadioButtons.length; i++) {
					mSideBarRadioButtons[i].setVisibility(View.VISIBLE);
				}
			}
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			mIsRunningAnimation = true;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(LAYOUT_ANIMATION_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			mIsSideBarOnOff  = visibility;
			mIsRunningAnimation = false;
			
			if (!visibility) {
				mSideBarControlBtn.setClickable(false);
				mSideBarLayout.setVisibility(View.GONE);
				mSideBarRadioGroup.setVisibility(View.GONE);
				
				for (int i = 0; i < mSideBarRadioButtons.length; i++) {
					mSideBarRadioButtons[i].setVisibility(View.GONE);
				}
			}
		}
	}
}

package madcat.studio.view;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import madcat.studio.activity.WidgetManagementActivity;
import madcat.studio.constants.Constants;
import madcat.studio.database.Data;
import madcat.studio.sticker.pro.R;
import madcat.studio.utils.Util;
import madcat.studio.view.data.ImageViewData;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class WidgetGalleryView extends LinearLayout {
//	private static final String TAG = "WIDGET_GALLERY_VIEW";
	
	private Context mContext = null;
	private String mPackageName = null;
	
	/**
	 * mStickerContainer에  ViewPager 적용해야됨.
	 */
	private LinearLayout mWidgetContainer = null;
	
	/**
	 * mSideScrollBarView에 Open/Close Animation 적용해야됨. -> 적용함. 2012. 08. 13. by daniel
	 */

	private ImageViewEventHandler mImageViewEventHandler = null;
	private ArrayList<ImageViewData> mWidgetImageViewDataList = null;
	private Data mSelectedWidgetData = null;
	
	private static final String PREFIX = "sticker_padding_";
	private int[] mPaddingImageArray = null;
	private boolean[] mPaddingDuplicateArray = null;
	
	private int CELL_WIDTH	= 77;
	private int CELL_HEIGHT	= 100;
	
	public WidgetGalleryView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);

		mContext = context;
		mPackageName = mContext.getPackageName();
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.view_layout_widget_gallery, this, true);
		
		mWidgetContainer = (LinearLayout)findViewById(R.id.view_layout_widget_gallery_sticker_container);
		
		mImageViewEventHandler = new ImageViewEventHandler();
		mWidgetImageViewDataList = new ArrayList<ImageViewData>();
		
		//Calculation Cell Width / Height
		float scale = context.getResources().getDisplayMetrics().density;
		CELL_WIDTH = (int)(CELL_WIDTH * scale);
		CELL_HEIGHT = (int)(CELL_HEIGHT * scale);
		
		mPaddingImageArray = new int[9];
		mPaddingDuplicateArray = new boolean[9];
		for (int i = 0; i < 9; i++) {
			mPaddingImageArray[i] = mContext.getResources().getIdentifier(PREFIX + i, Constants.DRAWABLE, mPackageName);
			mPaddingDuplicateArray[i] = false;
		}
	}
	
	private ArrayList<Data>[][] mWidgetList = null;
	
	public boolean isWidgetListEmpty() {
		boolean emptyFlag = true;
		
		if (mWidgetList != null) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					if (mWidgetList[i][j] != null && mWidgetList[i][j].isEmpty()) {
						return false;
					}
				}
			}
		}
		
		return emptyFlag;
	}
	
	@SuppressWarnings("unchecked")
	public void clearWidgetList() {
		if (mWidgetList == null) {
			mWidgetList = new ArrayList[4][4];
		}
		
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (mWidgetList[i][j] == null) {
					mWidgetList[i][j] = new ArrayList<Data>();
				} else {
					mWidgetList[i][j].clear();
				}
			}
		}
		
		if (mWidgetContainer.getChildCount() > 0) {
			Util.recursiveRecycle(mWidgetContainer);
		}
	}
	
	public void loadWidgetsFromDrawables() {
		clearWidgetList();
		
		Field[] drawables = R.drawable.class.getFields();
		String drawableName = null;
		String[] parsedDrawableName = null;
		
		HashSet<String> keywordHashSet = new HashSet<String>();
		
		for (int i = 0; i < drawables.length; i++) {
			drawableName = drawables[i].getName();

			if (drawableName.contains(Constants.STICKER_IDENTIFIER) && !drawableName.contains(Constants.BACKGROUND) && !drawableName.contains(Constants.PADDING)) {
				parsedDrawableName = drawableName.split(Constants.SPLITER);
				
				if (!keywordHashSet.contains(parsedDrawableName[3])) {
					keywordHashSet.add(parsedDrawableName[3]);
					
					Data data = new Data();
					
					if (parsedDrawableName[2].equals(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CALENDAR])) {
						data.setWidgetType(Constants.WIDGET_TYPE_DYNAMIC_CALENDAR);
					} else if (parsedDrawableName[2].equals(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12])) {
						data.setWidgetType(Constants.WIDGET_TYPE_DYNAMIC_CLOCK_12);
					} else if (parsedDrawableName[2].equals(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24])) {
						data.setWidgetType(Constants.WIDGET_TYPE_DYNAMIC_CLOCK_24);
					} else if (parsedDrawableName[2].equals(Constants.WIDGET_TYPE_ARRAY[Constants.WIDGET_TYPE_DYNAMIC_COUNTER])) {
						data.setWidgetType(Constants.WIDGET_TYPE_DYNAMIC_COUNTER);
					} else {
						data.setWidgetType(Constants.WIDGET_TYPE_STATIC);
					}
					
					data.setWidgetStickerName(drawableName);
					if (data.getWidgetType() != Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
						try {
							data.setWidgetBackgroundName(parsedDrawableName[0] + Constants.SPLITER + parsedDrawableName[1] + Constants.SPLITER + parsedDrawableName[2] + Constants.SPLITER + parsedDrawableName[3] + Constants.BACKGROUND + Constants.SPLITER + parsedDrawableName[5]);
						} catch (ArrayIndexOutOfBoundsException e) {
							data.setWidgetBackgroundName(parsedDrawableName[0] + Constants.SPLITER + parsedDrawableName[1] + Constants.SPLITER + parsedDrawableName[2] + Constants.SPLITER + parsedDrawableName[3] + Constants.BACKGROUND + Constants.BACKGROUND_COLOR_WHITE);
						}
					}
					
					data.setWidgetSize(Integer.valueOf(parsedDrawableName[1].substring(0, 1)), Integer.valueOf(parsedDrawableName[1].substring(2, 3)));
					mWidgetList[data.getWidgetWidth() - 1][data.getWidgetHeight() - 1].add(data);
				}
			}
		}
		
		if (!isWidgetListEmpty()) {
			makeUI();
		}
	}
	
	public void loadWidgetsFromDatabase(ArrayList<Data> dataList) {
		if (dataList != null && !dataList.isEmpty()) {
			clearWidgetList();
			
			mSelectedWidgetData = dataList.get(0);
			
			for (int i = 0; i < dataList.size(); i++) {
				Data data = dataList.get(i);
				mWidgetList[data.getWidgetWidth() - 1][data.getWidgetHeight() - 1].add(data);
			}
			
			if (!isWidgetListEmpty()) {
				makeUI();
			}
		}
	}
	
	private WidgetManagementActivity mParentActivity = null;
	
	public void setParentActivity(WidgetManagementActivity parentActivity) {
		mParentActivity = parentActivity;
	}
	
	public Data getSelectedWidgetData() {
		return mSelectedWidgetData;
	}
	
	@SuppressWarnings("unchecked")
	private void makeUI() {
		ArrayList<LinearLayout>[] galleryRowList = new ArrayList[4];
		for (int i = 0; i < 4; i++) {
			galleryRowList[i] = new ArrayList<LinearLayout>();
		}
		
		for (int i = 3; i >= 0; i--) {
			for (int j = 3; j >= 0; j--) {
				if (mWidgetList[i][j] != null) {
					for (int k = 0; !mWidgetList[i][j].isEmpty(); k++) {
						LinearLayout rowLayout = makeGalleryRow();
						
						switch ((i + 1)) {
						case Constants.WIDGET_SIZE_1:
						case Constants.WIDGET_SIZE_2:
						case Constants.WIDGET_SIZE_3:
							if (k % 2 == 0) {
								rowLayout.addView(makeImageLayout(mWidgetList[i][j].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH * (i + 1), CELL_HEIGHT * (j + 1)));
//								rowLayout.addView(makeGalleryColumn((3 - i), (j + 1)), new LinearLayout.LayoutParams(CELL_WIDTH * (3 - i), CELL_HEIGHT * (j + 1)));
								rowLayout.addView(makeGalleryColumn((3 - i), (j + 1)), new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
							} else {
//								rowLayout.addView(makeGalleryColumn((3 - i), (j + 1)), new LinearLayout.LayoutParams(CELL_WIDTH * (3 - i), CELL_HEIGHT * (j + 1)));
								rowLayout.addView(makeGalleryColumn((3 - i), (j + 1)), new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
								if (!mWidgetList[i][j].isEmpty()) {
									rowLayout.addView(makeImageLayout(mWidgetList[i][j].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH * (i + 1), CELL_HEIGHT * (j + 1)));	
								} else {
									rowLayout.addView(makeGalleryColumn((i + 1), (j + 1)));
								}
							}
							break;
						case Constants.WIDGET_SIZE_4:
							//이 부분 에러 관측됨. 근데 왜인지 모르겠음. 에러로그 첨부함.
//							09-10 20:25:32.050: E/AndroidRuntime(12417): FATAL EXCEPTION: main
//							09-10 20:25:32.050: E/AndroidRuntime(12417): java.lang.RuntimeException: Unable to start activity ComponentInfo{madcat.studio.sticker.pro/madcat.studio.activity.menu.WidgetViewActivity}: java.lang.IndexOutOfBoundsException: Invalid index 0, size is 0
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.ActivityThread.performLaunchActivity(ActivityThread.java:1647)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.ActivityThread.handleLaunchActivity(ActivityThread.java:1663)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.ActivityThread.access$1500(ActivityThread.java:117)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.ActivityThread$H.handleMessage(ActivityThread.java:931)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.os.Handler.dispatchMessage(Handler.java:99)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.os.Looper.loop(Looper.java:130)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.ActivityThread.main(ActivityThread.java:3683)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at java.lang.reflect.Method.invokeNative(Native Method)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at java.lang.reflect.Method.invoke(Method.java:507)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:839)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:597)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at dalvik.system.NativeStart.main(Native Method)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): Caused by: java.lang.IndexOutOfBoundsException: Invalid index 0, size is 0
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at java.util.ArrayList.throwIndexOutOfBoundsException(ArrayList.java:257)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at java.util.ArrayList.remove(ArrayList.java:406)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at madcat.studio.activity.view.WidgetGalleryView.makeImageLayout(WidgetGalleryView.java:286)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at madcat.studio.activity.view.WidgetGalleryView.makeUI(WidgetGalleryView.java:206)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at madcat.studio.activity.view.WidgetGalleryView.loadWidgetsFromDrawables(WidgetGalleryView.java:150)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at madcat.studio.activity.menu.WidgetViewActivity.onCreate(WidgetViewActivity.java:51)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.Instrumentation.callActivityOnCreate(Instrumentation.java:1047)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	at android.app.ActivityThread.performLaunchActivity(ActivityThread.java:1611)
//							09-10 20:25:32.050: E/AndroidRuntime(12417): 	... 11 more
							rowLayout.addView(makeImageLayout(mWidgetList[i][j].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH * (i + 1), CELL_HEIGHT * (j + 1)));
							break;
						}
						
						rowLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
						rowLayout.setGravity(Gravity.CENTER);
						galleryRowList[j].add(rowLayout);
					}
				}
			}
		}
		
		LinearLayout lastRow = null;
		
		//adding in widget container.
		while (!(galleryRowList[0].isEmpty() && galleryRowList[1].isEmpty() && galleryRowList[2].isEmpty() && galleryRowList[3].isEmpty())) {
			for (int i = 0; i < 4; i++) {
				if (!galleryRowList[i].isEmpty()) {
					mWidgetContainer.addView(galleryRowList[i].get(0));
					lastRow = galleryRowList[i].get(0);
					galleryRowList[i].remove(0);
				}
			}
		}
		
		recursivePaddingSearch(lastRow);
	}
	
	private LinearLayout makeGalleryRow() {
		LinearLayout galleryRow = new LinearLayout(mContext);
		galleryRow.setOrientation(LinearLayout.HORIZONTAL);
		galleryRow.setGravity(Gravity.LEFT|Gravity.TOP);
		galleryRow.setPadding(5, 5, 5, 5);
		
		return galleryRow;
	}
	
	private static final String PADDING_TAG = "padding";
	
	private RelativeLayout makePaddingImageLayout() {
		int duplicateFullCheck = 0;
		for (int i = 0; i < 9; i++) {
			if (mPaddingDuplicateArray[i]) {
				duplicateFullCheck++;
			}
		}
		
		if (duplicateFullCheck == 8) {
			for (int i = 0; i < 9; i++) {
				mPaddingDuplicateArray[i] = false;
			}
		}
		
		Random random = new Random();
		int randomIndex = 0;
		
		do {
			randomIndex = random.nextInt(mPaddingImageArray.length);
		} while (mPaddingDuplicateArray[randomIndex]);

		mPaddingDuplicateArray[randomIndex] = true;
		
		RelativeLayout layout = new RelativeLayout(mContext);
		ImageView sticker = new ImageView(mContext);
		
		RelativeLayout.LayoutParams stickerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
		
		stickerLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		
		sticker.setImageResource(mPaddingImageArray[randomIndex]);
		sticker.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
		sticker.setScaleType(ScaleType.FIT_CENTER);
		
		layout.addView(sticker, stickerLayoutParams);
		layout.setTag(PADDING_TAG);
		
		return layout;
	}
	
	private RelativeLayout makeImageLayout(Data data) {
		RelativeLayout layout = new RelativeLayout(mContext);
		ImageView sticker = new ImageView(mContext);
		ImageView background = null;
		
		RelativeLayout.LayoutParams backgroundLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
		RelativeLayout.LayoutParams stickerLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
		
		stickerLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		
		sticker.setImageResource(mContext.getResources().getIdentifier(data.getWidgetStickerName(), Constants.DRAWABLE, mPackageName));
		sticker.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
		sticker.setScaleType(ScaleType.FIT_CENTER);
		sticker.setClickable(true);
		sticker.setOnClickListener(mImageViewEventHandler);
		
		if (data.getWidgetType() != Constants.WIDGET_TYPE_DYNAMIC_CALENDAR) {
			background = new ImageView(mContext);
			backgroundLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
			background.setImageResource(mContext.getResources().getIdentifier(data.getWidgetBackgroundName(), Constants.DRAWABLE, mPackageName));
			background.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
			background.setScaleType(ScaleType.FIT_CENTER);
			background.setAlpha(data.getWidgetAlphaLevel());
			background.setClickable(false);
			
			layout.addView(background, backgroundLayoutParams);
		} else {
			sticker.setAlpha(data.getWidgetAlphaLevel());
		}
		
		layout.addView(sticker, stickerLayoutParams);
		
		mWidgetImageViewDataList.add(new ImageViewData(sticker, data));
		mWidgetList[data.getWidgetWidth() - 1][data.getWidgetHeight() - 1].remove(0);
		
		return layout;
	}
	
	/**
	 * This method not support a widget height upper 3.
	 * @param width
	 * @param height
	 * @return layout
	 */
	private LinearLayout makeGalleryColumn(int width, int height) {
		LinearLayout layout = new LinearLayout(mContext);
		layout.setGravity(Gravity.LEFT|Gravity.TOP);
		
		switch ((width * 10 + height)) {
		case Constants.WIDGET_SIZE_1X1:
			if (mWidgetList[0][0] != null && !mWidgetList[0][0].isEmpty()) { 
				layout.addView(makeImageLayout(mWidgetList[0][0].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT));
			} else {
				layout.addView(makePaddingImageLayout(), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT));
			}
			return layout;
		case Constants.WIDGET_SIZE_1X2:
			if (mWidgetList[0][1] != null && !mWidgetList[0][1].isEmpty()) {
				layout.addView(makeImageLayout(mWidgetList[0][1].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT * 2));
			} else {
				layout.setOrientation(LinearLayout.VERTICAL);
				for (int i = 0; i < 2; i++) {
					layout.addView(makeGalleryColumn(Constants.WIDGET_SIZE_1, Constants.WIDGET_SIZE_1), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT));
				}
			}
			
			return layout;
		case Constants.WIDGET_SIZE_1X3:
		case Constants.WIDGET_SIZE_1X4:
			if (mWidgetList[width - 1][height - 1] != null && !mWidgetList[width - 1][height - 1].isEmpty()) {
				layout.addView(makeImageLayout(mWidgetList[width - 1][height - 1].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT * height));
			} else {
				layout.setOrientation(LinearLayout.VERTICAL);
				layout.addView(makeGalleryColumn(Constants.WIDGET_SIZE_1, height - 1), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT * (height - 1)));
				layout.addView(makeGalleryColumn(Constants.WIDGET_SIZE_1, Constants.WIDGET_SIZE_1), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT));
			}
			
			return layout;
		case Constants.WIDGET_SIZE_2X1:
		case Constants.WIDGET_SIZE_2X2:
		case Constants.WIDGET_SIZE_2X3:
		case Constants.WIDGET_SIZE_2X4:
			if (mWidgetList[width - 1][height - 1] != null && !mWidgetList[width - 1][height - 1].isEmpty()) {
				layout.addView(makeImageLayout(mWidgetList[width - 1][height - 1].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH * width, CELL_HEIGHT * height));
			} else {
				layout.setOrientation(LinearLayout.HORIZONTAL);
				for (int i = 0; i < 2; i++) {
					layout.addView(makeGalleryColumn(Constants.WIDGET_SIZE_1, height), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT * height));
				}
			}
			
			return layout;
		case Constants.WIDGET_SIZE_3X1:
		case Constants.WIDGET_SIZE_3X2:
		case Constants.WIDGET_SIZE_3X3:
		case Constants.WIDGET_SIZE_3X4:
			if (mWidgetList[width - 1][height - 1] != null && !mWidgetList[width - 1][height - 1].isEmpty()) {
				layout.addView(makeImageLayout(mWidgetList[width - 1][height - 1].get(0)), new LinearLayout.LayoutParams(CELL_WIDTH * width, CELL_HEIGHT * height));
			} else {
				layout.setOrientation(LinearLayout.HORIZONTAL);
				layout.addView(makeGalleryColumn(Constants.WIDGET_SIZE_2, height), new LinearLayout.LayoutParams(CELL_WIDTH * (width - 1), CELL_HEIGHT * height));
				layout.addView(makeGalleryColumn(Constants.WIDGET_SIZE_1, height), new LinearLayout.LayoutParams(CELL_WIDTH, CELL_HEIGHT * height));
			}
			
			return layout;
		default:
			return null;
		}
	}

	private void recursivePaddingSearch(View root) {
		if (root instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup)root;
			int count = viewGroup.getChildCount();
			
			for (int i = 0; i < count; i++) {
				String tag = (String)viewGroup.getChildAt(i).getTag();
				
				if (tag != null && tag.equals(PADDING_TAG)) {
					viewGroup.removeViewAt(i);
				} else {
					recursivePaddingSearch(viewGroup.getChildAt(i));
				}
			}
			
			if (viewGroup.getChildCount() == 0) {
				viewGroup.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			}
		}
	}
	
	private class ImageViewEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			ImageViewData imageViewData = null;
			for (int i = 0; i < mWidgetImageViewDataList.size(); i++) {
				imageViewData = mWidgetImageViewDataList.get(i);
				if (imageViewData.getImageView().equals(v)) {
					mSelectedWidgetData = imageViewData.getData();
					mParentActivity.changeView(WidgetManagementActivity.MODE_CUSTOM);
					break;
				}
			}
		}
	}
}
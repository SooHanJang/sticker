package madcat.studio.view;

import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

public class ColorDrawableImageButton extends LinearLayout {
	private Context mContext = null;
	
	private ImageView mStateView = null;
	
	private ColorButtonDrawable mColorButtonDrawable = null;
	private String mColor = null;
	private boolean mClicked = false;
	
	private static final int OUTLINE_RADIUS	= 6;
	private static final int OUTLINE_WIDTH	= 5;
	
	public ColorDrawableImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mContext = context;
		
		mColorButtonDrawable = new ColorButtonDrawable();
		setBackgroundDrawable(mColorButtonDrawable);
		
		mStateView = new ImageView(context);
		mStateView.setScaleType(ScaleType.FIT_CENTER);
		mStateView.setImageResource(0);
		mStateView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
		mStateView.setPadding(OUTLINE_WIDTH, OUTLINE_WIDTH, OUTLINE_WIDTH, OUTLINE_WIDTH);
		
		addView(mStateView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
	}
	
	
	public void setFillColor(String color) {
		mColorButtonDrawable.setFillColor(Color.parseColor(color));
	}
	
	public String getFillColor() {
		return mColor;
	}
	
	public void setClicked(boolean clicked) {
		mClicked = clicked;
		
		if (isEnabled()) {
			if (clicked) {
				mStateView.setImageResource(R.drawable.custom_color_check);
			} else {
				mStateView.setImageResource(0);
			}
		} else {
			mStateView.setImageResource(R.drawable.custom_color_disabled);
		}
	}
	
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		
		if (enabled) {
			if (mClicked) {
				mStateView.setImageResource(R.drawable.custom_color_check);
			} else {
				mStateView.setImageResource(0);
			}
		} else {
			mStateView.setImageResource(R.drawable.custom_color_disabled);
		}
	}
	
	public boolean isClicked() {
		return mClicked;
	}
	
	private class ColorButtonDrawable extends PaintDrawable {
		private Paint mFillPaint = null, mStrokePaint = null;
		
		public ColorButtonDrawable() {
			// TODO Auto-generated constructor stub
			setShape(new RectShape());
			
			setCornerRadius(OUTLINE_RADIUS);
			
			mFillPaint = getPaint();
			mFillPaint.setColor(Color.WHITE);
			mFillPaint.setAntiAlias(true);
			
			mStrokePaint = new Paint(mFillPaint);
			mStrokePaint.setStyle(Paint.Style.STROKE);
			mStrokePaint.setStrokeWidth(OUTLINE_WIDTH);
			mStrokePaint.setColor(mContext.getResources().getColor(R.color.softpink));	//color : softpink
			mStrokePaint.setAntiAlias(true);
		}
		
		@Override
		protected void onDraw(Shape shape, Canvas canvas, Paint paint) {
			// TODO Auto-generated method stub
			shape.draw(canvas, mFillPaint);
			shape.draw(canvas, mStrokePaint);
		}
		
		public void setFillColor(int color) {
			mFillPaint.setColor(color);
		}
	}

}

package madcat.studio.view;

import madcat.studio.activity.WidgetHelpActivity;
import madcat.studio.constants.Constants;
import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class HelpContentView extends LinearLayout {
	private Context mContext = null;
	
	private ImageView mPageDisplay = null;
	private ImageButton mPagePrevBtn = null, mPageNextBtn = null;
	
	private WidgetHelpActivity mParentActivity = null;
	
	public static final int PAGE_ENROLL		= 0;
	public static final int PAGE_MANAGEMENT	= 1;
	
	private static final int ENROLL_MAX_PAGE		= 4;
	private static final int MANAGEMENT_MAX_PAGE	= 3;
	private int mPageNumber = 0;
	private int mPageMaxNumber = ENROLL_MAX_PAGE;
	
	private static final String PREFIX_ENROLL		= "help_image_enroll_";
	private static final String PREFIX_MANAGEMENT	= "help_image_mgt_";
	private String mPagePrefix = PREFIX_ENROLL;
	
	private String mPackageName = null;
	
	public HelpContentView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		mPackageName = mContext.getPackageName();
		
		LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.view_layout_help_content, this);
		
		mPageDisplay = (ImageView)findViewById(R.id.view_layout_help_content_image);
		mPagePrevBtn = (ImageButton)findViewById(R.id.view_layout_help_content_prevBtn);
		mPageNextBtn = (ImageButton)findViewById(R.id.view_layout_help_content_nextBtn);
		
		ImageButtonEventHandler eventHandler = new ImageButtonEventHandler();
		mPagePrevBtn.setOnClickListener(eventHandler);
		mPageNextBtn.setOnClickListener(eventHandler);
	}
	
	public void setParentActivity(WidgetHelpActivity parentActivity) {
		mParentActivity = parentActivity;
	}
	
	public void setHelpPage(int page) {
		mPageNumber = 0;
		
		switch (page) {
		case PAGE_ENROLL:
			mPageMaxNumber = ENROLL_MAX_PAGE;
			mPagePrefix = PREFIX_ENROLL;
			break;
		case PAGE_MANAGEMENT:
			mPageMaxNumber = MANAGEMENT_MAX_PAGE;
			mPagePrefix = PREFIX_MANAGEMENT;
			break;
		}
		
		mPageDisplay.setImageResource(mContext.getResources().getIdentifier(mPagePrefix + 0, Constants.DRAWABLE, mPackageName));
	}
	
	private class ImageButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.view_layout_help_content_prevBtn:
				if (mPageNumber == 0) {
					mParentActivity.goHelpPage(WidgetHelpActivity.HELP_PAGE_INDEX);
					mPageDisplay.setImageResource(0);
				} else {
					mPageDisplay.setImageResource(mContext.getResources().getIdentifier(mPagePrefix + (--mPageNumber), Constants.DRAWABLE, mPackageName));
				}
				break;
			case R.id.view_layout_help_content_nextBtn:
				if ((mPageNumber + 1) < mPageMaxNumber) {
					mPageDisplay.setImageResource(mContext.getResources().getIdentifier(mPagePrefix + (++mPageNumber), Constants.DRAWABLE, mPackageName));
				} else {
					mParentActivity.goHelpPage(WidgetHelpActivity.HELP_PAGE_INDEX);
					mPageDisplay.setImageResource(0);
				}
				break;
			}
		}
	}
}

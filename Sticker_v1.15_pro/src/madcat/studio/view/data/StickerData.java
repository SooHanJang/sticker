package madcat.studio.view.data;

import java.util.ArrayList;

import madcat.studio.constants.Constants;

import android.content.Context;

public class StickerData {
	private ArrayList<String> mStickerNameList = null;
	private ArrayList<String> mBackgroundNameList = null;
	
	public StickerData() {
		mStickerNameList = new ArrayList<String>();
		mBackgroundNameList = new ArrayList<String>();
	}
	
	public void addAll(StickerData stickerData) {
		if (!isEmpty()) {
			clear();
		}
		
		for (int i = 0; i < stickerData.getStickerNameListSize(); i++) {
			mStickerNameList.add(stickerData.getStickerName(i));
		}
		
		for (int i = 0; i < stickerData.getBackgroundNameListSize(); i++) {
			mBackgroundNameList.add(stickerData.getBackgroundName(i));
		}
	}
	
	public void addStickerName(String stickerName) {
		mStickerNameList.add(stickerName);
	}
	
	public void addBackgroundName(String backgroundName) {
		mBackgroundNameList.add(backgroundName);
	}
	
	public String getStickerName(int index) {
		if (index < mStickerNameList.size()) {
			return mStickerNameList.get(index);
		} else {
			return null;
		}
	}
	
	public String getBackgroundName(int index) {
		if (index < mBackgroundNameList.size()) {
			return mBackgroundNameList.get(index);
		} else {
			return null;
		}
	}
	
	private static final String PREFIX_COLOR = "#";
	
	public String getStickerColor(Context context, int index) {
		if (index < mStickerNameList.size()) {
			try {
				return PREFIX_COLOR + mStickerNameList.get(index).split(Constants.SPLITER)[4];
			} catch (ArrayIndexOutOfBoundsException e) {
				return Constants.DEFAULT_COLOR_GRAY;
			}
		} else {
			return Constants.DEFAULT_COLOR_GRAY;
		}
	}
	
	public String getBackgroundColor(Context context, int index) {
		if (index < mBackgroundNameList.size()) {
			try {
				return PREFIX_COLOR + mBackgroundNameList.get(index).split(Constants.SPLITER)[5];
			} catch (ArrayIndexOutOfBoundsException e) {
				return Constants.DEFAULT_COLOR_GRAY;
			}
		} else {
			return Constants.DEFAULT_COLOR_GRAY;
		}
	}
	
	public int getStickerNameListSize() {
		return mStickerNameList.size();
	}
	
	public int getBackgroundNameListSize() {
		return mBackgroundNameList.size();
	}
	
	public void clearStickerNameList() {
		mStickerNameList.clear();
	}
	
	public void clearBackgroundNameList() {
		mBackgroundNameList.clear();
	}
	
	public boolean isEmpty() {
		return mStickerNameList.isEmpty() && mBackgroundNameList.isEmpty();
	}
	
	public void clear() {
		mStickerNameList.clear();
		mBackgroundNameList.clear();
	}
}

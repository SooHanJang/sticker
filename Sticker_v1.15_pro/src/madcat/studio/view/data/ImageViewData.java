package madcat.studio.view.data;

import madcat.studio.database.Data;
import android.widget.ImageView;

public class ImageViewData {
	private ImageView mImageView = null;
	private Data mData = null;
	
	public ImageViewData() {
		// TODO Auto-generated constructor stub
		mImageView = null;
		mData = null;
	}
	
	public ImageViewData(ImageView iv, Data data) {
		mImageView = iv;
		mData = data;
	}
	
	public void setImageView(ImageView iv) {
		mImageView = iv;
	}
	
	public void setData(Data data) {
		mData = data;
	}
	
	public ImageView getImageView() {
		return mImageView;
	}
	
	public Data getData() {
		return mData;
	}
}

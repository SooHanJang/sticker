package madcat.studio.view;

import java.util.Locale;

import madcat.studio.activity.WidgetHelpActivity;
import madcat.studio.sticker.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class HelpIndexView extends LinearLayout {
	private Context mContext = null;
	
	private WidgetHelpActivity mParentActivity = null;
	private ImageButton mHelpEnrollBtn = null, mHelpManagementBtn = null;
	
	public HelpIndexView(Context context) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		Locale locale = mContext.getResources().getConfiguration().locale;
		
		if (locale.equals(Locale.KOREA) || locale.equals(Locale.KOREAN)) {
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			inflater.inflate(R.layout.view_layout_help_index_ko, this);
			
			mHelpEnrollBtn = (ImageButton)findViewById(R.id.view_layout_help_index_ko_enroll);
			mHelpManagementBtn = (ImageButton)findViewById(R.id.view_layout_help_index_ko_mgt);
			
			ImageButtonEventHandler eventHandler = new ImageButtonEventHandler();
			mHelpEnrollBtn.setOnClickListener(eventHandler);
			mHelpManagementBtn.setOnClickListener(eventHandler);
		} else {
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			inflater.inflate(R.layout.view_layout_help_index_en, this);
			
			mHelpEnrollBtn = (ImageButton)findViewById(R.id.view_layout_help_index_en_enroll);
			mHelpManagementBtn = (ImageButton)findViewById(R.id.view_layout_help_index_en_mgt);
			
			ImageButtonEventHandler eventHandler = new ImageButtonEventHandler();
			mHelpEnrollBtn.setOnClickListener(eventHandler);
			mHelpManagementBtn.setOnClickListener(eventHandler);
		}
	}
	
	public void setParentActivity(WidgetHelpActivity parentActivity) {
		mParentActivity = parentActivity;
	}
	
	private class ImageButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.view_layout_help_index_ko_enroll:
			case R.id.view_layout_help_index_en_enroll:
				mParentActivity.goHelpPage(WidgetHelpActivity.HELP_PAGE_ENROLL);
				break;
			case R.id.view_layout_help_index_ko_mgt:
			case R.id.view_layout_help_index_en_mgt:
				mParentActivity.goHelpPage(WidgetHelpActivity.HELP_PAGE_MANAGEMENT);
				break;
			}
		}
	}
}
